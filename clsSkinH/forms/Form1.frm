﻿#VisualFreeBasic_Form#  Version=5.6.8
Locked=-1

[Form]
Name=Form1
ClassStyle=CS_VREDRAW,CS_HREDRAW,CS_DBLCLKS
ClassName=
WinStyle=WS_THICKFRAME,WS_CAPTION,WS_SYSMENU,WS_MINIMIZEBOX,WS_MAXIMIZEBOX,WS_CLIPSIBLINGS,WS_CLIPCHILDREN,WS_VISIBLE,WS_EX_WINDOWEDGE,WS_EX_CONTROLPARENT,WS_EX_LEFT,WS_EX_LTRREADING,WS_EX_RIGHTSCROLLBAR,WS_POPUP,WS_SIZEBOX
Style=3 - 常规窗口
Icon=
Caption=皮肤测试
StartPosition=1 - 屏幕中心
WindowState=0 - 正常
Enabled=True
Repeat=False
Left=0
Top=0
Width=428
Height=460
TopMost=False
Child=False
MdiChild=False
TitleBar=True
SizeBox=True
SysMenu=True
MaximizeBox=True
MinimizeBox=True
Help=False
Hscroll=False
Vscroll=False
MinWidth=0
MinHeight=0
MaxWidth=0
MaxHeight=0
NoActivate=False
MousePass=False
TransPer=0
TransColor=SYS,25
Shadow=0 - 无阴影
BackColor=SYS,15
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Slider]
Name=Slider1
Index=-1
Style=1 - 刻度在底部
AutoTicks=True
NoTicks=False
EnableSelRange=False
FixedLength=False
NoThumb=False
ToolTips=False
Max=255
Min=20
Value=100
TickFrequency=1
BackColor=SYS,25
Enabled=True
Visible=True
Left=5
Top=48
Width=169
Height=24
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[TabControl]
Name=TabControl1
Index=-1
Style=0 - 标签在顶部
Custom=3|0|Tab1|||0|Tab2|||0|Tab3|||0|
FixedWidth=False
Multiline=False
Buttons=False
NoScroll=False
MultiSelect=False
FlatButton=False
IconLeft=False
LabelLeft=False
HotTrack=False
FocusButton=False
FocusNever=False
ToolTips=False
OwnDraw=False
HeaderTopPadding=4
HeaderSidePadding=4
TabHeight=22
TabWidth=40
Enabled=True
Visible=True
Left=184
Top=218
Width=225
Height=161
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[StatusBar]
Name=Status1
Status=皮肤测试例程QQ1493446087010
SizeGrip=True
ToolTips=True
Enabled=True
Visible=True
Font=微软雅黑,9,0
BackColor=SYS,15
MinHeight=0
Tag=
Tab=True

[ListBox]
Name=List1
Index=-1
Custom=
Style=0 - 单选
BStyle=3 - 凹边框
OwnDraw=0 - 系统绘制
ItemHeight=18
HasString=False
Sorted=False
NoHeight=True
MultiColumn=False
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9,0
Left=4
Top=247
Width=176
Height=131
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[ComboBox]
Name=Combo1
Index=-1
Style=0 - 标签和下拉框
Custom=
OwnDraw=0 - 系统绘制
LabelHeight=20
ItemHeight=18
HasString=False
Sorted=False
NoHeight=False
AutoHscroll=True
MaxLength=0
Uppercase=False
Lowercase=False
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9,0
Left=5
Top=219
Width=170
Height=28
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Option]
Name=Option1
Index=-1
Style=0 - 标准
Caption=Option1
TextAlign=3 - 中左对齐
Alignment=0 - 文本在左边
Value=0 - 未选择
Multiline=True
GroupName=OptionGroup1
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9,0
Left=241
Top=180
Width=74
Height=20
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[TextBox]
Name=Text1
Index=-1
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=Text1
Enabled=True
Visible=True
MaxLength=0
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=False
AutoHScroll=True
AutoVScroll=False
Left=107
Top=176
Width=122
Height=31
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[CheckBox]
Name=Check1
Index=-1
Style=0 - 标准
Caption=Check1
TextAlign=3 - 中左对齐
Alignment=0 - 文本在左边
Value=0 - 未选择
Multiline=True
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9,0
Left=30
Top=180
Width=106
Height=21
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Button]
Name=Command3
Index=-1
Caption=设置窗体透明度
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=8
Top=79
Width=176
Height=40
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command2
Index=-1
Caption=导入皮肤并设置色调、饱和度、亮度
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=189
Top=2
Width=213
Height=40
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command1
Index=-1
Caption=导入本地skinh.she皮肤文件
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=8
Top=2
Width=176
Height=40
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[TopMenu]
Name=TopMenu1
Menu=皮肤1mnuSkin10-10{［X.o］-MSN.shemnuMSN0-10［X.o］-QQ2009_宽_底边.shemnuQQ2009_10-10［X.o］-QQ2009_窄_底边.shemnuQQ2009_20-10［X.o］-QQ影音.shemnuQQVideo0-10［X.o］-REAL.shemnuReal0-10［X.o］-积木.shemnuToyBricks0-10［X.o］-炫绿.shemnuGreen0-10}皮肤2mnuSkin20-10{xmp.shemnuXmp0-10Xenes.shemnuXenes0-10wish.shemnuWish0-10whitefire.shemnuWhiteFire0-10vista.shemnuVista0-10storm.shemnuStorm0-10skinh.shemnuSkinh0-10royale.shemnuRoyale0-10qqgame.shemnuQQGame0-10}皮肤3mnuSkin30-10{QQ2009.shemnuQQ20090-10qq2008.shemnuQQ20080-10pixos.shemnuPixos0-10ouframe.shemnuOuframe0-10office2007.shemnuOffice20070-10longhorn.shemnuLonghorn0-10itunes.shemnuItunes0-10}皮肤4mnuSkin40-10{insomnia.shemnuInsomnia0-10homestead.shemnuHomestead0-10hlong.shemnuHlong0-10gem.shemnuGem0-10enjoy.shemnuEnjoy0-10elegance.shemnuElegance0-10dogmax.shemnuDogmax0-10}皮肤5mnuSkin50-10{darkroyale.shemnuDarkroyale0-10compact.shemnuCompact0-10china.shemnuChina0-10black.shemnuBlack0-10asus.shemnuAsus0-10aero.shemnuAero0-10adamant.shemnuAdamant0-10}
Tag=

[Button]
Name=Command4
Index=-1
Caption=设置色调、饱和度、亮度
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=189
Top=79
Width=213
Height=40
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command5
Index=-1
Caption=设置Aero特效
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=8
Top=120
Width=176
Height=40
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command6
Index=-1
Caption=设置菜单透明度
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=189
Top=120
Width=213
Height=40
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Frame]
Name=Frame1
Index=-1
Caption=Frame1
TextAlign=0 - 左对齐
Fillet=5
BorderWidth=1
BorderColor=SYS,16
ForeColor=SYS,8
BackColor=SYS,25
Enabled=True
Visible=True
Font=微软雅黑,9,0
Left=13
Top=158
Width=392
Height=56
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Slider]
Name=Slider2
Index=-1
Style=1 - 刻度在底部
AutoTicks=True
NoTicks=False
EnableSelRange=False
FixedLength=False
NoThumb=False
ToolTips=False
Max=180
Min=-180
Value=1
TickFrequency=10
BackColor=SYS,25
Enabled=True
Visible=True
Left=177
Top=50
Width=81
Height=24
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Slider]
Name=Slider3
Index=-1
Style=1 - 刻度在底部
AutoTicks=True
NoTicks=False
EnableSelRange=False
FixedLength=False
NoThumb=False
ToolTips=False
Max=100
Min=-100
Value=1
TickFrequency=10
BackColor=SYS,25
Enabled=True
Visible=True
Left=255
Top=51
Width=81
Height=24
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Slider]
Name=Slider4
Index=-1
Style=1 - 刻度在底部
AutoTicks=True
NoTicks=False
EnableSelRange=False
FixedLength=False
NoThumb=False
ToolTips=False
Max=100
Min=-100
Value=1
TickFrequency=10
BackColor=SYS,25
Enabled=True
Visible=True
Left=331
Top=50
Width=81
Height=24
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False


[AllCode]
'这是标准的工程模版，你也可做自己的模版。
'写好工程，复制全部文件到VFB软件文件夹里【template】里即可，子文件夹名为 VFB新建工程里显示的名称
'快去打造属于你自己的工程模版吧。

Dim Shared skin As clsSkinH
Dim Shared checkid As Long 

Sub Form1_TopMenu1_WM_Command(hWndForm As hWnd,wID As ULong)  '点击了菜单项
    Select Case wID
    Case mnuMSN ' ［X.o］-MSN.she
        skin.AttachEx(App.Path & "skin\［X.o］-MSN.she")
        If checkid <> mnuMSN Then 
            TopMenu1.Check(checkid) = False
            checkid = mnuMSN
            TopMenu1.Check(checkid) = True
        End If
    Case mnuQQ2009_1 ' ［X.o］-QQ2009_宽_底边.she
        skin.AttachEx(App.Path & "skin\［X.o］-QQ2009_宽_底边.she")
        If checkid <> mnuQQ2009_1 Then
            TopMenu1.Check(checkid) = False
            checkid = mnuQQ2009_1
            TopMenu1.Check(checkid) = True
        End If
    Case mnuQQ2009_2 ' ［X.o］-QQ2009_窄_底边.she
        skin.AttachEx(App.Path & "skin\［X.o］-QQ2009_窄_底边.she")
        If checkid <> mnuQQ2009_2 Then
            TopMenu1.Check(checkid) = False
            checkid = mnuQQ2009_2
            TopMenu1.Check(checkid) = True
        End If
    Case mnuQQVideo ' ［X.o］-QQ影音.she
        skin.AttachEx(App.Path & "skin\［X.o］-QQ影音.she")
        If checkid <> mnuQQVideo Then
            TopMenu1.Check(checkid) = False
            checkid = mnuQQVideo
            TopMenu1.Check(checkid) = True
        End If
    Case mnuReal ' ［X.o］-REAL.she
        skin.AttachEx(App.Path & "skin\［X.o］-REAL.she")
        If checkid <> mnuReal Then
            TopMenu1.Check(checkid) = False
            checkid = mnuReal
            TopMenu1.Check(checkid) = True
        End If
    Case mnuToyBricks ' ［X.o］-积木.she
        skin.AttachEx(App.Path & "skin\［X.o］-积木.she")
        If checkid <> mnuToyBricks Then
            TopMenu1.Check(checkid) = False
            checkid = mnuToyBricks
            TopMenu1.Check(checkid) = True
        End If
    Case mnuGreen ' ［X.o］-炫绿.she
        skin.AttachEx(App.Path & "skin\［X.o］-炫绿.she")
        If checkid <> mnuGreen Then
            TopMenu1.Check(checkid) = False
            checkid = mnuGreen
            TopMenu1.Check(checkid) = True
        End If
    Case mnuAdamant ' adamant.she
        skin.AttachEx(App.Path & "skin\adamant.she")
        If checkid <> mnuAdamant Then
            TopMenu1.Check(checkid) = False
            checkid = mnuAdamant
            TopMenu1.Check(checkid) = True
        End If
    Case mnuAero ' aero.she
        skin.AttachEx(App.Path & "skin\aero.she")
        If checkid <> mnuAero Then
            TopMenu1.Check(checkid) = False
            checkid = mnuAero
            TopMenu1.Check(checkid) = True
        End If
    Case mnuAsus ' asus.she
        skin.AttachEx(App.Path & "skin\asus.she")
        If checkid <> mnuAsus Then
            TopMenu1.Check(checkid) = False
            checkid = mnuAsus
            TopMenu1.Check(checkid) = True
        End If
    Case mnuBlack ' black.she
        skin.AttachEx(App.Path & "skin\black.she")
        If checkid <> mnuBlack Then
            TopMenu1.Check(checkid) = False
            checkid = mnuBlack
            TopMenu1.Check(checkid) = True
        End If
    Case mnuChina ' china.she
        skin.AttachEx(App.Path & "skin\china.she")
        If checkid <> mnuChina Then
            TopMenu1.Check(checkid) = False
            checkid = mnuChina
            TopMenu1.Check(checkid) = True
        End If
    Case mnuCompact ' compact.she
        skin.AttachEx(App.Path & "skin\compact.she")
        If checkid <> mnuCompact Then
            TopMenu1.Check(checkid) = False
            checkid = mnuCompact
            TopMenu1.Check(checkid) = True
        End If
    Case mnuDarkroyale ' darkroyale.she
        skin.AttachEx(App.Path & "skin\darkroyale.she")
        If checkid <> mnuDarkroyale Then
            TopMenu1.Check(checkid) = False
            checkid = mnuDarkroyale
            TopMenu1.Check(checkid) = True
        End If
    Case mnuDogmax ' dogmax.she
        skin.AttachEx(App.Path & "skin\dogmax.she")
        If checkid <> mnuDogmax Then
            TopMenu1.Check(checkid) = False
            checkid = mnuDogmax
            TopMenu1.Check(checkid) = True
        End If
    Case mnuElegance ' elegance.she
        skin.AttachEx(App.Path & "skin\elegance.she")
        If checkid <> mnuElegance Then
            TopMenu1.Check(checkid) = False
            checkid = mnuElegance
            TopMenu1.Check(checkid) = True
        End If
    Case mnuEnjoy ' enjoy.she
        skin.AttachEx(App.Path & "skin\enjoy.she")
        If checkid <> mnuEnjoy Then
            TopMenu1.Check(checkid) = False
            checkid = mnuEnjoy
            TopMenu1.Check(checkid) = True
        End If
    Case mnuGem ' gem.she
        skin.AttachEx(App.Path & "skin\gem.she")
        If checkid <> mnuGem Then
            TopMenu1.Check(checkid) = False
            checkid = mnuGem
            TopMenu1.Check(checkid) = True
        End If
    Case mnuHlong ' hlong.she
        skin.AttachEx(App.Path & "skin\hlong.she")
        If checkid <> mnuHlong Then
            TopMenu1.Check(checkid) = False
            checkid = mnuHlong
            TopMenu1.Check(checkid) = True
        End If
    Case mnuHomestead ' homestead.she
        skin.AttachEx(App.Path & "skin\homestead.she")
        If checkid <> mnuHomestead Then
            TopMenu1.Check(checkid) = False
            checkid = mnuHomestead
            TopMenu1.Check(checkid) = True
        End If
    Case mnuInsomnia ' insomnia.she
        skin.AttachEx(App.Path & "skin\insomnia.she")
        If checkid <> mnuInsomnia Then
            TopMenu1.Check(checkid) = False
            checkid = mnuInsomnia
            TopMenu1.Check(checkid) = True
        End If
    Case mnuItunes ' itunes.she
        skin.AttachEx(App.Path & "skin\itunes.she")
        If checkid <> mnuItunes Then
            TopMenu1.Check(checkid) = False
            checkid = mnuItunes
            TopMenu1.Check(checkid) = True
        End If
    Case mnuLonghorn ' longhorn.she
        skin.AttachEx(App.Path & "skin\longhorn.she")
        If checkid <> mnuLonghorn Then
            TopMenu1.Check(checkid) = False
            checkid = mnuLonghorn
            TopMenu1.Check(checkid) = True
        End If
    Case mnuOffice2007 ' office2007.she
        skin.AttachEx(App.Path & "skin\office2007.she")
        If checkid <> mnuOffice2007 Then
            TopMenu1.Check(checkid) = False
            checkid = mnuOffice2007
            TopMenu1.Check(checkid) = True
        End If
    Case mnuOuframe ' ouframe.she
        skin.AttachEx(App.Path & "skin\ouframe.she")
        If checkid <> mnuOuframe Then
            TopMenu1.Check(checkid) = False
            checkid = mnuOuframe
            TopMenu1.Check(checkid) = True
        End If
    Case mnuPixos ' pixos.she
        skin.AttachEx(App.Path & "skin\pixos.she")
        If checkid <> mnuPixos Then
            TopMenu1.Check(checkid) = False
            checkid = mnuPixos
            TopMenu1.Check(checkid) = True
        End If
    Case mnuQQ2008 ' qq2008.she
        skin.AttachEx(App.Path & "skin\qq2008.she")
        If checkid <> mnuQQ2008 Then
            TopMenu1.Check(checkid) = False
            checkid = mnuQQ2008
            TopMenu1.Check(checkid) = True
        End If
    Case mnuQQ2009 ' QQ2009.she
        skin.AttachEx(App.Path & "skin\QQ2009.she")
        If checkid <> mnuQQ2009 Then
            TopMenu1.Check(checkid) = False
            checkid = mnuQQ2009
            TopMenu1.Check(checkid) = True
        End If
    Case mnuQQGame ' qqgame.she
        skin.AttachEx(App.Path & "skin\qqgame.she")
        If checkid <> mnuQQGame Then
            TopMenu1.Check(checkid) = False
            checkid = mnuQQGame
            TopMenu1.Check(checkid) = True
        End If
    Case mnuRoyale ' royale.she
        skin.AttachEx(App.Path & "skin\royale.she")
        If checkid <> mnuRoyale Then
            TopMenu1.Check(checkid) = False
            checkid = mnuRoyale
            TopMenu1.Check(checkid) = True
        End If
    Case mnuSkinh ' skinh.she
        skin.AttachEx(App.Path & "skin\skinh.she")
        If checkid <> mnuSkinh Then
            TopMenu1.Check(checkid) = False
            checkid = mnuSkinh
            TopMenu1.Check(checkid) = True
        End If
    Case mnuStorm ' storm.she
        skin.AttachEx(App.Path & "skin\storm.she")
        If checkid <> mnuStorm Then
            TopMenu1.Check(checkid) = False
            checkid = mnuStorm
            TopMenu1.Check(checkid) = True
        End If
    Case mnuVista ' vista.she
        skin.AttachEx(App.Path & "skin\vista.she")
        If checkid <> mnuVista Then
            TopMenu1.Check(checkid) = False
            checkid = mnuVista
            TopMenu1.Check(checkid) = True
        End If
    Case mnuWhiteFire ' whitefire.she
        skin.AttachEx(App.Path & "skin\whitefire.she")
        If checkid <> mnuWhiteFire Then
            TopMenu1.Check(checkid) = False
            checkid = mnuWhiteFire
            TopMenu1.Check(checkid) = True
        End If
    Case mnuWish ' wish.she
        skin.AttachEx(App.Path & "skin\wish.she")
        If checkid <> mnuWish Then
            TopMenu1.Check(checkid) = False
            checkid = mnuWish
            TopMenu1.Check(checkid) = True
        End If
    Case mnuXenes ' Xenes.she
        skin.AttachEx(App.Path & "skin\Xenes.she")
        If checkid <> mnuXenes Then
            TopMenu1.Check(checkid) = False
            checkid = mnuXenes
            TopMenu1.Check(checkid) = True
        End If
    Case mnuXmp ' xmp.she
        skin.AttachEx(App.Path & "skin\xmp.she")
        If checkid <> mnuXmp Then
            TopMenu1.Check(checkid) = False
            checkid = mnuXmp
            TopMenu1.Check(checkid) = True
        End If
    End Select
End Sub

Sub Form1_Shown(hWndForm As hWnd,UserData As Integer)  '窗口完全显示后。UserData 来自显示窗口最后1个参数。
    skin.AttachEx(App.Path & "skin\［X.o］-MSN.she")
    TopMenu1.Check(mnuMSN) = True 
    checkid = mnuMSN
    
    With Combo1
        For i As Long = 0 To 100
            .AddItem "第" & i & "条"
        Next
        .ListIndex = 0
    End With
    
    With List1
        For i As Long = 0 To 10
            .AddItem "第" & i & "条"
        Next
        .ListIndex = 0
    End With

End Sub

Sub Form1_Command1_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
    skin.Attach
End Sub

Sub Form1_Command2_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
    skin.AttachExt(App.Path & "skin\［X.o］-MSN.she","",Slider2.Value,Slider3.Value,Slider4.Value)
End Sub

Sub Form1_Command3_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
    skin.SetWindowAlpha(hWndForm,Slider1.Value)
End Sub

Sub Form1_Command4_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
    skin.AdjustHSV(Slider2.Value,Slider3.Value,Slider4.Value) 
End Sub

Sub Form1_Command5_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
    skin.SetAero(True)
End Sub

Sub Form1_Command6_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)  '单击
    skin.SetMenuAlpha(Slider1.Value)
End Sub

Sub Form1_Slider1_WM_HScroll(hWndForm As hWnd,hWndControl As hWnd, nScrollCode As Long, nPosition As Long)  '发生水平滚动
    skin.SetWindowAlpha(hWndForm,Slider1.Value)            
End Sub












