﻿'=====================================================================================================
'SkinH皮肤类
'声明：本模块只供学习交流使用
'可以通过Attach方法加载当前目录下名为skinh.she的皮肤文件
'或者AttachEx方法加载指定路径的皮肤
'通过Detach方法卸载皮肤
'欢迎交流QQ1493446087 
'/**********************错误代码******************************/
#define  SRET_OK              0 '//成功
#define  SRET_ERROR           1 '//失败
#define  SRET_ERROR_READ      2 '//皮肤文件读取错误
#define  SRET_ERROR_PARAM     3 '//参数错误
#define  SRET_ERROR_CREATE    4 '//创建皮肤失败
#define  SRET_ERROR_FORMAT    5 '//皮肤格式不正确
#define  SRET_ERROR_VERSION   6 '//皮肤版本不兼容
#define  SRET_ERROR_PASSWORD  7 '//皮肤密钥错误
#define  SRET_ERROR_INVALID   8 '//换肤引擎无效

'//换肤类型
#define  TYPE_UNKNOWN      0    '//未知类型
#define  TYPE_ANIMATE      1001 '//动画控件
#define  TYPE_CHECKBOX     1002 '//复选框
#define  TYPE_COMBOBOX     1003 '//组合框
#define  TYPE_COMBOLBOX    1004 '//组合下拉框
#define  TYPE_CONTROLBAR   1005 '//控件栏
#define  TYPE_DATETIME     1006 '//日期控件
#define  TYPE_EDITBOX      1007 '//文本框
#define  TYPE_GROUPBOX     1008 '//分组框
#define  TYPE_HEADERCTRL   1009 '//列头控件
#define  TYPE_HOTKEY       1010 '//热键控件
#define  TYPE_IPADDRESS    1011 '//IP地址控件
#define  TYPE_LABEL        1012 '//标签控件
#define  TYPE_LISTBOX      1013 '//列表框
#define  TYPE_LISTVIEW     1014 '//列表视图
#define  TYPE_MDICLIENT    1015 '//MDI客户区
#define  TYPE_MENU         1016 '//菜单
#define  TYPE_MONTHCAL     1017 '//月历控件
#define  TYPE_PICTURE      1018 '//图片框
#define  TYPE_PROGRESS     1019 '//进度条
#define  TYPE_PUSHBUTTON   1020 '//普通按钮
#define  TYPE_RADIOBUTTON  1021 '//单选框
#define  TYPE_REBAR        1022 '//重组栏
#define  TYPE_RICHEDIT     1023 '//富文本框
#define  TYPE_SCROLLBAR    1024 '//滚动条
#define  TYPE_SCROLLCTRL   1025 '//内置滚动条的控件
#define  TYPE_SPINCTRL     1026 '//调节器
#define  TYPE_STATUSBAR    1027 '//状态栏
#define  TYPE_TABCTRL      1028 '//选择夹
#define  TYPE_TOOLBAR      1029 '//工具栏
#define  TYPE_TOOLBARWND   1030 '//MFC工具栏窗体
#define  TYPE_TRACKBAR     1031 '//滑条控件
#define  TYPE_TREEVIEW     1032 '//树形视图
#define  TYPE_WINDOW       1034 '//标准窗体
#define  TYPE_COMCTRL      1036 '//通用换肤
#define  TYPE_PAINTCTRL    1037 '//通用换肤

Type clsSkinH Extends Object
Private : 
    hlib As Any Ptr
Public : 
    Declare Constructor
    Declare Destructor
Public : 
    Declare Function Attach() As Long '加载程序当前目录下的文件名为skinh.she的皮肤进行换肤,成功返回0, 失败返回非0
    Declare Function AttachEx(sSkinFile As String ,sPassword As String = "") As Long '加载指定路径的皮肤进行换肤,成功返回0, 失败返回非0
    Declare Function AttachExt(sSkinFile As String ,sPassword As String = "" ,nHue As Long = 0 ,nSat As Long = 0 ,nBri As Long = 0)                As Long '加载指定路径的皮肤进行换肤并指定相应的色调，饱和度，亮度
    Declare Function AttachRes(nShe As UByte ,dwSize As DWORD ,sPassword As String = "" ,nHue As Long = 0 ,nSat As Long = 0 ,nBri As Long = 0)     As Long '加载指定资源进行换肤并指定相应的色调，饱和度，亮度
    Declare Function AttachResEx(sName As String ,sType As String ,sPassword As String = "" ,nHue As Long = 0 ,nSat As Long = 0 ,nBri As Long = 0) As Long '加载指定皮肤资源进行换肤并指定相应的色调，饱和度，亮度
    Declare Function Detach()                                                        As Long '卸载换肤
    Declare Function DetachEx(hWin As hWnd)                                          As Long '卸载指定句柄的窗体或者控件的皮肤
    Declare Function SetWindowAlpha(hWin As hWnd ,nAlpha As Long)                    As Long '设置指定窗体的透明度
    Declare Function AdjustHSV(nHue As Long = 0 ,nSat As Long = 0 ,nBri As Long = 0) As Long '调整当前皮肤的色调，饱和度，亮度
    Declare Function GetColor(hWin As hWnd ,nX As Long ,nY As Long)                  As Long '获取指定窗口或控件在nX , nY处的颜色值
    Declare Function Map(hWin As hWnd ,nType As Long)                                As Long '指定窗体和控件的换肤类型
    Declare Function SetAero(bAero As BOOLEAN)                                       As Long '设置Aero特效
    Declare Function AdjustAero(nAlpha As Long = 0 ,nShwDark As Long = 0 ,nShwSharp As Long = 0 ,nShwSize As Long = 2 ,nX As Long = 0 ,nY As Long = 0 ,nRed As Long = -1 ,nGreen As Long = -1 ,nBlue As Long = -1) As Long '设置Aero特效参数
    Declare Function SetWindowMovable(hWin As hWnd ,bMovable As BOOLEAN)                     As Long '设置窗体是否可以移动
    Declare Function SetBackColor(hWin As hWnd ,nRed As Long ,nGreen As Long ,nBlue As Long) As Long '设置控件的背景色(目前仅对单选框, 复选框, 分组框有效)
    Declare Function SetForeColor(hWin As hWnd ,nRed As Long ,nGreen As Long ,nBlue As Long) As Long '设置控件的文本颜色色(目前仅对单选框,复选框,分组框有效)
    Declare Function LockUpdate(hWin As hWnd ,bUpdate As BOOLEAN)                            As Long '用于填充表格或者列表控件数据时，重复绘制影响执行效率问题
    Declare Function SetMenuAlpha(nAlpha As Long)                                            As Long '设置菜单透明度
    Declare Function NineBlt(hDtDC As HDC, nLeft As Long, nTop As Long, nRight As Long, nBottom As Long, nMRect As Long) As Long  '绘制指定设备上下文的元素
    Declare Function SetTitleMenuBar(hWin As hWnd ,bEnable As BOOLEAN, nTMenuY As Long, nTopOffs As Long, nRightOffs As Long) As Long '设置标题菜单栏
    Declare Function SetFont(hWin As hWnd,hFnt As HFONT) As Long  '设置控件的字体
    Declare Function SetFontEx(hWin As hWnd ,szFace As String ,nHeight As Long ,nWidth As Long ,nWeight As Long ,nItalic As Long ,nUnderline As Long ,nStrikeOut As Long) As Long '设置控件的字体
    Declare Function VerifySign() As Long  '返回签名结果
End Type

Constructor clsSkinH
    hlib = DyLibLoad("SkinH.dll") '加载资源
End Constructor

Destructor clsSkinH
    Detach()                     '卸载皮肤
    If hlib Then DyLibFree(hlib) '释放资源
End Destructor

'/************************************************************************/
'/*功能  : 加载程序当前目录下的文件名为skinh.she的皮肤进行换肤          
'/*返回值: 成功返回0, 失败返回非0                                      
'/************************************************************************/
Function clsSkinH.Attach() As Long
    Dim SkinH_Attach As Function () As Long 
    If hlib Then  
        SkinH_Attach = DyLibSymbol(hlib ,"SkinH_Attach") 
        If SkinH_Attach Then 
            Return SkinH_Attach()
        End If
    End If
    Return SRET_ERROR
End Function

'/************************************************************************/
'/*功能  : 加载指定路径的皮肤进行换肤                                  
'/*参数  : sSkinFile    //皮肤文件路径
'/*        sPassword    //皮肤密钥
'/*返回值: 成功返回0, 失败返回非0                                      
'/************************************************************************/
Function clsSkinH.AttachEx(sSkinFile As String,sPassword As String) As Long 
    Dim SkinH_AttachEx As Function(As LPCTSTR ,As LPCTSTR) As Long  
    If hlib Then  
        SkinH_AttachEx = DyLibSymbol(hlib ,"SkinH_AttachEx") 
        If SkinH_AttachEx Then             
            Return SkinH_AttachEx(Cast(LPCTSTR,StrPtr(sSkinFile)),Cast(LPCTSTR,StrPtr(sPassword)))
        End If
    End If
    Return SRET_ERROR
End Function

'/************************************************************************/
'/*功能  : 加载指定路径的皮肤进行换肤并指定相应的色调，饱和度，亮度
'/*参数  : sSkinFile      //皮肤文件路径
'/*        sPassword      //皮肤密钥
'/*        nHue,          //色调，    取值范围-180-180, 默认值0
'/*        nSat,          //饱和度，  取值范围-100-100, 默认值0
'/*        nBri           //亮度，    取值范围-100-100, 默认值0
'/*返回值: 成功返回0 , 失败返回非0
'/************************************************************************/
Function clsSkinH.AttachExt(sSkinFile As String,sPassword As String,nHue As Long,nSat As Long,nBri As Long) As Long 
    Dim SkinH_AttachExt As Function(As LPCTSTR ,As LPCTSTR,As Long,As Long,As Long) As Long  
    If hlib Then  
        SkinH_AttachExt = DyLibSymbol(hlib ,"SkinH_AttachExt") 
        If SkinH_AttachExt Then             
            Return SkinH_AttachExt(Cast(LPCTSTR,StrPtr(sSkinFile)),Cast(LPCTSTR,StrPtr(sPassword)),nHue,nSat,nBri)
        End If
    End If
    Return SRET_ERROR
End Function

'/************************************************************************/
'/*功能  : 加载指定资源进行换肤并指定相应的色调，饱和度，亮度
'/*参数  : nShe,          //资源皮肤数据指针
'/*        dwSize,        //资源皮肤数据长度
'/*        sPassword      //皮肤密钥
'/*        nHue,          //色调，    取值范围-180-180, 默认值0
'/*        nSat,          //饱和度，  取值范围-100-100, 默认值0
'/*        nBri           //亮度，    取值范围-100-100, 默认值0
'/*返回值: 成功返回0 , 失败返回非0
'/************************************************************************/
Function clsSkinH.AttachRes(nShe As UByte ,dwSize As DWORD ,sPassword As String,nHue As Long,nSat As Long,nBri As Long) As Long 
    Dim SkinH_AttachRes As Function(As LPBYTE,As DWORD,As LPCTSTR ,As Long,As Long,As Long) As Long  
    If hlib Then  
        SkinH_AttachRes = DyLibSymbol(hlib ,"SkinH_AttachRes") 
        If SkinH_AttachRes Then             
            Return SkinH_AttachRes(Varptr(nShe),dwSize,Cast(LPCTSTR,StrPtr(sPassword)),nHue,nSat,nBri)
        End If
    End If
    Return SRET_ERROR 
End Function

'/************************************************************************/
'/*功能  : 加载指定皮肤资源进行换肤并指定相应的色调，饱和度，亮度
'/*参数  : sName,         //资源名
'/*        sType,         //资源类型
'/*        sPassword      //皮肤密钥
'/*        nHue,          //色调，    取值范围-180-180, 默认值0
'/*        nSat,          //饱和度，  取值范围-100-100, 默认值0
'/*        nBri           //亮度，    取值范围-100-100, 默认值0
'/*返回值: 成功返回0 , 失败返回非0
'/************************************************************************/
Function clsSkinH.AttachResEx(sName As String,sType As String,sPassword As String,nHue As Long,nSat As Long,nBri As Long) As Long 
    Dim SkinH_AttachResEx As Function(As LPCTSTR,As LPCTSTR,As LPCTSTR ,As Long,As Long,As Long) As Long  
    If hlib Then  
        SkinH_AttachResEx = DyLibSymbol(hlib ,"SkinH_AttachResEx") 
        If SkinH_AttachResEx Then             
            Return SkinH_AttachResEx(Cast(LPCTSTR,StrPtr(sName)),Cast(LPCTSTR,StrPtr(sType)),Cast(LPCTSTR,StrPtr(sPassword)),nHue,nSat,nBri)
        End If
    End If
    Return SRET_ERROR
End Function

'/************************************************************************/
'/*功能  : 卸载换肤
'/*返回值: 成功返回0 , 失败返回非0
'/************************************************************************/
Function clsSkinH.Detach() As Long 
    Dim SkinH_Detach As Function() As Long
    If hlib Then
        SkinH_Detach = DyLibSymbol(hlib ,"SkinH_Detach") 
        If SkinH_Detach Then 
            Return SkinH_Detach()
        End If    
    End If
    Return SRET_ERROR
End Function

'/************************************************************************/
'/*功能  : 卸载指定句柄的窗体或者控件的皮肤
'/*参数  : hWnd               //指定卸载皮肤的窗体或控件的句柄
'/*返回值: 成功返回0 , 失败返回非0
'/************************************************************************/
Function clsSkinH.DetachEx(hWin As hWnd) As Long
    Dim SkinH_DetachEx As Function(As hWnd) As Long
    If hlib Then
        SkinH_DetachEx = DyLibSymbol(hlib ,"SkinH_DetachEx") 
        If SkinH_DetachEx Then 
            Return SkinH_DetachEx(hWin)
        End If    
    End If
    Return SRET_ERROR
End Function

'/************************************************************************/
'/*功能  : 设置指定窗体的透明度
'/*参数  : hWnd         //窗体的句柄
'/*        nAlpha       //透明度
'/*返回值: 成功返回0 , 失败返回非0
'/************************************************************************/
Function clsSkinH.SetWindowAlpha(hWin As hWnd ,nAlpha As Long) As Long 
    Dim SkinH_SetWindowAlpha As Function(As hWnd,As Long) As Long
    If hlib Then
        SkinH_SetWindowAlpha = DyLibSymbol(hlib ,"SkinH_SetWindowAlpha") 
        If SkinH_SetWindowAlpha Then 
            Return SkinH_SetWindowAlpha(hWin,nAlpha)
        End If    
    End If
    Return SRET_ERROR  
End Function

'/************************************************************************/
'/*功能  : 调整当前皮肤的色调，饱和度，亮度
'/*参数  : nHue          //色调，    取值范围-180-180, 默认值0
'/*        nSat          //饱和度，  取值范围-100-100, 默认值0
'/*        nBri          //亮度，    取值范围-100-100, 默认值0
'/*返回值: 成功返回0 , 失败返回非0
'/************************************************************************/
Function clsSkinH.AdjustHSV(nHue As Long = 0 ,nSat As Long = 0 ,nBri As Long = 0) As Long
    Dim SkinH_AdjustHSV As Function(As Long,As Long,As Long) As Long
    If hlib Then
        SkinH_AdjustHSV = DyLibSymbol(hlib ,"SkinH_AdjustHSV") 
        If SkinH_AdjustHSV Then 
            Return SkinH_AdjustHSV(nHue,nSat,nBri)
        End If    
    End If
    Return SRET_ERROR 
End Function

'/************************************************************************/
'/*功能  : 获取指定窗口或控件在nX , nY处的颜色值
'/*参数  : hWnd                //指定窗体或控件的句柄
'/*        nX                  //横坐标
'/*        nY                  //纵坐标
'/*返回值: 成功返回0 , 失败返回非0
'/************************************************************************/
Function clsSkinH.GetColor(hWin As hWnd ,nX As Long ,nY As Long) As Long
    Dim SkinH_GetColor As Function(As hWnd,As Long,As Long) As Long
    If hlib Then
        SkinH_GetColor = DyLibSymbol(hlib ,"SkinH_GetColor") 
        If SkinH_GetColor Then 
            Return SkinH_GetColor(hWin,nX,nY)
        End If    
    End If
    Return SRET_ERROR   
End Function

'/************************************************************************/
'/*功能  : 指定窗体和控件的换肤类型
'/*参数  : hWnd                //指定窗体或控件的句柄
'/*        nType               //换肤类型
'/*返回值: 成功返回0 , 失败返回非0
'/************************************************************************/
Function clsSkinH.Map(hWin As hWnd ,nType As Long) As Long
    Dim SkinH_Map As Function(As hWnd,As Long) As Long
    If hlib Then
        SkinH_Map = DyLibSymbol(hlib ,"SkinH_Map") 
        If SkinH_Map Then 
            Return SkinH_Map(hWin,nType)
        End If    
    End If
    Return SRET_ERROR 
End Function

'/************************************************************************/
'/*功能  : 设置Aero特效
'/*参数  : bAero               //1为开启特效,0为关闭特效
'/*返回值: 成功返回0 , 失败返回非0
'/************************************************************************/
Function clsSkinH.SetAero(bAero As BOOLEAN) As Long
    Dim SkinH_SetAero As Function(As Long) As Long
    If hlib Then
        SkinH_SetAero = DyLibSymbol(hlib ,"SkinH_SetAero") 
        If SkinH_SetAero Then 
            Return SkinH_SetAero(bAero)
        End If    
    End If
    Return SRET_ERROR  
End Function

'/************************************************************************/
'/*功能  : 设置Aero特效参数
'/*参数  : nAlpha              //透明度,   0-255, 默认值0
'/*        nShwDark            //亮度,     0-255, 默认值0
'/*        nShwSharp           //锐度,     0-255, 默认值0
'/*        nShwSize            //阴影大小, 2-19,  默认值2
'/*        nX                  //水平偏移, 0-25,  默认值0 (目前不支持)
'/*        nY                  //垂直偏移, 0-25,  默认值0 (目前不支持)
'/*        nRed                //红色分量, 0-255, 默认值 -1
'/*        nGreen              //绿色分量, 0-255, 默认值 -1
'/*        nBlue               //蓝色分量, 0-255, 默认值 -1
'/*返回值: 成功返回0 , 失败返回非0
'/************************************************************************/
Function clsSkinH.AdjustAero(nAlpha As Long ,nShwDark As Long ,nShwSharp As Long ,nShwSize As Long ,nX As Long ,nY As Long ,nRed As Long ,nGreen As Long ,nBlue As Long) As Long
    Dim SkinH_AdjustAero As Function(As Long,As Long,As Long,As Long,As Long,As Long,As Long,As Long,As Long) As Long
    If hlib Then
        SkinH_AdjustAero = DyLibSymbol(hlib ,"SkinH_AdjustAero") 
        If SkinH_AdjustAero Then 
            Return SkinH_AdjustAero(nAlpha,nShwDark,nShwSharp,nShwSize,nX,nY,nRed,nGreen,nBlue)
        End If    
    End If
    Return SRET_ERROR 
End Function

'/************************************************************************/
'/*功能  : 设置窗体是否可以移动
'/*参数  : hWnd               //窗口句柄
'/*        bMovable           //0为不可移动, 1为可移动
'/*返回值: 成功返回0 , 失败返回非0
'/************************************************************************/
Function clsSkinH.SetWindowMovable(hWin As hWnd ,bMovable As BOOLEAN) As Long
    Dim SkinH_SetWindowMovable As Function(As hWnd,As Long) As Long
    If hlib Then
        SkinH_SetWindowMovable = DyLibSymbol(hlib ,"SkinH_SetWindowMovable") 
        If SkinH_SetWindowMovable Then 
            Return SkinH_SetWindowMovable(hWin,bMovable)
        End If    
    End If
    Return SRET_ERROR 
End Function

'/************************************************************************/
'/*功能  : 设置控件的背景色(目前仅对单选框, 复选框, 分组框有效)
'/*参数  : hWnd                //控件句柄
'/*        nRed                //红色分量
'/*        nGreen              //绿色分量
'/*        nBlue               //蓝色分量
'/*返回值: 成功返回0 , 失败返回非0
'/************************************************************************/
Function clsSkinH.SetBackColor(hWin As hWnd ,nRed As Long ,nGreen As Long ,nBlue As Long) As Long
    Dim SkinH_SetBackColor As Function(As hWnd,As Long,As Long,As Long) As Long
    If hlib Then
        SkinH_SetBackColor = DyLibSymbol(hlib ,"SkinH_SetBackColor") 
        If SkinH_SetBackColor Then 
            Return SkinH_SetBackColor(hWin,nRed,nGreen,nBlue)
        End If    
    End If
    Return SRET_ERROR
End Function

'/************************************************************************/
'/*功能  : 设置控件的文本颜色色(目前仅对单选框,复选框,分组框有效)
'/*参数  : hWnd                //控件句柄
'/*        nRed                //红色分量
'/*        nGreen              //绿色分量
'/*        nBlue               //蓝色分量
'/*返回值: 成功返回0 , 失败返回非0
'/************************************************************************/
Function clsSkinH.SetForeColor(hWin As hWnd ,nRed As Long ,nGreen As Long ,nBlue As Long) As Long
    Dim SkinH_SetForeColor As Function(As hWnd,As Long,As Long,As Long) As Long
    If hlib Then
        SkinH_SetForeColor = DyLibSymbol(hlib ,"SkinH_SetForeColor") 
        If SkinH_SetForeColor Then 
            Return SkinH_SetForeColor(hWin,nRed,nGreen,nBlue)
        End If    
    End If
    Return SRET_ERROR 
End Function

'/************************************************************************/
'/*功能  : 用于填充表格或者列表控件数据时，重复绘制影响执行效率问题
'/*参数  : hWnd,              //指定窗体或控件的句柄
'/*        bUpdate             //1为锁定绘制，0为解锁绘制
'/*返回值: 成功返回0 , 失败返回非0
'/************************************************************************/
Function clsSkinH.LockUpdate(hWin As hWnd,bUpdate As BOOLEAN) As Long
    Dim SkinH_LockUpdate As Function(As hWnd,As Long) As Long
    If hlib Then
        SkinH_LockUpdate = DyLibSymbol(hlib ,"SkinH_LockUpdate") 
        If SkinH_LockUpdate Then 
            Return SkinH_LockUpdate(hWin,bUpdate)
        End If    
    End If
    Return SRET_ERROR
End Function

'/************************************************************************/
'/*功能  : 设置菜单透明度
'/*参数  : nAlpha              //菜单透明度，取值范围 0 - 255
'/*返回值: 成功返回0 , 失败返回非0
'/************************************************************************/
Function clsSkinH.SetMenuAlpha(nAlpha As Long) As Long
    Dim SkinH_SetMenuAlpha As Function(As Long) As Long
    If hlib Then
        SkinH_SetMenuAlpha = DyLibSymbol(hlib ,"SkinH_SetMenuAlpha") 
        If SkinH_SetMenuAlpha Then 
            Return SkinH_SetMenuAlpha(nAlpha)
        End If    
    End If
    Return SRET_ERROR
End Function

'/************************************************************************/
'/*功能  : 绘制指定设备上下文的元素
'/*参数  : hDtDC               //目标设备上下文
'/*        left                //左上角水平坐标
'/*        top                 //左上角垂直坐标
'/*        right               //右下角水平坐标
'/*        bottom              //右下角垂直坐标
'/*        nMRect              //元素id
'/*返回值: 成功返回0 , 失败返回非0
'/************************************************************************/
Function clsSkinH.NineBlt(hDtDC As HDC ,nLeft As Long ,nTop As Long ,nRight As Long ,nBottom As Long ,nMRect As Long) As Long
    Dim SkinH_NineBlt As Function(As HDC,As Long,As Long,As Long,As Long,As Long) As Long
    If hlib Then
        SkinH_NineBlt = DyLibSymbol(hlib ,"SkinH_NineBlt") 
        If SkinH_NineBlt Then 
            Return SkinH_NineBlt(hDtDC,nLeft,nTop,nRight,nBottom,nMRect)
        End If    
    End If
    Return SRET_ERROR
End Function

'/************************************************************************/
'/*功能  : 设置标题菜单栏
'/*参数  : hWnd        //窗口句柄
'/*        bEnable     //是否设置 1为设置， 0 为取消
'/*        nTMenuY     //菜单栏高度
'/*        nTopOffs    //顶部偏移
'/*        nRightOffs  //右部偏移
'/*返回值: 成功返回0 , 失败返回非0
'/************************************************************************/
Function clsSkinH.SetTitleMenuBar(hWin As hWnd ,bEnable As BOOLEAN ,nTMenuY As Long ,nTopOffs As Long ,nRightOffs As Long) As Long
    Dim SkinH_SetTitleMenuBar As Function(As hWnd,As Long,As Long,As Long,As Long) As Long
    If hlib Then
        SkinH_SetTitleMenuBar = DyLibSymbol(hlib ,"SkinH_SetTitleMenuBar") 
        If SkinH_SetTitleMenuBar Then 
            Return SkinH_SetTitleMenuBar(hWin,bEnable,nTMenuY,nTopOffs,nRightOffs)
        End If    
    End If
    Return SRET_ERROR
End Function

'/************************************************************************/
'/*功能  : 设置控件的字体
'/*参数  : hWnd        //控件的句柄
'/*        hFont     //字体句柄
'/*返回值: 成功返回0 , 失败返回非0
'/************************************************************************/
Function clsSkinH.SetFont(hWin As hWnd ,hFnt As HFONT) As Long
    Dim SkinH_SetFont As Function(As hWnd,As HFONT) As Long
    If hlib Then
        SkinH_SetFont = DyLibSymbol(hlib ,"SkinH_SetFont") 
        If SkinH_SetFont Then 
            Return SkinH_SetFont(hWin,hFnt)
        End If    
    End If
    Return SRET_ERROR
End Function

'/************************************************************************/
'/*功能  : 返回签名结果
'/*返回值: 已签名返回1，未签名返回0
'/************************************************************************/
Function clsSkinH.VerifySign() As Long
    Dim SkinH_VerifySign As Function() As Long
    If hlib Then
        SkinH_VerifySign = DyLibSymbol(hlib ,"SkinH_VerifySign") 
        If SkinH_VerifySign Then 
            Return SkinH_VerifySign()
        End If    
    End If
    Return SRET_ERROR    
End Function































