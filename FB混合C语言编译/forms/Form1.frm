﻿#VisualFreeBasic_Form#  Version=5.8.8
Locked=0

[Form]
Name=Form1
ClassStyle=CS_VREDRAW,CS_HREDRAW,CS_DBLCLKS
ClassName=
WinStyle=WS_THICKFRAME,WS_CAPTION,WS_SYSMENU,WS_MINIMIZEBOX,WS_MAXIMIZEBOX,WS_CLIPSIBLINGS,WS_CLIPCHILDREN,WS_VISIBLE,WS_EX_WINDOWEDGE,WS_EX_CONTROLPARENT,WS_EX_LEFT,WS_EX_LTRREADING,WS_EX_RIGHTSCROLLBAR,WS_POPUP,WS_SIZEBOX
Style=3 - 常规窗口
Icon=
Caption=FreeBASIC与C语言混合编程
StartPosition=1 - 屏幕中心
WindowState=0 - 正常
Enabled=True
Repeat=False
Left=0
Top=0
Width=360
Height=236
TopMost=False
Child=False
MdiChild=False
TitleBar=True
SizeBox=True
SysMenu=True
MaximizeBox=True
MinimizeBox=True
Help=False
Hscroll=False
Vscroll=False
MinWidth=0
MinHeight=0
MaxWidth=0
MaxHeight=0
NoActivate=False
MousePass=False
TransPer=0
TransColor=SYS,25
Shadow=0 - 无阴影
BackColor=SYS,15
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Button]
Name=Command3
Index=-1
Caption=使用全局变量
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=90
Top=78
Width=174
Height=42
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command2
Index=-1
Caption=选择排序
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=180
Top=30
Width=84
Height=42
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command1
Index=-1
Caption=冒泡排序
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=90
Top=30
Width=84
Height=42
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=Command4
Index=-1
Caption=c文件内调用其他头文件
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=90
Top=126
Width=174
Height=42
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False


[AllCode]

Sub Form1_Command1_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)
    Print "冒泡排序>"
    Dim arr(10) As Long 
    '生成随机数
    Randomize  
    For i As Long = 0 To UBound(arr)
        arr(i) = Int(Rnd() * 100) + 1
    Next
    '显示排序前数组
    Print "排序前的数组>"
    For i As Long = 0 To UBound(arr)
        Print arr(i);",";    
    Next
    Print '换行 
    '进行冒泡排序
    BubbleSort(@arr(0), UBound(arr) + 1)
    '显示排序后数组
    Print "排序后的数组>"
    For i As Long = 0 To 10
        Print arr(i);",";    
    Next
    Print
End Sub

Sub Form1_Command2_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)
    Print "选择排序>"
    Dim arr(10) As Long 
    '生成随机数
    Randomize  
    For i As Long = 0 To UBound(arr)
        arr(i) = Int(Rnd() * 100) + 1
    Next
    '显示排序前数组
    Print "排序前的数组>"
    For i As Long = 0 To UBound(arr)
        Print arr(i);",";    
    Next
    Print '换行 
    '进行选择排序
    SelectSort(@arr(0), UBound(arr) + 1)
    '显示排序后数组
    Print "排序后的数组>"
    For i As Long = 0 To 10
        Print arr(i);",";    
    Next
    Print
End Sub

Sub Form1_Command3_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)
    Print "更改前";gvar
    gvar = 123456
    Print "更改后";gvar
End Sub

Sub Form1_Command4_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)
    PrintVar = 2233223
    PrintGlobalVar()
End Sub








