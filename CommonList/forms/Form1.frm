﻿#VisualFreeBasic_Form#  Version=5.8.8
Locked=0

[Form]
Name=Form1
ClassStyle=CS_VREDRAW,CS_HREDRAW,CS_DBLCLKS
ClassName=
WinStyle=WS_THICKFRAME,WS_CAPTION,WS_SYSMENU,WS_MINIMIZEBOX,WS_MAXIMIZEBOX,WS_CLIPSIBLINGS,WS_CLIPCHILDREN,WS_VISIBLE,WS_EX_WINDOWEDGE,WS_EX_CONTROLPARENT,WS_EX_LEFT,WS_EX_LTRREADING,WS_EX_RIGHTSCROLLBAR,WS_POPUP,WS_SIZEBOX
Style=3 - 常规窗口
Icon=
Caption=通用链表测试
StartPosition=1 - 屏幕中心
WindowState=0 - 正常
Enabled=True
Repeat=False
Left=0
Top=0
Width=515
Height=283
TopMost=False
Child=False
MdiChild=False
TitleBar=True
SizeBox=True
SysMenu=True
MaximizeBox=True
MinimizeBox=True
Help=False
Hscroll=False
Vscroll=False
MinWidth=0
MinHeight=0
MaxWidth=0
MaxHeight=0
NoActivate=False
MousePass=False
TransPer=0
TransColor=SYS,25
Shadow=0 - 无阴影
BackColor=SYS,15
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Label]
Name=Label1
Index=-1
Style=0 - 无边框
Caption=查找年龄：
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,25
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
Prefix=True
Ellipsis=False
Left=204
Top=216
Width=72
Height=24
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[TextBox]
Name=Text1
Index=-1
Style=3 - 凹边框
TextScrollBars=0 - 无滚动条
Text=55
Enabled=True
Visible=True
MaxLength=0
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9,0
TextAlign=0 - 左对齐
PasswordChar=
Locked=False
HideSelection=True
Multiline=False
Uppercase=False
Lowercase=False
Number=False
AutoHScroll=True
AutoVScroll=False
Left=263
Top=210
Width=72
Height=24
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
LeftMargin=0
RightMargin=0
AcceptFiles=False

[Button]
Name=cmdDefAdd
Index=-1
Caption=添加
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=192
Top=30
Width=54
Height=30
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Frame]
Name=Frame1
Index=-1
Caption=默认数据类型(以Long为例)
TextAlign=0 - 左对齐
Fillet=5
BorderWidth=1
BorderColor=SYS,16
ForeColor=SYS,8
BackColor=SYS,25
Enabled=True
Visible=True
Font=微软雅黑,9,0
Left=186
Top=0
Width=306
Height=102
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[ListBox]
Name=List1
Index=-1
Custom=
Style=0 - 单选
BStyle=3 - 凹边框
OwnDraw=0 - 系统绘制
ItemHeight=18
HasString=False
Sorted=False
NoHeight=True
MultiColumn=False
Enabled=True
Visible=True
ForeColor=SYS,8
BackColor=SYS,5
Font=微软雅黑,9,0
Left=6
Top=4
Width=174
Height=240
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False
AcceptFiles=False

[Button]
Name=cmdDefInsertIdx
Index=-1
Caption=按序号插入
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=246
Top=30
Width=72
Height=30
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=cmdDefDelIdx
Index=-1
Caption=按序号删除
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=318
Top=30
Width=72
Height=30
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=cmdDefDelAll
Index=-1
Caption=全部删除
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=390
Top=30
Width=72
Height=30
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=cmdDefSetIdx
Index=-1
Caption=按序号设置
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=192
Top=66
Width=72
Height=30
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=cmdDefFindNext
Index=-1
Caption=前往后找
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=336
Top=66
Width=72
Height=30
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=cmdDefMoveIdx
Index=-1
Caption=按序号移动
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=264
Top=66
Width=72
Height=30
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=cmdDefFindPrev
Index=-1
Caption=后往前找
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=408
Top=66
Width=72
Height=30
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=cmdFindPrev
Index=-1
Caption=后往前找
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=414
Top=204
Width=72
Height=30
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=cmdMoveIdx
Index=-1
Caption=按序号移动
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=270
Top=174
Width=72
Height=30
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=cmdFindNext
Index=-1
Caption=前往后找
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=342
Top=204
Width=72
Height=30
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=cmdSetIdx
Index=-1
Caption=按序号设置
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=198
Top=174
Width=72
Height=30
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=cmdDelAll
Index=-1
Caption=全部删除
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=396
Top=138
Width=72
Height=30
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=cmdDelIdx
Index=-1
Caption=按序号删除
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=324
Top=138
Width=72
Height=30
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Button]
Name=cmdInsertIdx
Index=-1
Caption=按序号插入
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=252
Top=138
Width=72
Height=30
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False

[Frame]
Name=Frame2
Index=-1
Caption=自定义数据类型(Type persion)
TextAlign=0 - 左对齐
Fillet=5
BorderWidth=1
BorderColor=SYS,16
ForeColor=SYS,8
BackColor=SYS,25
Enabled=True
Visible=True
Font=微软雅黑,9,0
Left=186
Top=108
Width=306
Height=132
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
ToolTip=
ToolTipBalloon=False

[Button]
Name=cmdAdd
Index=-1
Caption=添加
TextAlign=1 - 居中
Ico=
Enabled=True
Visible=True
Default=False
OwnDraw=False
MultiLine=False
Font=微软雅黑,9,0
Left=198
Top=138
Width=54
Height=30
Layout=0 - 不锚定
MousePointer=0 - 默认
Tag=
Tab=True
ToolTip=
ToolTipBalloon=False


[AllCode]
'通用链表类，默认为TLIST + 你定义的数据类型名。如定义的数据是Long,则链表类型为TLISTLong。
Dim Shared deflst As TLISTLong 'Long 型数据

'添加数据
Sub Form1_cmdDefAdd_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)
    For i As Long = 0 To 9
        deflst.Add i + 1 
    Next
'更新显示列表
    List1.Clear
    For i As Long = 0 To deflst.GetSize - 1
        List1.AddItem deflst.GetValueIndex(i)
    Next
End Sub

'插入
Sub Form1_cmdDefInsertIdx_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)
    deflst.InsertItemIndex(0, -1)
'更新显示列表
    List1.Clear
    For i As Long = 0 To deflst.GetSize - 1
        List1.AddItem deflst.GetValueIndex(i)
    Next     
End Sub

'删除
Sub Form1_cmdDefDelIdx_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)
    deflst.DeleteItemIndex(0)
'更新显示列表
    List1.Clear
    For i As Long = 0 To deflst.GetSize - 1
        List1.AddItem deflst.GetValueIndex(i)
    Next       
End Sub

'全部删除
Sub Form1_cmdDefDelAll_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)
    deflst.DeleteAll()
'更新显示列表
    List1.Clear
    For i As Long = 0 To deflst.GetSize - 1
        List1.AddItem deflst.GetValueIndex(i)
    Next   
End Sub

'设置/更新数据
Sub Form1_cmdDefSetIdx_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)
    deflst.SetValueIndex(0, 100)
'更新显示列表
    List1.Clear
    For i As Long = 0 To deflst.GetSize - 1
        List1.AddItem deflst.GetValueIndex(i)
    Next     
End Sub

'移动/交换数据
Sub Form1_cmdDefMoveIdx_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)
    deflst.MoveItemIndex(0, 1)
'更新显示列表
    List1.Clear
    For i As Long = 0 To deflst.GetSize - 1
        List1.AddItem deflst.GetValueIndex(i)
    Next    
End Sub

'查找节点（从前到后），根据节点进行插入、删除、设置、移动、获取数据等操作
Sub Form1_cmdDefFindNext_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)
    Dim f As Long = 5 '想要查找的数据
    Dim deflstnode As TLISTNODELong Ptr = deflst.GetFirst
    While deflstnode
        If deflstnode->xValue = f Then Exit While '找到就跳出来
        deflstnode = deflst.GetNext(deflstnode)
    Wend
    If deflstnode Then deflst.InsertItem(deflstnode, 200) '
'更新显示列表
    List1.Clear
    For i As Long = 0 To deflst.GetSize - 1
        List1.AddItem deflst.GetValueIndex(i)
    Next
End Sub

'查找节点（从后到前），根据节点进行插入、删除、设置、移动、获取数据等操作
Sub Form1_cmdDefFindPrev_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)
    Dim f As Long = 5 '想要查找的数据
    Dim deflstnode As TLISTNODELong Ptr = deflst.GetLast
    While deflstnode
        If deflstnode->xValue = f Then Exit While '找到就跳出来
        deflstnode = deflst.GetPrev(deflstnode)
    Wend
    If deflstnode Then deflst.MoveItem(deflstnode, deflst.GetFirst) '    
'更新显示列表
    List1.Clear
    For i As Long = 0 To deflst.GetSize - 1
        List1.AddItem deflst.GetValueIndex(i)
    Next
End Sub

'通用链表类也可以使用自定义类型，默认为TLIST + 自定义类型名。如定义的数据是persion,则链表类型为TLISTPersion。
Dim Shared lst As TLISTPersion '自定义Persion数据

Sub Form1_cmdAdd_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)
    Dim s As String = "甲乙丙丁戊己庚辛任癸"
    Dim t As persion
    Randomize
    For i As Long = 0 To 9
        t.sname = "路人" & Mid(s,1 + i * 2,2) 
        t.age = Int(Rnd * 50) + 20 '20 - 70岁
        lst.Add(t)
    Next
'更新显示列表
    List1.Clear
    For i As Long = 0 To lst.GetSize - 1
        t = lst.GetValueIndex(i)
        List1.AddItem i & " - " & t.sname & " : " & t.age & "岁" 
    Next
End Sub

Sub Form1_cmdInsertIdx_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)
    Dim t As persion = ("路人x",34)
    lst.InsertItemIndex(0,t)
'更新显示列表
    List1.Clear
    For i As Long = 0 To lst.GetSize - 1
        t = lst.GetValueIndex(i)
        List1.AddItem i & " - " & t.sname & " : " & t.age & "岁" 
    Next     
End Sub

Sub Form1_cmdDelIdx_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)
    Dim t As persion
    lst.DeleteItemIndex(0)
'更新显示列表
    List1.Clear
    For i As Long = 0 To lst.GetSize - 1
        t = lst.GetValueIndex(i)
        List1.AddItem i & " - " & t.sname & " : " & t.age & "岁" 
    Next     
End Sub

Sub Form1_cmdDelAll_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)
    Dim t As persion
    lst.DeleteAll()
'更新显示列表
    List1.Clear
    For i As Long = 0 To lst.GetSize - 1
        t = lst.GetValueIndex(i)
        List1.AddItem i & " - " & t.sname & " : " & t.age & "岁" 
    Next  
End Sub

Sub Form1_cmdSetIdx_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)
    Dim t As persion = lst.GetValueIndex(0) '获取原数据
    t.age = 200                             '修改需要修改的部分
    lst.SetValueIndex(0,t)                  '设置数据（如果是全新的数据，直接设置就行，不需要先获取）
'更新显示列表
    List1.Clear
    For i As Long = 0 To lst.GetSize - 1
        t = lst.GetValueIndex(i)
        List1.AddItem i & " - " & t.sname & " : " & t.age & "岁" 
    Next     
End Sub

Sub Form1_cmdMoveIdx_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)
    Dim t As persion
    lst.MoveItemIndex(0,2)
'更新显示列表
    List1.Clear
    For i As Long = 0 To lst.GetSize - 1
        t = lst.GetValueIndex(i)
        List1.AddItem i & " - " & t.sname & " : " & t.age & "岁" 
    Next      
End Sub

Sub Form1_cmdFindNext_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)
    Dim t As persion
    Dim f As Long = Val(Text1.Text) '想要查找的数据
    Dim lstnode As TLISTNODEPersion Ptr = lst.GetFirst
    While lstnode
        t = lstnode->xValue
        If t.age = f Then Exit While '找到就跳出来
        lstnode = lst.GetNext(lstnode)
    Wend
    If lstnode Then lst.DeleteItem(lstnode) '
'更新显示列表
    List1.Clear
    For i As Long = 0 To lst.GetSize - 1
        t = lst.GetValueIndex(i)
        List1.AddItem i & " - " & t.sname & " : " & t.age & "岁" 
    Next
End Sub

Sub Form1_cmdFindPrev_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd)
    Dim t As persion
    Dim f As Long = Val(Text1.Text) '想要查找的数据
    Dim lstnode As TLISTNODEPersion Ptr = lst.GetLast
    While lstnode
        t = lstnode->xValue
        If t.age = f Then Exit While '找到就跳出来
        lstnode = lst.GetPrev(lstnode)
    Wend
    t.sname = "小明"
    t.age = 500
    If lstnode Then lst.SetValue(lstnode,t) '
'更新显示列表
    List1.Clear
    For i As Long = 0 To lst.GetSize - 1
        t = lst.GetValueIndex(i)
        List1.AddItem i & " - " & t.sname & " : " & t.age & "岁" 
    Next
End Sub











































