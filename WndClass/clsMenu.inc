Type Class_Menu
Private : 
    hWndControl As.HMENU '控件句柄
    sWndParent As hWnd 
Public : 
    Declare Constructor
    Declare Destructor
    Declare Property hMenu() As.HMENU                  '返回/设置控件句柄,创建新菜单 =CreatePopupMenu()
    Declare Property hMenu(hMenuNew As.HMENU)
    Declare Property hWndForm() As.hWnd         '返回/设置控件所在的窗口句柄，主要用于多开窗口后，要使用控件前，必须先指定控件所在的窗口句柄，才能正常使用控件。
    Declare Property hWndForm(ByVal hWndParent As.hWnd) '获取控件所在的窗口句柄
    Declare Property TextID(idItem As Long) As String    '按ID方式：返回/设置菜单项的文本，
    Declare Property TextID(idItem As Long,sText As String)
    Declare Property TextPos(nPos As Long) As String     '按位置方式：返回/设置菜单项的文本，Pos从0开始
    Declare Property TextPos(nPos As Long, sText  As String)
    Declare Property EnabledID(idItem As Long) As Boolean     '按ID方式：返回/设置菜单项是否允许操作。{=.True.False} 
    Declare Property EnabledID(idItem As Long, bValue As Boolean)
    Declare Property EnabledPos(nPos As Long) As Boolean     '按位置方式：返回/设置菜单项是否允许操作。{=.True.False} 
    Declare Property EnabledPos(nPos As Long, bValue As Boolean)
    Declare Property CheckID(idItem As Long) As Boolean     '按ID方式：返回/设置菜单项是否选中。{=.True.False}
    Declare Property CheckID(idItem As Long, bValue As Boolean)
    Declare Property CheckPos(nPos As Long) As Boolean     '按位置方式：返回/设置菜单项是否选中。{=.True.False}
    Declare Property CheckPos(nPos As Long, bValue As Boolean)
    Declare Sub PopupMenu(hWndParent As hWnd)       '在当前鼠标位置，弹出此菜单，hWndParent 是接收菜单事件的窗口。
    Declare Sub AddText(sText As String, idItem As Long)       '增加1个菜单项，文本
    Declare Sub AddSeparator()       '增加1个菜单项，分隔线
    Declare Sub AddChild(hMenuNew As.HMENU, sText As String)       '增加1个子菜单项，把其它菜单作为子菜单
    Declare Sub InsertTextID(cid As Long, sText As String, idItem As Long)       '在cid位置插入1个菜单项，文本
    Declare Sub InsertSeparatorID(cid As Long)       '在cid位置插入增加1个菜单项，分隔线
    Declare Sub InsertChildID(cid As Long,hMenuNew As.HMENU, sText As String)       '在cid位置插入1个子菜单项，把其它菜单作为子菜单
    Declare Sub InsertTextPos(nPos As Long, sText As String, idItem As Long)       '在nPos位置(从0开始的索引)插入1个菜单项，文本
    Declare Sub InsertSeparatorPos(nPos As Long)       '在nPos位置(从0开始的索引)插入增加1个菜单项，分隔线
    Declare Sub InsertChildPos(nPos As Long, hMenuNew As.HMENU, sText As String)       '在nPos位置(从0开始的索引)插入1个菜单项，子菜单
    Declare Sub AddMenuBmpID(idItem As Long, nBmp As HBITMAP)  '给ID加BMP图标，BMP句柄不可以销毁，不然就画不上了
    Declare Sub AddMenuBmpPos(nPos As Long, nBmp As HBITMAP)   '在nPos位置(从0开始的索引)BMP加图标，BMP句柄不可以销毁，不然就画不上了
    Declare Sub DeleteMenuID(idItem As Long)  '删除这个ID的菜单项
    Declare Sub DeleteMenuPos(nPos As Long)   '删除这个nPos位置(从0开始的索引)的菜单项
    
Private : 
    Declare Function GetState(idItem As Long, yesPos As Boolean) As Long   '检索指定的菜单项的状态。
    Declare Function SetState(idItem As Long, nState As Long, yesPos As Boolean) As Long   '设置指定的菜单项的状态。
    
End Type

'
Constructor Class_Menu
  
End Constructor

Destructor Class_Menu
  
End Destructor
Property Class_Menu.hMenu() As.HMENU                    '句柄
  Return hWndControl
End Property
Property Class_Menu.hMenu(ByVal hWndNew As.HMENU)        '句柄
  hWndControl = hWndNew
End Property
Property Class_Menu.hWndForm() As.hWnd         '用于多开窗口时，要使用控件前，必须先指定控件的父句柄
  Return sWndParent
End Property
Property Class_Menu.hWndForm(ByVal hWndParent As .hWnd)         '用于多开窗口时，要使用控件前，必须先指定控件的父句柄
  hWndControl = GetMenu(hWndParent)
  sWndParent = hWndParent
End Property
Property Class_Menu.TextID(idItem As Long) As String    '按ID方式:返回/设置菜单项的文本
   Dim mType As MenuItemInfo
   Dim sText As String
   
   mType.cbSize     = SizeOf(mType)
   mType.fMask      = MIIM_STRING
   mType.dwTypeData = 0
   
   ' Get the size of the string first 
   If GetMenuItemInfo( hWndControl, idItem, False, VarPtr(mType) ) Then
      mType.cch = mType.cch + 1  ' make room for the trailing Nul.
      sText = Space(mType.cch)
   
      ' Now get the actual string into the buffer
      mType.dwTypeData = StrPtr(sText)
      If GetMenuItemInfo( hWndControl, idItem, False, VarPtr(mType) ) Then
         Return  RTrim(sText, Chr(0))
      End If
   
   End If   
End Property
Property Class_Menu.TextID(idItem As Long, sText  As String)
  Dim mType As MenuItemInfo
  
  mType.cbSize = SizeOf(mType)
  mType.fMask = MIIM_STRING
  mType.dwTypeData = StrPtr(sText)
  mType.cch = Len(sText)
  
  SetMenuItemInfo hWndControl, idItem, False, VarPtr(mType)
  DrawMenuBar sWndParent
End Property
Property Class_Menu.TextPos(nPos As Long) As String     '按位置方式:返回/设置菜单项的文本，Pos从0开始
   Dim mType As MenuItemInfo
   Dim sText As String
   
   mType.cbSize     = SizeOf(mType)
   mType.fMask      = MIIM_STRING
   mType.dwTypeData = 0
   
   ' Get the size of the string first 
   If GetMenuItemInfo( hWndControl, nPos, True , VarPtr(mType) ) Then
      mType.cch = mType.cch + 1  ' make room for the trailing Nul.
      sText = Space(mType.cch)
   
      ' Now get the actual string into the buffer
      mType.dwTypeData = StrPtr(sText)
      If GetMenuItemInfo( hWndControl, nPos, True, VarPtr(mType) ) Then
         Return  RTrim(sText, Chr(0))
      End If
   
   End If   
End Property
Property Class_Menu.TextPos(nPos As Long, sText  As String)
  Dim mType As MenuItemInfo
  
  mType.cbSize = SizeOf(mType)
  mType.fMask = MIIM_STRING
  mType.dwTypeData = StrPtr(sText)
  mType.cch = Len(sText)
  
  SetMenuItemInfo hWndControl, nPos, True, VarPtr(mType)
  DrawMenuBar sWndParent
End Property
Property Class_Menu.EnabledID(idItem As Long) As Boolean     '按ID方式:返回/设置菜单项是否允许操作。{=.True.False}
  Dim aa As Long = This.GetState(idItem, False)
  If aa And MFS_DISABLED = MFS_DISABLED Then
      Return False
  Else
      Return True
  End If
End Property
Property Class_Menu.EnabledID(idItem As Long,bValue As Boolean)
   If bValue Then 
        This.SetState idItem,MFS_ENABLED,False
   Else
        This.SetState idItem,MFS_DISABLED,False
   End If
   DrawMenuBar sWndParent 
End Property
Property Class_Menu.EnabledPos(nPos As Long) As Boolean     '按位置方式:返回/设置菜单项是否允许操作。{=.True.False}
  Dim aa As Long = This.GetState(nPos, True)
  If aa And MFS_DISABLED = MFS_DISABLED Then
      Return False
  Else
      Return True
  End If
End Property
Property Class_Menu.EnabledPos(nPos As Long,bValue As Boolean)
   If bValue Then 
        This.SetState nPos,MFS_ENABLED,True
   Else
        This.SetState nPos,MFS_DISABLED,True
   End If
   DrawMenuBar sWndParent 
End Property
Property Class_Menu.CheckID(idItem As Long) As Boolean     '按ID方式:返回/设置菜单项是否选中。{=.True.False}
  Dim aa As Long = This.GetState(idItem, False)
  If aa And MFS_UNCHECKED = MFS_UNCHECKED Then
      Return False
  Else
      Return True
  End If
End Property
Property Class_Menu.CheckID(idItem As Long,bValue As Boolean)
   If bValue Then 
        This.SetState idItem,MFS_CHECKED,False
   Else
        This.SetState idItem,MFS_UNCHECKED,False
   End If
   DrawMenuBar sWndParent 
End Property
Property Class_Menu.CheckPos(nPos As Long) As Boolean     '按位置方式:返回/设置菜单项是否选中。{=.True.False}
  Dim aa As Long = This.GetState(nPos, True)
  If aa And MFS_UNCHECKED = MFS_UNCHECKED Then
      Return False
  Else
      Return True
  End If
End Property
Property Class_Menu.CheckPos(nPos As Long,bValue As Boolean)
   If bValue Then 
        This.SetState nPos,MFS_CHECKED,False
   Else
        This.SetState nPos,MFS_UNCHECKED,False
   End If
   DrawMenuBar sWndParent 
End Property
Sub Class_Menu.PopupMenu( hWndParent As HWnd )       '在当前鼠标位置，弹出此菜单，hWndParent 是接收菜单事件的窗口。
  Dim  P As  Point
  GetCursorPos @p
  TrackPopupMenu hWndControl, 0, p.x, p.y, 0, hWndParent, Null '比方说弹出菜单
End Sub
Sub Class_Menu.AddText(sText As String,idItem As Long)       '增加1个菜单项，文本
  AppendMenu hWndControl, MF_STRING, idItem, sText
  DrawMenuBar sWndParent 
End Sub
Sub Class_Menu.AddSeparator()       '增加1个菜单项，分隔线
  AppendMenu hWndControl, MF_SEPARATOR, 0, ""
  DrawMenuBar sWndParent 
End Sub
Sub Class_Menu.AddChild(hMenuNew As .HMENU,sText As String)       '增加1个菜单项，子菜单
  AppendMenu hWndControl, MF_POPUP, Cast(UINT_PTR, hMenuNew), sText
  DrawMenuBar sWndParent 
End Sub
Sub Class_Menu.InsertTextID(cid As Long,sText As String,idItem As Long)       '在ID插入1个菜单项，文本
  InsertMenu  hWndControl, cid, MF_BYCOMMAND Or MF_STRING, idItem, sText
  DrawMenuBar sWndParent 
End Sub
Sub Class_Menu.InsertSeparatorID(cid As Long)       '在ID插入增加1个菜单项，分隔线
  InsertMenu  hWndControl, cid, MF_SEPARATOR, 0, ""
  DrawMenuBar sWndParent 
End Sub
Sub Class_Menu.InsertChildID(cid As Long,hMenuNew As .HMENU,sText As String)       '在ID插入1个菜单项，子菜单
  InsertMenu hWndControl,cid, MF_POPUP, Cast(Integer , hMenuNew),sText
  DrawMenuBar sWndParent 
End Sub
Sub Class_Menu.InsertTextPos(nPos As Long,sText As String,idItem As Long)       '在位置插入1个菜单项，文本
  InsertMenu  hWndControl, nPos, MF_BYPOSITION Or MF_STRING, idItem, sText
  DrawMenuBar sWndParent 
End Sub
Sub Class_Menu.InsertSeparatorPos(nPos As Long)       '在位置插入增加1个菜单项，分隔线
  InsertMenu  hWndControl, nPos,MF_BYPOSITION Or MF_SEPARATOR, 0, ""
  DrawMenuBar sWndParent 
End Sub
Sub Class_Menu.InsertChildPos(nPos As Long,hMenuNew As .HMENU,sText As String)       '在位置插入1个菜单项，子菜单
  InsertMenu hWndControl,nPos,MF_BYPOSITION Or MF_POPUP, Cast(Integer, hMenuNew), sText
  DrawMenuBar sWndParent 
End Sub
Sub Class_Menu.AddMenuBmpID(idItem As Long,nBmp As HBITMAP )  '给ID加BMP图标，BMP句柄不可以销毁，不然就画不上了
  SetMenuItemBitmaps( hWndControl, idItem, MF_BYCOMMAND,nBmp, nBmp)
  DrawMenuBar sWndParent 
End Sub
Sub Class_Menu.AddMenuBmpPos(nPos As Long,nBmp As HBITMAP )   '在位置BMP加图标，BMP句柄不可以销毁，不然就画不上了 
  SetMenuItemBitmaps( hWndControl, nPos, MF_BYPOSITION,nBmp, nBmp)
  DrawMenuBar sWndParent 
End Sub
Sub Class_Menu.DeleteMenuID(idItem As Long)  '删除这个ID的菜单项
  DeleteMenu( hWndControl, idItem, MF_BYCOMMAND)
  DrawMenuBar sWndParent 
End Sub
Sub Class_Menu.DeleteMenuPos(nPos As Long)   '删除这个位置的菜单项
  DeleteMenu( hWndControl, nPos, MF_BYPOSITION)
  DrawMenuBar sWndParent 
End Sub
Function Class_Menu.GetState(idItem As Long,yesPos As Boolean) As Long   '检索指定的菜单项的状态。
   Dim mType As MenuItemInfo
   
   mType.cbSize = SizeOf(mType)
   mType.fMask  = MIIM_STATE
   
   If GetMenuItemInfo( hMenu, idItem, yesPos, VarPtr(mType) ) Then
      Function = mType.fState
   End If   
End Function
Function Class_Menu.SetState(idItem As Long,nState As Long,yesPos As Boolean) As Long   '设置指定的菜单项的状态。
   Dim mType As MenuItemInfo
   
   mType.cbSize = SizeOf(mType)
   mType.fMask  = MIIM_STATE
   mType.fState = nState    
   
   'MFS_ENABLED, MFS_GRAYED, MFS_DISABLED, MFS_CHECKED, MFS_UNCHECKED 
   'MFS_DEFAULT, MFS_HILITE, MFS_UNHILITE

   ' Function returns 0 on fail.
   Function = SetMenuItemInfo( hMenu, idItem, yesPos, VarPtr(mType) ) 
End Function

