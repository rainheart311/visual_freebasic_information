Type Class_DateTimePicker Extends Class_Control
    Declare Constructor
    Declare Destructor
    Declare Property DateTime() As SYSTEMTIME           '返回/设置时间
    Declare Property DateTime(ByVal bValue As SYSTEMTIME)
    Declare Property MaxDate() As SYSTEMTIME           '返回/设置允许最大时间。
    Declare Property MaxDate(ByVal bValue As SYSTEMTIME)
    Declare Property MinDate() As SYSTEMTIME           '返回/设置允许最小时间。
    Declare Property MinDate(ByVal bValue As SYSTEMTIME)
    Declare Function FormatString(sz As String) As Boolean '设置显示格式字符串
    Declare Property MonthCalColor(iColor As Long) As ULong      '返回/设置部分颜色，{1.MCSC_BACKGROUND 月份之间显示的背景颜色.MCSC_MONTHBK 月份内显示的背景颜色.MCSC_TEXT 月内显示文字的颜色.MCSC_TITLEBK 标题中显示的背景颜色.MCSC_TITLETEXT 标题中用于显示文本的颜色.MCSC_TRAILINGTEXT 标题日和尾日文本的颜色}
    Declare Property MonthCalColor(iColor As Long, clr As ULong)
#if _WIN32_WINNT = &h0602 
    Declare Property MonthCalStyle() As ULong            '返回/设置控件的样式，详细见WINFBX
    Declare Property MonthCalStyle(ByVal dwStyle As ULong)
#endif
End Type
'----------------------------------------------------------------------------------------------------------------------------------------------------------------
Constructor Class_DateTimePicker
  
End Constructor

Destructor Class_DateTimePicker
  
End Destructor
Property Class_DateTimePicker.DateTime() As SYSTEMTIME           '返回/设置时间
  Dim st As SYSTEMTIME
  DateTime_GetSystemtime(hWndControl, @st)
  Return st
End Property
Property Class_DateTimePicker.DateTime(ByVal bValue As SYSTEMTIME)
  DateTime_SetSystemtime(hWndControl, GDT_VALID, @bValue)
End Property
Property Class_DateTimePicker.MaxDate() As SYSTEMTIME           '返回/设置允许最大时间。
  Dim st(1) As SYSTEMTIME
  DateTime_GetSystemtime(hWndControl, @st(0))
  Return st(1)
End Property
Property Class_DateTimePicker.MaxDate(ByVal bValue As SYSTEMTIME)
  Dim st(1) As SYSTEMTIME
  st(1) = bValue
  DateTime_SetSystemtime(hWndControl, GDTR_MAX, @bValue)
End Property
Property Class_DateTimePicker.MinDate() As SYSTEMTIME           '返回/设置允许最小时间。
  Dim st(1) As SYSTEMTIME
  DateTime_GetSystemtime(hWndControl, @st(0))
  Return st(0)
End Property
Property Class_DateTimePicker.MinDate(ByVal bValue As SYSTEMTIME)
  Dim st(1) As SYSTEMTIME
  st(0) = bValue
  DateTime_SetSystemtime(hWndControl, GDTR_MIN, @bValue)
End Property
Function Class_DateTimePicker.FormatString(sz As String) As Boolean '设置显示格式字符串
  Return Cast(Boolean, SendMessage(hWndControl, DTM_SETFORMAT, 0, Cast(lParam, StrPtr(sz))))
End Function

Property Class_DateTimePicker.MonthCalColor(iColor As Long) As ULong      '返回/设置部分颜色
  Return SendMessage(hWndControl, DTM_GETMCCOLOR, iColor, 0)
End Property
Property Class_DateTimePicker.MonthCalColor(iColor As Long, clr As ULong)
  SendMessage(hWndControl, DTM_SETMCCOLOR, iColor, clr)
End Property

#if _WIN32_WINNT = &h0602 
Property Class_DateTimePicker.MonthCalStyle() As ULong            '返回/设置控件的样式，详细见WINFBX
    Return SendMessage(hWndControl, DTM_FIRST + 12, 0, 0)
End Property
Property Class_DateTimePicker.MonthCalStyle(ByVal dwStyle As ULong)
    SendMessage(hWndControl, DTM_FIRST + 11, 0, Cast(lParam, dwStyle))
End Property
#endif
