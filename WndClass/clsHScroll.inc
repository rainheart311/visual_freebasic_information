Type Class_HScroll Extends Class_Control
    Declare Constructor
    Declare Destructor
    Declare Property Value() As Long                 '返回/设置滚动值
    Declare Property Value(ByVal bValue As Long)     '
    Declare Property nMax() As Long                 '返回/设置滚动条最大值
    Declare Property nMax(ByVal posMax As Long)     '
    Declare Property nMin() As Long                 '返回/设置滚动最小值
    Declare Property nMin(ByVal posMin As Long)     '
End Type

'----------------------------------------------------------------------------------------------------------------------------------------------------------------
Constructor Class_HScroll
  
End Constructor

Destructor Class_HScroll
  
End Destructor
Property Class_HScroll.Value() As Long                 '返回/设置滚动值
  Return GetScrollPos(hWndControl, SB_CTL)
End Property
Property Class_HScroll.Value(ByVal bValue As Long)     '
  SetScrollPos(hWndControl, SB_CTL, bValue, True)
End Property
Property Class_HScroll.nMax() As Long                 '返回/设置滚动条最大值
  Dim lpposMin As Long, lpposMax As Long
  GetScrollRange(hWndControl, SB_CTL, @lpposMin, @lpposMax)
  Return lpposMax
End Property
Property Class_HScroll.nMax(ByVal posMax As Long) 
  Dim lpposMin As Long, lpposMax As Long
  GetScrollRange(hWndControl, SB_CTL, @lpposMin, @lpposMax)
  SendMessage(hWndControl, SBM_SETRANGEREDRAW, lpposMin, posMax)
End Property
Property Class_HScroll.nMin() As Long                 '返回/设置滚动最小值
  Dim lpposMin As Long, lpposMax As Long
  GetScrollRange(hWndControl, SB_CTL, @lpposMin, @lpposMax)
  Return lpposMin
End Property
Property Class_HScroll.nMin(ByVal posMin As Long)     '
  Dim lpposMin As Long, lpposMax As Long
  GetScrollRange(hWndControl, SB_CTL, @lpposMin, @lpposMax)
  SendMessage(hWndControl, SBM_SETRANGEREDRAW, posMin, lpposMax)
End Property

