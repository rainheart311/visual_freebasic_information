Type Class_StatusBar Extends Class_Control
Private : 
    m_Width(Any) As Long
Public : 
    Declare Constructor
    Declare Destructor
	
	Declare Sub Add(nWidth As Long = 100,sText As string = "")
    Declare Sub Remove(Index As Integer)
    Declare Sub Clear()
    Declare Sub AutoSize()
    Declare Function SetBackColor(nColor As COLORREF) As COLORREF  '在状态栏中设置的背景色。返回前一种背景颜色，如果颜色背景为默认颜色 =CLR_DEFAULT 。 
	Declare Function GetTextDrawingMode(iPart As Long) As Long     '检索用于绘制文本操作的类型。

	Declare Property Count() As long                               '返回部件总数
    Declare Property Text(iPart As Long) As String                 '返回/设置文本。
    Declare Property Text(iPart As Long,nText As String)
    Declare Property TipText(iPart As Long) As String              '返回/设置工具提示文本
    Declare Property TipText(iPart As Long,nText As String)
    Declare Property Simple()  As Boolean                          '返回/设置简单模式。{=.True.False} 
    Declare Property Simple(fSimple As Boolean)
    Declare Property Icon(iPart As Long) As HICON                  '返回/设置在状态栏中的图标。iPart：从0开始的索引(简单模式状态栏为-1),如果成功返回图标的句柄，否则为 NULL。
    Declare Property Icon(iPart As Long, hIcon As HICON)           '如果设置图标的值为 NULL，将从 iPart 中删除该图标。
	Declare Property PartWidth(iPart As Long) As Long
    Declare Property PartWidth(iPart As Long,nWidth As Long) 
End Type


'----------------------------------------------------------------------------------------------------------------------------------------------------------------
Constructor Class_StatusBar
  
End Constructor

Destructor Class_StatusBar
  
End Destructor

Sub Class_StatusBar.Add(nWidth As Long = 100,sText As string = "")
	Dim nCount As Long = Count
	ReDim Preserve m_Width(Count) As Long

	If nCount = 1 Then
		m_Width(0) = nWidth
		m_Width(1) = -1
	Else
		For i As Long = 0 To nCount - 2
		   	m_Width(nCount - 1) += m_Width(i) '当前实际宽度 = 前n个宽度之和 + 当前宽度
		Next i
		m_Width(nCount - 1) += nWidth         '加当前宽度
		m_Width(nCount) = -1                  '最后一个为-1，表示剩下所有
	EndIf

    SendMessage(hWndControl,SB_SETPARTS,nCount + 1,Cast(LPARAM,@m_Width(0)))
    Text(nCount - 1) = sText
End Sub

Sub Class_StatusBar.Remove(Index As Integer)
    Dim nCount As Long = Count
    Dim nWidth As Long
    
    If UBound(m_Width) = -1 Then SendMessage(hWndControl,SB_GETPARTS,0,Cast(LPARAM,@m_Width(0)))
    
    If UBound(m_Width) = -1 Then 
    	Print "no parts in statusbar"
    	Exit Sub
    EndIf
    If Index < nCount Then '在范围内
        nWidth = IIf(Index = 0,m_Width(Index),m_Width(Index) - m_Width(Index - 1)) '计算当前栏的宽度

        If Index < nCount - 1 Then '不是最后一个
            For i As Long = Index To nCount - 2 '将后面的宽度移动到前面
                m_Width(i) = m_Width(i + 1) - nWidth '减去当前栏的宽度
            Next i
        End If
        ReDim Preserve m_Width(nCount - 2) As Long
        SendMessage(hWndControl,SB_SETPARTS,nCount - 1,Cast(LPARAM,@m_Width(0)))
    Else
    	Print "Class_StatusBar.Remove:Index out of range"
    EndIf
End Sub

Sub Class_StatusBar.Clear
    ReDim Preserve m_Width(0) As Long
    m_Width(0) = -1
    SendMessage(hWndControl,SB_SETPARTS,1,Cast(LPARAM,@m_Width(0)))
    Text(0) = ""
End Sub

Sub Class_StatusBar.AutoSize()
	If IsWindow(hWndControl) Then
		MoveWindow(hWndControl,0,0,0,0,TRUE)        'Auto Size StatusBar
	Else
		Print "Class_StatusBar.AutoSize:the hwnd is Invalid"
	EndIf
End Sub

Function Class_StatusBar.SetBackColor(nColor As COLORREF) As COLORREF
    If IsWindow(hWndControl) Then
    	Return SendMessage(hWndControl, SB_SETBKCOLOR, 0,nColor)
    Else
    	Print "Class_StatusBar.SetBackColor:the hwnd is Invalid"
    EndIf
End Function

Function Class_StatusBar.GetTextDrawingMode(iPart As Long) As Long   '检索用于绘制文本操作的类型。
   Dim dwResult As Long
   dwResult = SendMessage (hWndControl, SB_GETTEXTLENGTH, iPart, 0)
   Function = HiWord(dwResult)
'   返回值：
'
'      值       意义
'-------------- -------------------------------------------------------
'0               与要显示边框绘制文本低于窗口的飞机。
'SBT_NOBORDERS   无边界绘制文本。
'SBT_OWNERDRAW   由父窗口绘制文本。
'SBT_POPOUT      文本绘制带边框显示窗口的飞机比高。
'SBT_RTLREADING  显示文本在希伯来语或阿拉伯语的系统上使用从右向左的阅读顺序。
End Function

Property Class_StatusBar.Count() As Long
    Return SendMessage(hWndControl, SB_GETPARTS, 0, 0)
End Property

Property Class_StatusBar.Text(iPart As Long) As String                  '返回/设置文本。
   Dim nLen   As Long
   Dim bb As String
   nLen = LoWord( SendMessage (hWndControl, SB_GETTEXTLENGTH, iPart, 0) )
   bb = Space(nLen + 1) 'ensure room for the trailing $Nul
   SendMessage hWndControl, SB_GETTEXT, iPart, Cast(lParam, StrPtr(bb))
   Return Mid(bb,1, nLen)
End Property

Property Class_StatusBar.Text(iPart As Long,nText As String)
   SendMessage (hWndControl, SB_SETTEXT, iPart , Cast(lParam, StrPtr(nText)))
   InvalidateRect hWndControl, ByVal 0, True
   UpdateWindow hWndControl
End Property

Property Class_StatusBar.TipText(iPart As Long) As String                  '返回/设置工具提示文本
  Dim bb As String
  Dim nSize As Long = 2048
  bb = Space(nSize + 1)
  SendMessage(hWndControl, SB_GETTIPTEXT, MAKELONG(iPart, nSize), Cast(lParam, StrPtr(bb)))
  nSize = InStr(bb, Chr(0))
  If nSize = 0 Then
      Return bb
  Else
      Return  Mid(bb, 1, nSize-1)
  End If
End Property

Property Class_StatusBar.TipText(iPart As Long,nText As String)
   SendMessage hWndControl, SB_SETTIPTEXT, iPart, Cast(lParam, StrPtr(nText))
End Property

Property Class_StatusBar.Simple()  As Boolean                 '返回/设置简单模式。{=.True.False} 
  Return SendMessage (hWndControl, SB_ISSIMPLE, 0, 0)
End Property

Property Class_StatusBar.Simple(fSimple As Boolean)
  SendMessage (hWndControl, SB_SIMPLE, fSimple, 0)
End Property

Property Class_StatusBar.Icon(iPart As Long) As HICON          '返回/设置在状态栏中的图标。
  Return Cast(HICON , SendMessage (hWndControl, SB_GETICON, iPart, 0) )
End Property

Property Class_StatusBar.Icon(iPart As Long, hIcon As HICON)  '如果设置图标的值为 NULL，将从 iPart 中删除该图标。
  SendMessage (hWndControl, SB_SETICON, iPart, Cast(lParam, hIcon))
End Property

Property Class_StatusBar.PartWidth(iPart As Long) As Long
    Return m_Width(iPart)
End Property

Property Class_StatusBar.PartWidth(iPart As Long,nWidth As Long)    
    Dim nCount As Long = Count
    Dim nTmp As Long = 0
    
    If iPart >= nCount Then Return  '直接退出，最后一个不设置宽度，所有剩下的都归最后一个
    
    If iPart = 0 Then '
    	nTmp = nWidth - m_Width(iPart)
    Else
    	nTmp = nWidth - (m_Width(iPart) - m_Width(iPart - 1))
    EndIf    
    
    For i As Long = iPart To nCount - 1
    	m_Width(i) += nTmp
    Next i

    SendMessage(hWndControl,SB_SETPARTS,nCount + 1,Cast(LPARAM,@m_Width(0)))    
End Property
    
