#include once "../WinClass/clsControl.inc"
Type Class_Form Extends Class_Control
Private : 
    m_toumeidu As Long  '透明度百分比 0-100
    m_yanse As Long =-1 '透明颜色
Public : 
    Declare Constructor
    Declare Destructor

	Declare Function Show(ByVal hWndP As .hWnd =HWND_DESKTOP,ByVal Modal As Boolean = False )  As .hWnd  '加载窗口并且显示, 模态显示用True {2.True.False}
    Declare Sub Hide()                                                                                   '隐藏一个 MDIForm 或 Form 对象，但不卸载它。
	Declare Sub Center(ByVal hWndP As .hWnd = Null)                                                      '窗口居中，默认在屏幕上，有句柄为在其窗口上居中
	Declare Sub SetForegroundWindow()                                                                    '置于前台并激活窗口。
	Declare Sub Close() '关闭本窗口，如果是软件第一个窗口，将会关闭整个软件。 
	Declare Sub MoveWin() '用来拖动窗口，鼠标点下可以拖动窗口，放在窗口的鼠标按下事件里即可。 
	Declare Sub DoEvents() '让窗口处理消息，避免耗时的代码（如循环做很多事）造成软件假死。
	Declare Function ClassName() As String     '获取窗口类名
	Declare Function WindowPlacementSave(FileIni As String, nName As String) As Boolean   '在INI文件里保存窗口位置、大小。成功返回 True (通常在窗口销毁事件里)
    Declare Sub WindowPlacementLoad(FileIni As String, nName As String)  '从INI文件里加载窗口位置、大小来恢复窗口。 (通常在窗口加载事件里) 
    Declare Function FlashWindow() As Boolean '激活窗口并且闪耀窗口	

    Declare Property Caption() As String                '设置对象的标题栏中或图标下面的文本。
    Declare Property Caption(ByVal sText As String)
    Declare Property WindowState() As Integer             '{=.0'正常.1'最小化.2'最大化}
    Declare Property WindowState(ByVal bValue As Integer)
    Declare Property ScaleWidth() As Long                '返回/设置对象内部的W（像素）
    Declare Property ScaleHeight() As Long               '返回/设置对象内部的H（像素）
    Declare Property WindowsZ() As Boolean  '返回是否前置/设置在某个窗口之上或是{=.HWND_BOTTOM 普通层所有窗口最后.HWND_TOP 普通层所有窗口最前.HWND_NOTOPMOST 普通层窗口.HWND_TOPMOST 顶层窗口(置顶).某窗口句柄 将置于此窗口前}
    Declare Property WindowsZ(hWndlnsertAfter As .hWnd )  
    Declare Property Icon() As hIcon     '返回/设置本窗口图标，设置后的图标句柄不可销毁。
    Declare Property Icon(Ico As hIcon)  '设置为NULL，则删除图标。
    Declare Property UserDate() As Integer      '返回/设置用户数据，就是1个窗口可以存放1个数值。
    Declare Property UserDate(bValue As Integer)  
    Declare Property MousePass() As Boolean      '返回/设置窗口是否鼠标穿透效果（只能看到而无法用鼠标点到）{=.True.False}
    Declare Property MousePass(bValue As Boolean )   
    Declare Property TransPer() As Long      '返回/设置窗口透明度，百分比(0--100)，0%不透明 100%全透明    
    Declare Property TransPer(bValue As Long )       
    Declare Property TransColor() As Long  '返回/设置窗口透明颜色，RGB颜色值(用 BGR(r, g, b) 获取)，窗口上的此颜色将 100%全透明 （半透明实现不了）注：设为-1 为取消透明色     
    Declare Property TransColor(bValue As Long )       
    Declare Property AcceptFiles() As Boolean  '返回/设置窗口是否接受拖放文件。{=.True.False}
    Declare Property AcceptFiles(bValue As Boolean)       
End Type

Constructor Class_Form

End Constructor

Destructor Class_Form

End Destructor

Function Class_Form.Show(ByVal hWndP As .hWnd =HWND_DESKTOP,ByVal Modal As Boolean = False) As .hWnd 
    '留空，VFB自动处理这个
    Function=0
End Function

Sub Class_Form.Hide()
    ShowWindow(hWndControl,SW_HIDE)
End Sub

Sub Class_Form.Center(ByVal hWndP As .hWnd = Null) '窗口居中，默认在屏幕上，有句柄为在其窗口上居中
   AfxCenterWindow(hWndControl,hWndP)
End Sub

Sub Class_Form.SetForegroundWindow() '置于前台并激活窗口。
   .SetForegroundWindow(hWndControl)
End Sub

Function Class_Form.FlashWindow() As Boolean'激活窗口并且闪耀窗口  
    Dim aa As FLASHWINFO
    aa.cbSize=SizeOf(FLASHWINFO)
    aa.hWnd=hWndControl
    aa.dwFlags=FLASHW_ALL
    aa.uCount=5
    aa.dwTimeout=100
    OpenIcon hWndControl
    .SetForegroundWindow hWndControl
   Function= FlashWindowEx (@aa)
End Function

Sub Class_Form.Close() '关闭本窗口，如果是软件第一个窗口，将会关闭整个软件。 
    PostMessage hWndControl, WM_CLOSE, 0, 0
End Sub

Sub Class_Form.MoveWin() '用来拖动窗口，鼠标点下可以拖动窗口，放在窗口的鼠标按下事件里即可。 
    ReleaseCapture
    SendMessage hWndControl, WM_NCLBUTTONDOWN, HTCAPTION, Null
End Sub

Function Class_Form.ClassName() As String     '获取窗口类名
  Dim wszClassName As ZString * 260
  If IsWindow(hWndControl) Then
      GetClassName hWndControl, @wszClassName, SizeOf(wszClassName)
      Function = wszClassName
  End If
End Function

Sub Class_Form.DoEvents() '让窗口处理消息，避免耗时的代码（如循环做很多事）造成软件假死。
  If IsWindow(hWndControl) Then
      Dim uMsg As Msg
      Do While PeekMessage(VarPtr(uMsg), 0, 0, 0, PM_REMOVE)
          If IsDialogMessage(hWndControl, VarPtr(uMsg)) = 0 Then
              TranslateMessage VarPtr(uMsg)
              DispatchMessage VarPtr(uMsg)
          End If
      Loop
  End If
End Sub

Function Class_Form.WindowPlacementSave(FileIni As String, nName As String) As Boolean '在INI文件里保存窗口位置、大小。 
   ' // 获取当前窗口位置
   Dim WinPla As WINDOWPLACEMENT
   WinPla.Length = SizeOf(WinPla)
   GetWindowPlacement(hWndControl, @WinPla)
   ' // 将值保存在.ini文件中
   Dim bRes As BOOLEAN
   bRes = WritePrivateProfileString(StrPtr(nName), "Left", Str(WinPla.rcNormalPosition.Left), StrPtr(FileIni))
   bRes = WritePrivateProfileString(StrPtr(nName), "Right", Str(WinPla.rcNormalPosition.Right), StrPtr(FileIni))
   bRes = WritePrivateProfileString(StrPtr(nName), "Top", Str(WinPla.rcNormalPosition.Top), StrPtr(FileIni))
   bRes = WritePrivateProfileString(StrPtr(nName), "Bottom", Str(WinPla.rcNormalPosition.Bottom), StrPtr(FileIni))
   Return bRes
End Function

Sub Class_Form.WindowPlacementLoad(FileIni As String, nName As String)  '从INI文件里加载窗口位置、大小来恢复窗口。  
   ' // 获取当前窗口位置
   Dim WinPla As WINDOWPLACEMENT
   WinPla.Length = SizeOf(WinPla)
   GetWindowPlacement(hWndControl, @WinPla)
   ' // 读取.ini文件中保存的值
   Dim wszBuffer As ZString * 260, dwChars As DWord
   dwChars = GetPrivateProfileString(nName, "Left", Null, @wszBuffer, MAX_PATH, StrPtr(FileIni))
   If dwChars Then WinPla.rcNormalPosition.Left = Val(wszBuffer)
   dwChars = GetPrivateProfileString(nName, "Right", Null, @wszBuffer, MAX_PATH, StrPtr(FileIni))
   If dwChars Then WinPla.rcNormalPosition.Right = Val(wszBuffer)
   If WinPla.rcNormalPosition.Right = 0 Then WinPla.rcNormalPosition.Right = 750
   dwChars = GetPrivateProfileString(nName, "Top", Null, @wszBuffer, MAX_PATH, StrPtr(FileIni))
   If dwChars Then WinPla.rcNormalPosition.Top = Val(wszBuffer)
   dwChars = GetPrivateProfileString(nName, "Bottom", Null, @wszBuffer, MAX_PATH, StrPtr(FileIni))
   If dwChars Then WinPla.rcNormalPosition.Bottom = Val(wszBuffer)
   If WinPla.rcNormalPosition.Bottom = 0 Then WinPla.rcNormalPosition.Bottom = 450
   SetWindowPlacement hWndControl, @WinPla
   ' // 桌面的实际尺寸
   Dim rcDeskTop As RECT
   SystemParametersInfo SPI_GETWORKAREA, 0, @rcDesktop, 0
   ' // 确保对话框没有水平过大
   If WinPla.rcNormalPosition.Right - WinPla.rcNormalPosition.Left > rcDesktop.Right Then
      WinPla.rcNormalPosition.Left = 0
      WinPla.rcNormalPosition.Right = rcDesktop.Right
   End If
   ' // 确保对话框不是垂直超大的
   If WinPla.rcNormalPosition.Bottom - WinPla.rcNormalPosition.Top > rcDesktop.Bottom Then
       WinPla.rcNormalPosition.Top = 0
      WinPla.rcNormalPosition.Bottom = rcDesktop.Bottom
   End If
   ' // 确保对话框的左侧可见
   If WinPla.rcNormalPosition.Left < 0 Then
      WinPla.rcNormalPosition.Right = WinPla.rcNormalPosition.Right - WinPla.rcNormalPosition.Left
      WinPla.rcNormalPosition.Left = 0
   End If
   ' // 确保对话框的右侧可见
   If WinPla.rcNormalPosition.Right > rcDesktop.Right Then
      WinPla.rcNormalPosition.Left = WinPla.rcNormalPosition.Left - (WinPla.rcNormalPosition.Right - rcDesktop.Right)
      WinPla.rcNormalPosition.Right = rcDesktop.Right
   End If
   ' // 确保对话框的顶部可见
   If WinPla.rcNormalPosition.Top < 0 Then
      WinPla.rcNormalPosition.Bottom = WinPla.rcNormalPosition.Bottom - WinPla.rcNormalPosition.Top
      WinPla.rcNormalPosition.Top = 0
   End If
   ' // 确保对话框的底部可见
   If WinPla.rcNormalPosition.Bottom > rcDesktop.Bottom Then
      WinPla.rcNormalPosition.Top = WinPla.rcNormalPosition.Top - (WinPla.rcNormalPosition.Bottom - rcDesktop.Bottom)
      WinPla.rcNormalPosition.Bottom = rcDesktop.Bottom
   End If
   ' // 调整工作区域
   Dim rc As RECT
   SystemParametersInfo SPI_GETWORKAREA, 0, @rc, 0
   If WinPla.rcNormalPosition.Left = rc.Left And WinPla.rcNormalPosition.Right = rc.Right Then WinPla.rcNormalPosition.Right = WinPla.rcNormalPosition.Right
   If WinPla.rcNormalPosition.Top = rc.Top And WinPla.rcNormalPosition.Bottom = rc.Bottom Then WinPla.rcNormalPosition.Bottom = WinPla.rcNormalPosition.Bottom
   ' // 放置窗口
   SetWindowPlacement hWndControl, @WinPla
End Sub

Property Class_Form.Caption() As String
    Return AfxGetWindowText(hWndControl)  
End Property

Property Class_Form.Caption(ByVal sText As String)
    AfxSetWindowText  hWndControl,sText
End Property

Property Class_Form.WindowState() As Integer  
    Dim m_WndState As Integer
    If IsIconic(hWndControl) Then      '最小化
        m_WndState = 1
    ElseIf IsZoomed(hWndControl) Then  '最大化
        m_WndState = 2
    Else 
        m_WndState = 0           '正常
    End If
    Return m_WndState
End Property

Property Class_Form.WindowState(ByVal iTxt As Integer)
  Select Case iTxt
      Case 0
          ShowWindow(hWndControl, SW_RESTORE)  '用原来的大小和位置显示一个窗口，同时令其进入活动状态
      Case 1
          ShowWindow(hWndControl, SW_MINIMIZE) '//最小化
      Case 2
          ShowWindow(hWndControl, SW_MAXIMIZE) '//最大化
      Case Else
  End Select
End Property

Property Class_Form.ScaleHeight() As Long               '
    Dim rc As Rect
    GetClientRect(hWndControl,@rc)     '获得客户区大小
    Return rc.Bottom - rc.Top
End Property

Property Class_Form.ScaleWidth() As Long                   '
    Dim rc As Rect
    GetClientRect(hWndControl,@rc)     '获得客户区大小
    Return rc.Right - rc.Left 
End Property

Property Class_Form.WindowsZ() As Boolean 
'HWND_BOTTOM,HWND_TOP,HWND_NOTOPMOST,HWND_TOPMOST,设置窗口Z位置:最后，最前，普通，置顶 
   Return (AfxGetWindowExStyle(hWndControl) And WS_EX_TOPMOST) =WS_EX_TOPMOST
End Property

Property Class_Form.WindowsZ(ByVal hWndlnsertAfter As .hWnd ) 
'HWND_BOTTOM,HWND_TOP,HWND_NOTOPMOST,HWND_TOPMOST,设置窗口Z位置:最后，最前，普通，置顶 
   SetWindowPos( hWndControl,hWndlnsertAfter,0,0,0,0, SWP_NOSIZE Or SWP_NOMOVE Or SWP_SHOWWINDOW)
End Property

Property Class_Form.Icon() As hIcon        '返回/设置本窗口小图标(普通图标)，设置后的图标句柄不可销毁，系统在ALT + TAB对话框中显示大图标，在窗口标题中显示小图标。
  Dim aa As HICON
  aa = Cast(hicon,SendMessage(hWndControl, WM_GETICON, ICON_SMALL, 0))
  '没有显式设置图标的窗口（使用WM_SETICON）使用已注册窗口类的图标，在这种情况下，DefWindowProc将为WM_GETICON消息返回0 。
  '如果向窗口发送WM_GETICON消息返回0，则接下来尝试调用该窗口的GetClassLongPtr函数。如果返回0，则尝试LoadIcon函数。
  If aa = 0 Then   aa = Cast(hicon, GetClassLongPtr(hWndControl, GCLP_HICON))
  Return aa
End Property

Property Class_Form.Icon(Ico As hIcon)  '设置为NULL，则删除图标。 
  Dim aa As HICON = Cast(hicon,SendMessage(hWndControl, WM_SETICON, ICON_SMALL, Cast(lParam, Ico)))
  If aa <> 0 Then DeleteObject Cast(HGDIOBJ, aa)
  aa = Cast(hicon,SendMessage(hWndControl, WM_SETICON, ICON_BIG, Cast(lParam, Ico)))
  If aa <> 0 Then DeleteObject Cast(HGDIOBJ, aa)
End Property         

Property Class_Form.UserDate() As Integer      '返回/设置用户数据，就是1个窗口可以存放1个数值。
  Return GetWindowLongPtr(hWndControl, GWLP_USERDATA)
End Property

Property Class_Form.UserDate(bValue As Integer)  
  SetWindowLongPtr hWndControl,GWLP_USERDATA, bValue
End Property  

Property Class_Form.MousePass() As Boolean      '返回/设置窗口鼠标穿透效果（只能看到而无法用鼠标点到）{=.True.False}
  Return (AfxGetWindowExStyle(hWndControl) And WS_EX_TRANSPARENT) =WS_EX_TRANSPARENT
End Property

Property Class_Form.MousePass(bValue As Boolean)
  If bValue Then
      AfxAddWindowExStyle hWndControl, WS_EX_TRANSPARENT
      AfxAddWindowExStyle hWndControl, WS_EX_LAYERED
  Else
      AfxRemoveWindowExStyle hWndControl, WS_EX_TRANSPARENT
      If m_toumeidu = 0 And m_yanse = -1 Then
          AfxRemoveWindowExStyle hWndControl, WS_EX_LAYERED
      End If
  End If
End Property

Property Class_Form.TransPer() As Long      '返回/设置窗口透明度，百分比(0--100)，0%不透明 100%全透明    
  Return m_toumeidu
End Property

Property Class_Form.TransPer(bValue As Long)
  m_toumeidu = bValue
  If m_toumeidu < 0 Then m_toumeidu = 0
  If m_toumeidu > 100 Then m_toumeidu = 100
  If m_toumeidu = 0 And m_yanse = -1 And MousePass=0 Then
      AfxRemoveWindowExStyle hWndControl, WS_EX_LAYERED
  Else
      AfxAddWindowExStyle hWndControl, WS_EX_LAYERED
      If m_toumeidu = 0 Then
          SetLayeredWindowAttributes hWndControl, m_yanse, 0, LWA_COLORKEY
      Else
          If m_yanse = -1 Then
              SetLayeredWindowAttributes hWndControl, 0, (100 - m_toumeidu) * 2.55, LWA_ALPHA
          Else
              SetLayeredWindowAttributes hWndControl, m_yanse, (100 - m_toumeidu) * 2.55, LWA_ALPHA Or LWA_COLORKEY
          End If
      End If
  End If
End Property

Property Class_Form.TransColor() As Long      '返回/设置窗口透明颜色，窗口上的此颜色将 100%全透明 （半透明实现不了）  
  Return m_yanse
End Property

Property Class_Form.TransColor(bValue As Long)
  m_yanse = bValue
  If m_toumeidu = 0 And m_yanse = -1  And MousePass=0 Then
      AfxRemoveWindowExStyle hWndControl, WS_EX_LAYERED
  Else
      AfxAddWindowExStyle hWndControl, WS_EX_LAYERED
      If m_yanse = -1 Then
          SetLayeredWindowAttributes hWndControl, 0, (100 - m_yanse) * 2.55, LWA_COLORKEY
      Else
          If m_toumeidu = 0 Then
             SetLayeredWindowAttributes hWndControl, m_yanse, 0, LWA_COLORKEY 
          Else
              SetLayeredWindowAttributes hWndControl, m_yanse, (100 - m_toumeidu) * 2.55, LWA_ALPHA Or LWA_COLORKEY
          End If
      End If
  End If
End Property

Property Class_Form.AcceptFiles() As Boolean  '返回/设置窗口是否接受拖放文件。
  Return (AfxGetWindowExStyle(hWndControl) And WS_EX_ACCEPTFILES) =WS_EX_ACCEPTFILES
End Property

Property Class_Form.AcceptFiles(bValue As Boolean )   
  If bValue Then
      AfxAddWindowExStyle hWndControl, WS_EX_ACCEPTFILES
  Else
      AfxRemoveWindowExStyle hWndControl, WS_EX_ACCEPTFILES
  End If
End Property



