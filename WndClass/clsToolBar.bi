Enum ToolButtonStyle
    tbsAutosize           = TBSTYLE_AUTOSIZE
    tbsButton             = TBSTYLE_BUTTON
    tbsCheck              = TBSTYLE_CHECK
    tbsCheckGroup         = TBSTYLE_CHECKGROUP
    tbsGroup              = TBSTYLE_GROUP
    tbsDropDown           = TBSTYLE_DROPDOWN
    tbsNoPrefix           = TBSTYLE_NOPREFIX
    tbsSeparator          = TBSTYLE_SEP
    tbsShowText           = BTNS_SHOWTEXT
    tbsWholeDropdown      = BTNS_WHOLEDROPDOWN
End Enum
    
Enum ToolButtonState    
    tstIndeterminate      = TBSTATE_INDETERMINATE
    tstEnabled            = TBSTATE_ENABLED
    tstHidden             = TBSTATE_HIDDEN
    tstEllipses           = TBSTATE_ELLIPSES
    tstChecked            = TBSTATE_CHECKED
    tstPressed            = TBSTATE_PRESSED
    tstMarked             = TBSTATE_MARKED
    tstWrap               = TBSTATE_WRAP
End Enum

Type clsToolBar Extends clsControl
Public:
    Declare Constructor
    Declare Destructor	
    
    Declare Function Create(hParent As HWND,Id As Integer = 0,dwStyle As Integer = 0,dwExStyle As Integer = 0) As HWND
    Declare Function AddBitmap(nButtons As DWORD = 0,tabm As TBADDBITMAP Ptr = NULL,IsSysBig As Boolean = TRUE) As Long 
    Declare Function AddButton(idxBitmap As Long,idCommand As Long,fsState As ToolButtonState = TBSTATE_ENABLED,fsStyle As ToolButtonStyle = TBSTYLE_BUTTON,dwData As DWORD_PTR = 0,pwszText As LPTSTR = NULL) As Boolean  '���Ӱ�ť
    Declare Function AddSeparator(nWidth As Long = 0) As Boolean '���ӷָ���
    Declare Function InsertButton OverLoad (idxButton As Long,lpButton As TBBUTTON Ptr) As Boolean	
    Declare Function InsertButton OverLoad (idxButton As Long,idxBitmap As Long,idCommand As Long ,fsState As ToolButtonState = TBSTATE_ENABLED,fsStyle As ToolButtonStyle = TBSTYLE_BUTTON,dwData As DWORD_PTR = 0,pwszText As LPTSTR = NULL) As Boolean 
    Declare Function GetItemRect(iButton As Long,lprc As RECT Ptr) As Boolean

    Declare Sub ButtonStructSize(cb As Long = 0)
    Declare Sub AutoSize()

    Declare Property Count As Integer
    Declare Property Enabled(idButton AS Long) As Boolean
    Declare Property Enabled(idButton AS Long,nEnabled As Boolean)
    Declare Property Checked(idButton AS Long) As Boolean
    Declare Property Checked(idButton AS Long,nCheck As Boolean)
End Type

'============================================================================================================================================================
Constructor clsToolBar

End Constructor

Destructor clsToolBar

End Destructor

Function clsToolBar.Create(hParent As HWND,Id As Integer = 0,dwStyle As Integer = 0,dwExStyle As Integer = 0) As HWND
	If IsWindow(m_hWnd) Then Exit Function
                                                                                                                            'WS_BORDER �߿�TBSTYLE_FLAT �ָ�������
    If dwStyle = 0 Then dwStyle = WS_CHILD Or WS_VISIBLE Or WS_CLIPCHILDREN Or WS_CLIPSIBLINGS Or CCS_TOP Or TBSTYLE_TOOLTIPS Or WS_BORDER Or TBSTYLE_FLAT 

    m_hWnd = CreateWindowEx(dwExStyle,"ToolBarWindow32","",dwStyle,0,0,0,0,hParent,Cast(HMENU,Id),GetModuleHandle(NULL),NULL)
    SendMessage(m_hWnd,TB_BUTTONSTRUCTSIZE,SizeOf(TBBUTTON),0)
    SendMessage(m_hWnd,WM_SETFONT,Cast(wParam,GetStockObject(DEFAULT_GUI_FONT)),0)
    Function = m_hWnd
End Function

Function clsToolBar.AddBitmap(nButtons As DWORD = 0,tabm As TBADDBITMAP Ptr = NULL,IsSysBig As Boolean = TRUE) As Long 
	If tabm = NULL Then
	    Dim tbab As TBADDBITMAP
	    tbab.hInst = HINST_COMMCTRL
		If IsSysBig Then
	        tbab.nID = IDB_STD_LARGE_COLOR 
		Else
		    tbab.nID = IDB_STD_SMALL_COLOR 
		EndIf
		tabm = @tbab
		
		If nButtons = 0 OrElse nButtons > 15 Then nButtons = 15
	End If

    Function = SendMessage(m_hWnd,TB_ADDBITMAP,Cast(WPARAM,nButtons), Cast(LPARAM,tabm))
End Function

Function clsToolBar.AddButton(idxBitmap As Long,idCommand As Long,fsState As ToolButtonState = TBSTATE_ENABLED,fsStyle As ToolButtonStyle = TBSTYLE_BUTTON,dwData As DWORD_PTR = 0,pwszText As LPTSTR = NULL) As Boolean
    If  fsState = 0 Then  fsState = TBSTATE_ENABLED
    Dim  idxString AS INT_PTR
    If pwszText <> NULL Then  idxString = IIf(Len(*pwszText) = 0, -1, Cast(INT_PTR,pwszText))
#ifdef __FB_64BIT__
    Dim tbb As TBBUTTON = (idxBitmap, idCommand, fsState, fsStyle, {0, 0, 0, 0, 0, 0}, dwData, idxString)
#else
    Dim tbb As TBBUTTON = (idxBitmap, idCommand, fsState, fsStyle, {0, 0}, dwData, idxString)
#endif
    Function = SendMessage(m_hWnd,TB_ADDBUTTONS, 1, Cast(LPARAM,@tbb))
End Function

Function clsToolBar.InsertButton OverLoad (idxButton As Long,lpButton As TBBUTTON Ptr) As Boolean
    Function  = SendMessage(m_hWnd,TB_INSERTBUTTON, Cast(WPARAM, idxButton), Cast(LPARAM, lpButton))
End Function

Function clsToolBar.InsertButton OverLoad (idxButton As Long,idxBitmap As Long,idCommand As Long ,fsState As ToolButtonState = TBSTATE_ENABLED,fsStyle As ToolButtonStyle = TBSTYLE_BUTTON,dwData As DWORD_PTR = 0,pwszText As LPTSTR = NULL) As Boolean 
    If  fsState = 0 Then  fsState = TBSTATE_ENABLED
    Dim  idxString AS INT_PTR
    If pwszText <> NULL Then  idxString = IIf(Len(*pwszText) = 0, -1, Cast(INT_PTR,pwszText))
#ifdef __FB_64BIT__
    Dim tbb As TBBUTTON = (idxBitmap, idCommand, fsState, fsStyle, {0, 0, 0, 0, 0, 0}, dwData, idxString)
#else
    Dim tbb As TBBUTTON = (idxBitmap, idCommand, fsState, fsStyle, {0, 0}, dwData, idxString)
#endif
    Function = SendMessage(m_hWnd,TB_ADDBUTTONS,Cast(WPARAM, idxButton), Cast(LPARAM,@tbb))
END Function

Function clsToolBar.AddSeparator(nWidth As Long = 0) As Boolean
#ifdef __FB_64BIT__
    Dim tbb As TBBUTTON = (nWidth, 0, TBSTATE_ENABLED, TBSTYLE_SEP, {0, 0, 0, 0, 0, 0}, 0, -1)
#else
    Dim tbb As TBBUTTON = (nWidth, 0, TBSTATE_ENABLED, TBSTYLE_SEP, {0, 0}, 0, -1)
#endif
    Function = SendMessage(m_hWnd, TB_ADDBUTTONS, 1, Cast(LPARAM, @tbb))
END Function

Sub clsToolBar.ButtonStructSize(cb As Long = 0)
	If cb = 0 Then cb = SizeOf(TBBUTTON)
    SendMessage(m_hWnd, TB_BUTTONSTRUCTSIZE, cb, 0)
END Sub

Sub clsToolBar.AutoSize()
	SendMessage(m_hWnd,TB_AUTOSIZE,0,0)    'Auto Size ToolBar
End Sub

Property clsToolBar.Count As Integer
    Return SendMessageW(m_hWnd,TB_BUTTONCOUNT, 0, 0)
End Property

Property clsToolBar.Enabled(idButton As Long) As Boolean
    Return SendMessage(m_hWnd, TB_ISBUTTONENABLED, Cast(WPARAM,idButton), 0)
End Property

Property clsToolBar.Enabled(idButton As Long,nEnabled As Boolean)
    SendMessage(m_hWnd,TB_ENABLEBUTTON,Cast(WPARAM,idButton),Cast(LPARAM,MAKELONG(IIf(nEnabled,CTRUE,FALSE), 0)))
End Property

Property clsToolBar.Checked(idButton As Long) As Boolean
    Return SendMessage(m_hWnd,TB_ISBUTTONCHECKED, Cast(WPARAM,idButton), 0)
End Property

Property clsToolBar.Checked(idButton AS Long,nCheck As Boolean)
    SendMessage(m_hWnd,TB_CHECKBUTTON,Cast(WPARAM,idButton),Cast(LPARAM,MAKELONG(IIf(nCheck,CTRUE,FALSE), 0)))
End Property

Function clsToolBar.GetItemRect(iButton As Long,lprc As RECT Ptr) As Boolean
	Return SendMessage(m_hWnd,TB_GETITEMRECT,Cast(WPARAM,iButton),Cast(LPARAM,lprc))
End Function

























