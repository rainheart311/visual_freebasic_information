Type Class_MonthCalendar Extends Class_Control
    Declare Constructor
    Declare Destructor
    Declare Property CurSel() As SYSTEMTIME                 '返回/设置当前选择的日期
    Declare Property CurSel(ByVal pst As SYSTEMTIME)
    Declare Property SelMaxDate() As SYSTEMTIME           '返回/设置用户选择最大时间。
    Declare Property SelMaxDate(ByVal bValue As SYSTEMTIME)
    Declare Property SelMinDate() As SYSTEMTIME           '返回/设置用户选择最小时间。
    Declare Property SelMinDate(ByVal bValue As SYSTEMTIME)
    Declare Property MaxDate() As SYSTEMTIME           '返回/设置允许最大时间。如果任一元素都设置为全零，则不会为月历控件设置相应的限制。
    Declare Property MaxDate(ByVal bValue As SYSTEMTIME)
    Declare Property MinDate() As SYSTEMTIME           '返回/设置允许最小时间。如果任一元素都设置为全零，则不会为月历控件设置相应的限制。
    Declare Property MinDate(ByVal bValue As SYSTEMTIME)
    Declare Property MaxSelCount() As Long           '返回/设置可以选择的最大日期范围。
    Declare Property MaxSelCount(ByVal bValue As Long)
    Declare Property Color(ByVal iColor As Long) As Long    '返回/设置给定部分的颜色,{1.MCSC_BACKGROUND 月份之间显示的背景颜色.MCSC_MONTHBK 月份内显示的背景颜色.MCSC_TEXT 月内显示文字的颜色.MCSC_TITLEBK 标题中显示的背景颜色.MCSC_TITLETEXT 标题中用于显示文本的颜色.MCSC_TRAILINGTEXT 标题日和尾日文本的颜色}
    Declare Property Color(ByVal iColor As Long,ByVal bValue As Long)
    Declare Property CurrentView() As Long           '返回/设置视图样式，最低操作系统Windows Vista。,{=.MCMV_MONTH 月视图.MCMV_YEAR 年度视图.MCMV_DECADE 10年视图.MCMV_CENTURY 世纪视图}
    Declare Property CurrentView(ByVal dwNewView As Long)
    Declare Property FirstDayOfWeek() As Long            '返回/设置一周中的第一天,{=.0 星期一.1 星期二.2 星期三.3 星期四.4 星期五.5 星期六. 6 星期日}
    Declare Property FirstDayOfWeek(ByVal iDay As Long )
    Declare Property Today() As SYSTEMTIME                 '返回/设置指定为“今天”的日期的日期信息。
    Declare Property Today(ByVal pst As SYSTEMTIME)
End Type

'----------------------------------------------------------------------------------------------------------------------------------------------------------------
Constructor Class_MonthCalendar
  
End Constructor

Destructor Class_MonthCalendar
  
End Destructor
Property Class_MonthCalendar.CurSel() As SYSTEMTIME                 '返回/设置当前选择的日期
  Dim pst As SYSTEMTIME 
   MonthCal_GetCurSel(hWndControl,@pst)
  Return pst 
End Property
Property Class_MonthCalendar.CurSel(ByVal pst As SYSTEMTIME)
  MonthCal_SetCurSel(hWndControl, @pst)
End Property 
Property Class_MonthCalendar.SelMaxDate() As SYSTEMTIME           '返回/设置用户选择最大时间。
  Dim pst(1) As SYSTEMTIME 
   MonthCal_GetSelRange(hWndControl,@pst(0))
  Return pst(1)
End Property
Property Class_MonthCalendar.SelMaxDate(ByVal bValue As SYSTEMTIME)
  Dim pst(1) As SYSTEMTIME 
   MonthCal_GetSelRange(hWndControl,@pst(0))
   pst(1)=bValue
   SNDMSG(hWndControl, MCM_SETSELRANGE, 0, Cast(lParam, (@pst(0))))
'  MonthCal_SetSelRange hWndControl,@pst(0)
End Property 
Property Class_MonthCalendar.SelMinDate() As SYSTEMTIME           '返回/设置用户选择最小时间。
  Dim pst(1) As SYSTEMTIME 
   MonthCal_GetSelRange(hWndControl,@pst(0))
  Return pst(0)
End Property
Property Class_MonthCalendar.SelMinDate(ByVal bValue As SYSTEMTIME)
  Dim pst(1) As SYSTEMTIME 
   MonthCal_GetSelRange(hWndControl,@pst(0))
   pst(0)=bValue
   SNDMSG(hWndControl, MCM_SETSELRANGE, 0, Cast(lParam, (@pst(0))))
'  MonthCal_SetSelRange hWndControl,@pst(0)
End Property   
Property Class_MonthCalendar.MaxDate() As SYSTEMTIME           '返回/设置允许最大时间。
  Dim pst(1) As SYSTEMTIME 
   MonthCal_GetRange(hWndControl,@pst(0))
  Return pst(1)
End Property
Property Class_MonthCalendar.MaxDate(ByVal bValue As SYSTEMTIME)
  Dim pst(1) As SYSTEMTIME 
   MonthCal_GetRange(hWndControl,@pst(0))
   pst(1)=bValue
'  MonthCal_SetRange hWndControl,@pst(0)
   SNDMSG(hWndControl, MCM_SETRANGE, Cast(wParam, (GDTR_MAX)), Cast(lParam, (@pst(0))))
End Property   
Property Class_MonthCalendar.MinDate() As SYSTEMTIME           '返回/设置允许最小时间。
  Dim pst(1) As SYSTEMTIME 
   MonthCal_GetRange(hWndControl,@pst(0))
  Return pst(0)
End Property
Property Class_MonthCalendar.MinDate(ByVal bValue As SYSTEMTIME)
  Dim pst(1) As SYSTEMTIME 
   MonthCal_GetRange(hWndControl,@pst(0))
   pst(0)=bValue
'  MonthCal_SetRange hWndControl,@pst(0)
  SNDMSG(hWndControl, MCM_SETRANGE, Cast(wParam, (GDTR_MIN)), Cast(lParam, (@pst(0))))
End Property    
Property Class_MonthCalendar.MaxSelCount() As Long           '返回/设置可以选择的最大日期范围。
  Return MonthCal_GetMaxSelCount(hWndControl)
End Property
Property Class_MonthCalendar.MaxSelCount(ByVal bValue As Long)
'  MonthCal_SetMaxSelCount hWndControl,bValue
   SNDMSG(hWndControl, MCM_SETMAXSELCOUNT, Cast(wParam, (bValue)), Cast(lParam, 0))
End Property    
Property Class_MonthCalendar.Color(ByVal iColor As Long) As Long            '返回/设置给定部分的颜色
  Return MonthCal_GetColor(hWndControl,iColor)
End Property
Property Class_MonthCalendar.Color(ByVal iColor As Long,ByVal bValue As Long)
'  MonthCal_SetColor hWndControl,iColor,bValue
   SNDMSG(hWndControl, MCM_SETCOLOR, iColor, bValue)
End Property    
Property Class_MonthCalendar.CurrentView() As Long           '返回/设置视图样式，
'  Return MonthCal_GetCurrentView(hWndControl)
   Return Cast(Long, SNDMSG(hWndControl, MCM_FIRST + 22, 0, 0))
End Property
Property Class_MonthCalendar.CurrentView(ByVal dwNewView As Long)
'  MonthCal_SetCurrentView hWndControl,dwNewView
   SNDMSG(hWndControl, MCM_FIRST + 32, 0, Cast(lParam, (dwNewView)))
End Property  
Property Class_MonthCalendar.FirstDayOfWeek() As Long            '返回/设置一周中的第一天,0是星期一，1是星期二，以此类推。
  Dim aa As Long 
  aa =MonthCal_GetFirstDayOfWeek(hWndControl)
  Return LoWord(aa)
End Property
Property Class_MonthCalendar.FirstDayOfWeek(ByVal iDay As Long )
'  MonthCal_SetFirstDayOfWeek hWndControl,bValue
  SNDMSG(hWndControl, MCM_SETFIRSTDAYOFWEEK, 0, iDay)
End Property  
Property Class_MonthCalendar.Today() As SYSTEMTIME                 '返回/设置指定为“今天”的日期的日期信息。
  Dim aa As SYSTEMTIME 
  MonthCal_GetToday(hWndControl,@aa)
  Return aa
End Property
Property Class_MonthCalendar.Today(ByVal pst As SYSTEMTIME)
'  MonthCal_SetToday hWndControl,@pst
   SNDMSG(hWndControl, MCM_SETTODAY, 0, Cast(lParam, (@pst)))
End Property  

