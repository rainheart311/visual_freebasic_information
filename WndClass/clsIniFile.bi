Type clsIniFile
Private:
#Ifndef UNICODE
    m_FilePath As ZString * MAX_PATH
#Else
    m_FilePath As WString * MAX_PATH
#EndIf
Public:
    Declare Constructor
    Declare Destructor  

    Declare Function DelKey(sSection As LPCTSTR,sKey As LPCTSTR) As Boolean   
    Declare Function GetKey OverLoad(sSection As LPCTSTR,sKey As LPCTSTR,sDefault As LPCTSTR) As LPCTSTR     
    Declare Function SetKey OverLoad(sSection As LPCTSTR,sKey As LPCTSTR,sValue As LPCTSTR) As Boolean  
    Declare Function GetKey OverLoad(sSection As LPCTSTR,sKey As LPCTSTR,sDefault As Integer) As Integer    
    Declare Function SetKey OverLoad(sSection As LPCTSTR,sKey As LPCTSTR,sValue As Integer) As Boolean 
    Declare Function GetKey OverLoad(sSection As LPCTSTR,sKey As LPCTSTR,sDefault As Double) As Double   
    Declare Function SetKey OverLoad(sSection As LPCTSTR,sKey As LPCTSTR,sValue As Double) As Boolean 
    Declare Function GetKeys(lpszApp As LPCTSTR,lpszKey As LPCTSTR,szTypes As LPCTSTR,lpDta As Any Ptr) As Boolean
    Declare Function SetKeys(lpszApp As LPCTSTR,ByVal lpszKey As LPCTSTR,lpszTypes As LPCTSTR,lpDta As Any Ptr) As Boolean

    Declare Property FilePath() As LPCTSTR
    Declare Property FilePath(sFilePath As LPCTSTR) 
End Type

Constructor clsIniFile
    m_FilePath = ""
End Constructor

Destructor clsIniFile

End Destructor

Function clsIniFile.DelKey(sSection As LPCTSTR,sKey As LPCTSTR) As Boolean   
    If Dir(m_FilePath) = "" Then 
    	Print "File Path is empty"
    	Exit Function
    EndIf

    Function = WritePrivateProfileString(sSection,sKey,NULL,@m_FilePath)
End Function

Function clsIniFile.GetKey OverLoad(sSection As LPCTSTR,sKey As LPCTSTR,sDefault As LPCTSTR) As LPCTSTR      
' 从 INI 文件获取密钥值
'返回： lKey lSection 里面的值的字符串
'如果找不到 lKey则返回 lDefault 值                       
    Dim zText As TSTR * 1024

    GetPrivateProfileString(sSection,sKey,sDefault, @zText,SizeOf(zText),@m_FilePath)
    Function = @zText
End Function  

Function clsIniFile.SetKey OverLoad(sSection As LPCTSTR,sKey As LPCTSTR,sValue As LPCTSTR) As Boolean  	
    Function = WritePrivateProfileString(sSection,sKey,sValue,@m_FilePath)
End Function

Function clsIniFile.GetKey OverLoad(sSection As LPCTSTR,sKey As LPCTSTR,sDefault As Integer) As Integer   
#Ifndef UNICODE
	Dim zText As ZString * 1024
    Dim sTmp As ZString * 1024 = Str(sDefault)
#Else
    Dim zText As WString * 1024
    Dim sTmp As WString * 1024 = WStr(sDefault)
#EndIf
    GetPrivateProfileString sSection,sKey, @sTmp,@zText,SizeOf(zText),@m_FilePath
    Function = Val(zText)
End Function 

Function clsIniFile.SetKey OverLoad(sSection As LPCTSTR,sKey As LPCTSTR,sValue As Integer) As Boolean 
#Ifndef UNICODE
    Dim sTmp As ZString * 1024 = Str(sValue)
#Else
    Dim sTmp As WString * 1024 = WStr(sValue)
#EndIf
    Function = WritePrivateProfileString(sSection,sKey,@sTmp,@m_FilePath)
End Function

Function clsIniFile.GetKey OverLoad(sSection As LPCTSTR,sKey As LPCTSTR,ByVal sDefault As Double) As Double  
#Ifndef UNICODE
	Dim zText As ZString * 1024
    Dim sTmp As ZString * 1024 = Str(sDefault)
#Else
    Dim zText As WString * 1024
    Dim sTmp As WString * 1024 = WStr(sDefault)
#EndIf
    GetPrivateProfileString sSection,sKey,@sTmp,@zText,SizeOf(zText),@m_FilePath
    Function = Val(zText)
End Function 

Function clsIniFile.SetKey OverLoad(sSection As LPCTSTR,sKey As LPCTSTR,sValue As Double) As Boolean 
#Ifndef UNICODE
    Dim sTmp As ZString * 1024 = Str(sValue)
#Else
    Dim sTmp As WString * 1024 = WStr(sValue)
#EndIf
    Function = WritePrivateProfileString(sSection,sKey,@sTmp,@m_FilePath)
End Function

Function clsIniFile.GetKeys(lpszApp As LPCTSTR,lpszKey As LPCTSTR,szTypes As LPCTSTR,lpDta As Any Ptr) As Boolean
	Dim i As Integer
	Dim ofs As Integer
	Dim v As Integer
	Dim p As LPTSTR
#Ifndef UNICODE
	Dim tmp As ZString*256
	Dim szDta As ZString*4096
#Else
    Dim tmp As WString*256
	Dim szDta As WString*4096
#EndIf

	If GetPrivateProfileString(lpszApp,lpszKey,NULL,@szDta,4096,@m_FilePath) Then
		For i=1 To Len(*szTypes) 'get nulber
			v=0
			Select Case Asc(*szTypes,i)-48 'get type
				Case 0
					' String
					RtlMoveMemory(@p,lpDta+ofs,4)
					If InStr(szDta,",") Then
						tmp=Left(szDta,InStr(szDta,",")-1)
					Else
						tmp=szDta
					EndIf
					lstrcpy(p,@tmp)
					ofs=ofs+4
				Case 1  '1 char
					' Byte
					If Len(szDta) Then
						v=Val(szDta)
						RtlMoveMemory(lpDta+ofs,@v,1)
					EndIf
					ofs=ofs+1
				Case 2  '2 char = short
					' Word
					If Len(szDta) Then
						v=Val(szDta)
						RtlMoveMemory(lpDta+ofs,@v,2)
					EndIf
					ofs=ofs+2
				Case 4 '4 char = long
					' DWord
					If Len(szDta) Then
						v=Val(szDta)
						RtlMoveMemory(lpDta+ofs,@v,4)
					EndIf
					ofs=ofs+4
			End Select
			If InStr(szDta,",") Then
				szDta=Mid(szDta,InStr(szDta,",")+1)
			Else
				szDta=""
			EndIf
		Next i
	Else
		Return FALSE
	EndIf
	Return TRUE
End Function

Function clsIniFile.SetKeys(lpszApp As LPCTSTR,ByVal lpszKey As LPCTSTR,lpszTypes As LPCTSTR,lpDta As Any Ptr) As Boolean
#Ifndef UNICODE
	Dim value As ZString*4096
#Else
    Dim value As WString*4096
#EndIf
	Dim i As Integer
	Dim ofs As Integer
	Dim v As Integer
	Dim p As LPTSTR

	For i=0 To lstrlen(lpszTypes)-1
		v=0
		Select Case lpszTypes[i]-48
			Case 0
				' String
				RtlMoveMemory(@p,lpDta+ofs,4)
				value=value & ","
				lstrcat(@value,p)
				ofs=ofs+4
			Case 1
				' Byte
				RtlMoveMemory(@v,lpDta+ofs,1)
				ofs=ofs+1
				value=value & "," & Str(v)
			Case 2
				' Word
				RtlMoveMemory(@v,lpDta+ofs,2)
				ofs=ofs+2
				value=value & "," & Str(v)
			Case 4
				' DWord
				RtlMoveMemory(@v,lpDta+ofs,4)
				ofs=ofs+4
				value=value & "," & Str(v)
		End Select
	Next i
	value=Mid(value,2)
	WritePrivateProfileString(lpszApp,lpszKey,@value,@m_FilePath)
	Return TRUE
End Function 

Property clsIniFile.FilePath() As LPCTSTR
    Return @m_FilePath
End Property

Property clsIniFile.FilePath(sFilePath As LPCTSTR) 
    m_FilePath = *sFilePath
End Property




