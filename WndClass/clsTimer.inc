Type Class_Timer
Private : 
    hWndForm As HWnd '控件句柄
    m_TimerID As Long     '控件IDC
    m_Value As Long 
    m_En As Boolean
    m_Tab As String 
Public : 
    Declare Constructor
    Declare Destructor
    Declare Sub pHWnd(ByVal hWndParent As HWnd)   '用于多开窗口时，要使用控件前，必须先指定控件的父句柄
    Declare Property Enabled() As Boolean                 '返回/设置控件是否允许操作。{=.True.False}
    Declare Property Enabled(ByVal bValue As Boolean)
    Declare Property Interval() As Long                  '返回/设置两次调用 Timer 控件的 Timer 事件间隔的毫秒数。
    Declare Property Interval(ByVal wTimer As Long)
    Declare Property Tag() As String                  '返回/设置 附加数据。
    Declare Property Tag(bValue As String)
    Declare Sub SetHWndID(hWndParent As HWnd,wTimerID As Long ,wTimer As Long ) '设置窗口和IDC值，由VFB创建时自动设置
End Type

'----------------------------------------------------------------------------------------------------------------------------------------------------------------
Constructor Class_Timer
  
End Constructor

Destructor Class_Timer
  
End Destructor

Sub Class_Timer.SetHWndID(hWndParent As HWnd,wTimerID As Long ,wTimer As Long ) '设置窗口和IDC值，由VFB自动设置
  hWndForm = hWndParent
  m_TimerID = wTimerID
  m_Value = wTimer
  SetTimer(hWndForm,m_TimerID,m_Value,Null)
End Sub

Sub Class_Timer.pHWnd(ByVal hWndParent As.HWnd)         '用于多开窗口时，要使用控件前，必须先指定控件的父句柄
  hWndForm = hWndParent
End Sub
Property Class_Timer.Enabled() As Boolean                 '使能
  Return m_En
End Property
Property Class_Timer.Enabled(ByVal bValue As Boolean)
  If m_En Then KillTimer( hWndForm,m_TimerID )
  m_En = bValue
  If m_En Then
      SetTimer(hWndForm,m_TimerID,m_Value,Null)
  End If
End Property
Property Class_Timer.Interval() As Long                  '返回/设置两次调用 Timer 控件的 Timer 事件间隔的毫秒数。
  Return m_Value
End Property
Property Class_Timer.Interval(ByVal wTimer As Long)
  m_Value = wTimer
  If m_En Then
      KillTimer( hWndForm,m_TimerID )
      SetTimer(hWndForm,m_TimerID,m_Value,Null)
  End If
End Property
Property Class_Timer.Tag() As String                  '返回/设置 附加数据。
  Return m_Tab
End Property
Property Class_Timer.Tag(bValue As String)
  m_Tab = bValue
End Property
