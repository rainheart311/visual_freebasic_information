Type Class_CheckBox Extends Class_Control
    Declare Constructor
    Declare Destructor
    Declare Property Value() As Boolean                 '返回/设置选中状态。{=.True.False}
    Declare Property Value(ByVal nCheckState As Boolean)
    Declare Property Caption() As String                '返回/设置控件中的文本
    Declare Property Caption(ByVal sText As String)
End Type

'----------------------------------------------------------------------------------------------------------------------------------------------------------------
Constructor Class_CheckBox
  
End Constructor

Destructor Class_CheckBox
  
End Destructor
Property Class_CheckBox.Caption() As String
  Return AfxGetWindowText(hWndControl)
End Property
Property Class_CheckBox.Caption(ByVal sText As String)
  AfxSetWindowText  hWndControl, sText
End Property

Property Class_CheckBox.Value() As Boolean               '
  If SendMessage(hWndControl, BM_GETCHECK, 0, 0) = BST_CHECKED Then
      Return True
  Else
      Return False
  End If
End Property
Property Class_CheckBox.Value(ByVal nCheckState As Boolean)
  If nCheckState Then
      SendMessage hWndControl, BM_SETCHECK, BST_CHECKED, 0
  Else
      SendMessage hWndControl, BM_SETCHECK, BST_UNCHECKED, 0
  End If
End Property