Type Class_Control
Protected : 
    hWndControl As.hWnd '控件句柄
    m_IDC As Long     '控件IDC
Public : 
    Declare Constructor
    Declare Destructor
    
    Declare Sub Move(ByVal nLeft As Long, ByVal nTop As Long, ByVal nWidth As Long = 0, ByVal nHeight As Long = 0) '设置窗口位置和大小，高度、宽度=0时不修改。
    Declare Sub Refresh()                                 '刷新窗口
    Declare Sub SetFocus()                                ' 获取键盘焦点
    Declare Function Kill() As Boolean                    '从窗体中销毁控件。成功返回 True
    
    Declare Property hWnd() As.hWnd                       '返回/设置控件句柄
    Declare Property hWnd(ByVal hWndNew As.hWnd)
    Declare Property hWndForm() As.hWnd                   '返回/设置控件所在的窗口句柄，主要用于多开窗口后，要使用控件前，必须先指定控件所在的窗口句柄，才能正常使用控件。
    Declare Property hWndForm(ByVal hWndParent As.hWnd)   '获取控件所在的窗口句柄
    Declare Property ID() As Long                         '控件ID
    
    Declare Property Enabled() As Boolean                 '返回/设置控件是否允许操作。{=.True.False}
    Declare Property Enabled(ByVal bValue As Boolean)
    Declare Property Visible() As Boolean                 '显示或隐藏控件。{=.True.False}
    Declare Property Visible(ByVal bValue As Boolean)
    Declare Property AcceptFiles() As Boolean             '返回/设置控件是否接受拖放文件。
    Declare Property AcceptFiles(bValue As Boolean)     
    Declare Property Tag() As String                      '存储程序所需的附加数据。
    Declare Property Tag(ByVal sText As String)
    Declare Property Tag2() As String                     '存储程序所需的附加数据。
    Declare Property Tag2(ByVal sText As String)
    
    Declare Property Left() As Long                       '返回/设置相对于父窗口的 X（像素）
    Declare Property Left(ByVal nLeft As Long)
    Declare Property Top() As Long                        '返回/设置相对于父窗口的 Y（像素）
    Declare Property Top(ByVal nTop As Long)
    Declare Property Width() As Long                      '返回/设置控件宽度（像素）
    Declare Property Width(ByVal nWidth As Long)
    Declare Property Height() As Long                     '返回/设置控件高度（像素）
    Declare Property Height(ByVal nHeight As Long)
	Declare Property Right() As Long
    Declare Property Bottom() As Long
    
    Declare Property Font() As HFONT                      '返回/设置用于在控件中绘制文本的字体
    Declare Property Font(nFont As HFONT)                 '字体句柄不可以自己销毁，VFB自动处理的
    Declare Property FontName() As String                 '控件字体名称
    Declare Property FontName(sFontName As String)    
    Declare Property FontSize() As Integer                '控件字体大小
    Declare Property FontSize(nFontSize As Integer)    
    Declare Property FontItalic() As Boolean              '控件字体斜体标志
    Declare Property FontItalic(bFontItalic As Boolean)     
    Declare Property FontBold() As Boolean                '控件字体粗体标志
    Declare Property FontBold(bFontBold As Boolean)    
    Declare Property FontStrikethru() As Boolean          '控件字体删除线标志
    Declare Property FontStrikethru(bFontStrikethru As Boolean)    
    Declare Property FontUnderline() As Boolean           '控件字体下划线标志
    Declare Property FontUnderline(bFontUnderline As Boolean)    
End Type

'----------------------------------------------------------------------------------------------------------------------------------------------------------------
Constructor Class_Control
  
End Constructor

Destructor Class_Control
  
End Destructor

Sub Class_Control.Move(ByVal nLeft As Long, ByVal nTop As Long, ByVal nWidth As Long, ByVal nHeight As Long)
    Dim rc As Rect
    GetWindowRect hWndControl, @rc          '获得窗体大小
    If nWidth <= 0 Then nWidth = rc.Right - rc.Left
    If nHeight <= 0  Then  nHeight = rc.Bottom - rc.Top
    SetWindowPos(hWndControl, 0, nLeft, nTop, nWidth, nHeight, SWP_NOZORDER Or SWP_NOACTIVATE)
End Sub

Sub Class_Control.Refresh() 
    InvalidateRect(hWndControl, Null, True)
    UpdateWindow(hWndControl)
End Sub

Sub Class_Control.SetFocus()  ' 获取键盘焦点
     .SetFocus hWndControl
End Sub

Function Class_Control.Kill() As Boolean  '从窗体中销毁控件。成功返回 True
    Return DestroyWindow(hWndControl)
End Function

Property Class_Control.hWnd() As .hWnd                    '句柄
  Return hWndControl
End Property

Property Class_Control.hWnd(ByVal hWndNew As .hWnd)        '句柄
    hWndControl = hWndNew
    m_IDC = GetDlgCtrlID(hWndNew)
End Property

Property Class_Control.hWndForm() As.hWnd         '用于多开窗口时，要使用控件前，必须先指定控件的父句柄
  Return GetParent(hWndControl)
End Property
Property Class_Control.hWndForm(ByVal hWndParent As .hWnd)         '用于多开窗口时，要使用控件前，必须先指定控件的父句柄
  hWndControl = GetDlgItem(hWndParent,m_IDC)
End Property

Property Class_Control.ID() As Long                         '控件ID
    Return GetDlgCtrlID(hWndControl)  
End Property

'使能
Property Class_Control.Enabled() As Boolean                 
    Return IsWindowEnabled(hWndControl)
End Property

Property Class_Control.Enabled(ByVal bValue As Boolean)
    EnableWindow(hWndControl, bValue)
End Property

'可见
Property Class_Control.Visible() As Boolean                 
    Return IsWindowVisible(hWndControl)
End Property

Property Class_Control.Visible(ByVal bValue As Boolean)
    If bValue Then
        ShowWindow(hWndControl, SW_SHOW)
    Else
        ShowWindow(hWndControl, SW_HIDE)
    End If
End Property

Property Class_Control.AcceptFiles() As Boolean  '返回/设置窗口是否接受拖放文件。
    Return (AfxGetWindowExStyle(hWndControl) And WS_EX_ACCEPTFILES) = WS_EX_ACCEPTFILES
End Property

Property Class_Control.AcceptFiles(bValue As Boolean )   
    If bValue Then
        AfxAddWindowExStyle hWndControl, WS_EX_ACCEPTFILES
    Else
        AfxRemoveWindowExStyle hWndControl, WS_EX_ACCEPTFILES
    End If
End Property

'附加信息
Property Class_Control.Tag() As String
    Dim pzString As ZString Ptr
    pzString = GetProp(hWndControl, "FLY_TAGPROPERTY")
    If pzString = 0 Then
        Return ""
    Else
        Return *pzString
    End If
End Property

Property Class_Control.Tag(ByVal sText As String)
    If IsWindow(hWndControl) Then
        Dim pzString As ZString Ptr
        pzString = GetProp(hWndControl, "FLY_TAGPROPERTY")                   '从hWnd PROP获取指向Tag字符串的指针。
        If pzString Then HeapFree GetProcessHeap(), 0, ByVal pzString   '释放任何当前分配的内存
        If .Right(sText, 1) <> Chr(0) Then sText = sText & Chr(0)
        pzString = HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, Len(sText) + 1)
        If pzString Then *pzString = sText
        SetProp hWndControl, "FLY_TAGPROPERTY", pzString
    End If
End Property

Property Class_Control.Tag2() As String                      '
    Dim pzString As ZString Ptr
    pzString = GetProp(hWndControl, "FLY_TAGPROPERTY2")  '从hWnd PROP获取指向Tag2字符串的指针。
    If pzString = 0 Then 'Null pointer
        Return ""
    Else
        Return *pzString
    End If
End Property

Property Class_Control.Tag2(ByVal sText As String)
    If IsWindow(hWndControl) Then
        Dim pzString As ZString Ptr
      ''从hWnd PROP获取指向Tag字符串的指针。
        pzString = GetProp(hWndControl, "FLY_TAGPROPERTY2")
        If pzString Then HeapFree GetProcessHeap(), 0, ByVal pzString   '释放任何当前分配的内存
        If .Right(sText, 1) <> Chr(0) Then sText = sText & Chr(0)
        pzString = HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, Len(sText) + 1)
        If pzString Then *pzString = sText
        SetProp hWndControl, "FLY_TAGPROPERTY2", pzString
    End If
End Property

'左上角位置
Property Class_Control.Left() As Long                '
    If IsWindow(hWndControl) Then
        Dim rc As Rect
        GetWindowRect hWndControl, @rc          '获得窗体大小
        MapWindowPoints HWND_DESKTOP, GetParent(hWndControl), Cast(LPPOINT, @rc), 2
        Return rc.Left
    End If
End Property

Property Class_Control.Left(ByVal nLeft As Long)
    If IsWindow(hWndControl) Then
        Dim rc As Rect
        GetWindowRect hWndControl, @rc          '获得窗体大小
        MapWindowPoints HWND_DESKTOP, GetParent(hWndControl), Cast(LPPOINT, @rc), 2
        SetWindowPos(hWndControl, 0, nLeft, rc.top, 0, 0, SWP_NOZORDER Or SWP_NOSIZE  Or SWP_NOACTIVATE)
    End If
End Property

Property Class_Control.Top() As Long     '
    If IsWindow(hWndControl) Then
        Dim rc As Rect
        GetWindowRect hWndControl, @rc     '获得窗体大小
        MapWindowPoints HWND_DESKTOP, GetParent(hWndControl), Cast(LPPOINT, @rc), 2
        Return rc.Top
    End If 
End Property

Property Class_Control.Top(ByVal nTop As Long)
    If IsWindow(hWndControl) Then
        Dim rc As Rect
        GetWindowRect hWndControl, @rc          '获得窗体大小
        MapWindowPoints HWND_DESKTOP, GetParent(hWndControl), Cast(LPPOINT, @rc), 2
        SetWindowPos(hWndControl, 0, rc.Left, nTop, 0, 0, SWP_NOZORDER Or SWP_NOSIZE  Or SWP_NOACTIVATE)
    End If
End Property

'大小
Property Class_Control.Height() As Long 
    If IsWindow(hWndControl) Then               
        Dim rc As Rect
        GetWindowRect hWndControl, @rc          '获得窗体大小
        Return rc.Bottom - rc.Top
    End If 
End Property

Property Class_Control.Height(ByVal nHeight As Long)
    If IsWindow(hWndControl) Then
        Dim rc As Rect
        GetWindowRect hWndControl, @rc          '获得窗体大小
        SetWindowPos(hWndControl, 0, 0, 0, rc.Right - rc.Left, nHeight, SWP_NOZORDER Or SWP_NOMOVE Or SWP_NOACTIVATE)
    End If
End Property

Property Class_Control.Width() As Long
    If IsWindow(hWndControl) Then
        Dim rc As Rect
        GetWindowRect hWndControl, @rc          '获得窗体大小
        Return rc.Right - rc.Left
    End If 
End Property

Property Class_Control.Width(ByVal nWidth As Long)
    If IsWindow(hWndControl) Then
        Dim rc As Rect
        GetWindowRect hWndControl, @rc          '获得窗体大小
        SetWindowPos(hWndControl, 0, 0, 0, nWidth, rc.Bottom - rc.Top, SWP_NOZORDER Or SWP_NOMOVE Or SWP_NOACTIVATE)
    End If
End Property

Property Class_Control.Right() As Long
    If IsWindow(hWndControl) Then
        Dim rc As Rect
        GetWindowRect hWndControl, @rc          '获得窗体大小
		MapWindowPoints HWND_DESKTOP, GetParent(hWndControl), Cast(LPPOINT, @rc), 2
        Return rc.Right
    End If 
End Property

Property Class_Control.Bottom() As Long
    If IsWindow(hWndControl) Then
        Dim rc As Rect
        GetWindowRect hWndControl, @rc          '获得窗体大小
		MapWindowPoints HWND_DESKTOP, GetParent(hWndControl), Cast(LPPOINT, @rc), 2
        Return rc.bottom
    End If 
End Property

Property Class_Control.Font() As HFONT                  '返回/设置用于在控件中绘制文本的字体
    Return Cast(HFONT, SendMessage( hWndControl, WM_GETFONT, 0, 0 ))
End Property

Property Class_Control.Font(nFont As HFONT)
    Dim ff_control As FLY_DATA Ptr 
    Dim lb         As Long                
    Dim ub         As Long
    Dim x          As Long
    Dim nFound     As Long
    Dim nMargins   As Long
    Dim IsTextBox  As Long
    Dim zClassName As ZString * 50
    
    '检查窗口句柄是否有效
    If IsWindow(hWndControl) Then  
        GetClassName hWndControl, zClassName, SizeOf(zClassName)
        If UCase(zClassName) = "EDIT" Then IsTextBox = True
        
        If IsTextBox Then
            nMargins = SendMessage(hWndControl, EM_GETMARGINS, 0, 0)
        End If
        
        ff_control = GetProp(hWndControl, "FLY_PTR")
        
        If ff_control Then
           ' Delete existing font only if it does not exist (other controls may be using it)
           lb = LBound(gFLY_FontHandles)
           ub = UBound(gFLY_FontHandles)
   
           For x = lb To ub
              If gFLY_FontHandles(x) = ff_control->hFont Then        
                 nFound = -1: Exit For
              End If   
           Next

           If nFound = 0 Then  
              DeleteObject ff_control->hFont
           End If
           
           ' Set the new font
           ff_control->hFont = nFont
           SendMessage hWndControl, WM_SETFONT, Cast(wParam, ff_control->hFont), True
        Else
           SendMessage hWndControl, WM_SETFONT, Cast(wParam, nFont), True
        End If
       
        If IsTextBox Then
          SendMessage hWndControl, EM_SETMARGINS, EC_LEFTMARGIN Or EC_RIGHTMARGIN, nMargins
        End If
    End If
  
End Property

Property Class_Control.FontName() As string              
    If IsWindow(hWndControl) Then 
        Dim lf As LOGFONT
        GetObject(Font,SizeOf(LOGFONT),@lf)
        Return lf.lfFaceName   
    End If
End Property

Property Class_Control.FontName(sFontName As string)   
    If IsWindow(hWndControl) Then 
       Dim lf As LOGFONT
       GetObject(Font,SizeOf(LOGFONT),@lf)
       lf.lfFaceName = sFontName  
       Font = CreateFontIndirect(@lf)
    End If
End Property

Property Class_Control.FontSize() As Integer               
    If IsWindow(hWndControl) Then   
        Dim lf As LOGFONT
        Dim m_hDC As hDC
        GetObject(Font,SizeOf(LOGFONT),@lf)
        m_hDC = GetDC(hWndControl)
        Property = MulDiv(-lf.lfHeight, 72, GetDeviceCaps(m_hDC, LogPixelsY))
        ReleaseDC(hWndControl,m_hDC) 
    End If
End Property

Property Class_Control.FontSize(nFontSize As Integer)    
    If IsWindow(hWndControl) Then 
       Dim lf As LOGFONT
       Dim m_hDC As hDC
       GetObject(Font,SizeOf(LOGFONT),@lf)
       m_hDC = GetDC(hWndControl)
       lf.lfHeight = -MulDiv(nFontSize, GetDeviceCaps(m_hDC, LOGPIXELSY), 72)  
       ReleaseDC(hWndControl,m_hDC) 
       Font = CreateFontIndirect(@lf)
    End If
End Property

Property Class_Control.FontItalic() As Boolean             
    If IsWindow(hWndControl) Then   
       Dim lf As LOGFONT
        GetObject(Font,SizeOf(LOGFONT),@lf)
        Return Cast(Boolean,lf.lfItalic)
    End If
End Property

Property Class_Control.FontItalic(bFontItalic As Boolean)     
    If IsWindow(hWndControl) Then 
       Dim lf As LOGFONT
       GetObject(Font,SizeOf(LOGFONT),@lf)
       lf.lfItalic = bFontItalic
       Font = CreateFontIndirect(@lf)
    End If
End Property

Property Class_Control.FontBold() As Boolean               
    If IsWindow(hWndControl) Then   
       Dim lf As LOGFONT
        GetObject(Font,SizeOf(LOGFONT),@lf)
        Return IIf(lf.lfWeight = 700,TRUE,FALSE) '400是正常值，700是粗体
    End If
End Property

Property Class_Control.FontBold(bFontBold As Boolean)  
    If IsWindow(hWndControl) Then 
       Dim lf As LOGFONT
       GetObject(Font,SizeOf(LOGFONT),@lf)
       lf.lfWeight = IIf(bFontBold,700,400)
       Font = CreateFontIndirect(@lf)
    End If
End Property

Property Class_Control.FontStrikethru() As Boolean          
    If IsWindow(hWndControl) Then   
       Dim lf As LOGFONT
        GetObject(Font,SizeOf(LOGFONT),@lf)
        Return Cast(Boolean,lf.lfStrikeOut)
    End If
End Property

Property Class_Control.FontStrikethru(bFontStrikethru As Boolean)    
    If IsWindow(hWndControl) Then 
       Dim lf As LOGFONT
       GetObject(Font,SizeOf(LOGFONT),@lf)
       lf.lfStrikeOut = bFontStrikethru
       Font = CreateFontIndirect(@lf)
    End If
End Property

Property Class_Control.FontUnderline() As Boolean           
    If IsWindow(hWndControl) Then   
       Dim lf As LOGFONT
        GetObject(Font,SizeOf(LOGFONT),@lf)
        Return Cast(Boolean,lf.lfUnderline)
    End If
End Property

Property Class_Control.FontUnderline(bFontUnderline As Boolean)    
    If IsWindow(hWndControl) Then 
       Dim lf As LOGFONT
       GetObject(Font,SizeOf(LOGFONT),@lf)
       lf.lfUnderline = bFontUnderline
       Font = CreateFontIndirect(@lf)
    End If
End Property


