Type Class_ComboBox Extends Class_Control
    Declare Constructor
    Declare Destructor
	
	Declare Function AddItem(sText As String) As Long                       '新增项目，返回新添加的索引（从0开始）,失败返回CB_ERR
    Declare Function InsertItem(nIndex As Long, TheText As String) As Long  '插入项目，返回新添加的索引（从0开始）,失败返回CB_ERR
	Declare Function RemoveItem(nIndex As Long) As Long                     '删除一个项目，返回剩余项目数
	Declare Sub Clear()                                                     '删除组合框中所有项目。
	
    Declare Property Text() As String                                       '返回/设置控件中的文本
    Declare Property Text(ByVal sText As String)
	Declare Property TextLimit() As Long                 '返回/设置当前的文本长度限制，以字符为单位。
    Declare Property TextLimit(MaxCharacters As Long)
    Declare Property List(nIndex As Long)  As String                        '返回/设置项目文本
    Declare Property List(nIndex As Long, sText As String)
    Declare Function ListCount() As Long                                    '检索组合框列表框中的项目数。
    Declare Property ListIndex() As Long                                    '返回/设置项目
    Declare Property ListIndex(nIndex As Long)
    Declare Property ItemData(nIndex As Long)  As Integer                   '返回/设置指定项目在组合框中关联的值。
    Declare Property ItemData(nIndex As Long, nValue As Integer)
    Declare Property ReDraw()  As Boolean                                   '设置重绘标志，大量新增项目时关闭重绘，避免控件闪耀和提高速度。{=.True.False}
    Declare Property ReDraw(nValue As Boolean)                              '返回不使用
    Declare Property ItemHeight() As Long                                   '返回/设置每行的高度，单位像素，如：AfxScaleY(15)，可变行高自绘控件时无效
    Declare Property ItemHeight(nValue As Long)  

	Declare Function GetTopIndex() As Long                                            '检索组合框的列表框部分中第一个可见项目的从零开始的索引。
    Declare Function FindString(indexStart As Long, lpszFind As String) As Long       '搜索以指定字符串中的字符开头的项目。如果搜索不成功，则为CB_ERR。
    Declare Function FindStringExact(indexStart As Long, lpszFind As String) As Long  '查找包含指定字符串在组合框中的第一个项目。。如果搜索不成功，则为CB_ERR。
    Declare Function FindItemData(indexStart As Long, nData As Integer) As Long       '查找包含指定项目数据在组合框中的第一个项目。。如果搜索不成功，则为CB_ERR。
End Type

'----------------------------------------------------------------------------------------------------------------------------------------------------------------
Constructor Class_ComboBox
  
End Constructor

Destructor Class_ComboBox
  
End Destructor

Function Class_ComboBox.AddItem(sText As String) As Long  '新增项目，返回新添加的索引（从0开始）
    Function = SendMessage(hWndControl, CB_ADDSTRING, 0, Cast(lParam, StrPtr(sText)))
End Function

Function Class_ComboBox.InsertItem(nIndex As Long, TheText As String) As Long  '插入项目，返回新添加的索引（从0开始）
    Function = SendMessage( hWndControl, CB_INSERTSTRING, nIndex, Cast(lParam, StrPtr(TheText)))
End Function

Function Class_ComboBox.RemoveItem(nIndex As Long) As Long  '删除一个项目，返回剩余项目数
    Function = SendMessage( hWndControl, CB_DELETESTRING, nIndex, 0)
End Function

Sub Class_ComboBox.Clear() '删除组合框中所有项目。
    SendMessage hWndControl, CB_RESETCONTENT, 0, 0
End Sub

Property Class_ComboBox.Text() As String
    Return AfxGetWindowText(hWndControl)
End Property

Property Class_ComboBox.Text(ByVal sText As String)
    AfxSetWindowText hWndControl, sText
End Property

Property Class_ComboBox.TextLimit() As Long                 '返回/设置当前的文本长度限制，以字符为单位。
    Return SendMessage(hWndControl, EM_GETLIMITTEXT, 0, 0)
End Property

Property Class_ComboBox.TextLimit(MaxCharacters As Long)
    SendMessage hWndControl, EM_SETLIMITTEXT, MaxCharacters, 0
End Property

Property Class_ComboBox.List(nIndex As Long)  As String        '返回/设置项目文本
    Dim nBufferSize As Long
    Dim nBuffer     As String
    '检查窗口句柄是否有效
    If IsWindow(hWndControl) Then  
       ' 得到的文本的长度
         nBufferSize = SendMessage(hWndControl, CB_GETLBTEXTLEN, nIndex, 0)
         If (nBufferSize = 0) Or (nBufferSize = CB_ERR) Then 
            Return  ""
         End If                       
       ' Add an extra character for the Nul terminator
         nBufferSize = nBufferSize + 1
       ' Create the temporary buffer
         nBuffer = Space(nBufferSize)
       ''发消息给控件
         If SendMessage( hWndControl, CB_GETLBTEXT, nIndex, Cast(lParam, StrPtr(nBuffer))) = CB_ERR Then
            Return  ""
         Else
            ' Remove the Null
            nBuffer = RTrim(nBuffer, Chr(0))
            Return  nBuffer
         End If      
    End If 
End Property

Property Class_ComboBox.List(nIndex As Long, sText As String)
    If IsWindow(hWndControl) Then
        SendMessage(hWndControl, CB_DELETESTRING, nIndex, 0)
        SendMessage(hWndControl, CB_INSERTSTRING, nIndex, Cast(lParam, StrPtr(sText)))
    End If
End Property

Function Class_ComboBox.ListCount() As Long  '检索组合框列表框中的项目数。
    Function = SendMessage( hWndControl, CB_GETCOUNT, 0, 0)
End Function

Property Class_ComboBox.ListIndex() As Long                  '返回/设置项目
    Property = SendMessage( hWndControl, CB_GETCURSEL, 0, 0)
End Property

Property Class_ComboBox.ListIndex(nIndex As Long)
    SendMessage( hWndControl, CB_SETCURSEL, nIndex, 0)
End Property 

Property Class_ComboBox.ItemData(nIndex As Long)  As Integer         '返回/设置指定项目在组合框中关联的 32 位值
    Property = SendMessage( hWndControl, CB_GETITEMDATA, nIndex, 0)
End Property

Property Class_ComboBox.ItemData(nIndex As Long, nValue As Integer)
    SendMessage( hWndControl, CB_SETITEMDATA, nIndex, nValue)
End Property

Property Class_ComboBox.ReDraw()  As Boolean         '返回/设置重绘标志，大量新增项目时关闭重绘，避免控件闪耀和提高速度。{=.True.False}
    Property = False '无需返回
End Property

Property Class_ComboBox.ReDraw(nValue As Boolean)
    SendMessage( hWndControl, WM_SETREDRAW, nValue, 0)
End Property

Property Class_ComboBox.ItemHeight() As Long  '返回/设置每行的高度，单位像素，如：AfxScaleY(15)
    Property = SendMessage( hWndControl, CB_GETITEMHEIGHT, 0, 0)
End Property

Property Class_ComboBox.ItemHeight(nValue As Long) 
    SendMessage( hWndControl, CB_SETITEMHEIGHT, 0, nValue)
End Property

Function Class_ComboBox.GetTopIndex() As Long  '检索组合框的列表框部分中第一个可见项目的从零开始的索引。
    Function = SendMessage( hWndControl, CB_GETTOPINDEX, 0, 0)
End Function

Function Class_ComboBox.FindString(indexStart As Long, lpszFind As String) As Long  '搜索以指定字符串中的字符开头的项目
    Function = SendMessage( hWndControl, CB_FINDSTRING, indexStart, Cast(lParam, StrPtr(lpszFind)))
End Function

Function Class_ComboBox.FindStringExact(indexStart As Long, lpszFind As String) As Long  '查找包含指定字符串在组合框中的第一个项目。
    Function = SendMessage( hWndControl, CB_FINDSTRINGEXACT, indexStart, Cast(lParam, StrPtr(lpszFind)))
End Function

Function Class_ComboBox.FindItemData(indexStart As Long, nData As Integer) As Long  '查找包含指定项目数据在组合框中的第一个项目。
    Function = SendMessage( hWndControl, CB_FINDSTRING, indexStart, nData)
End Function



