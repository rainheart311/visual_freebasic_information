#Include Once "win\cderr.bi"
#Include Once "win\commdlg.bi"

#Define DefFontName    "宋体"
#Define DefFontSize    9
#Define DefMaxFontSize 72
#Define DefMinFontSize 8
#Define DefFontColor   &H000000      '默认黑色

Type clsCommonDialog
Private:
    m_PrinterFlags As DWORD               '
    As WORD m_Copies,m_FromPage,m_ToPage,m_MinPage,m_MaxPage
#Ifndef UNICODE  
    m_DialogTitle  As ZString * MAX_PATH
    m_FilePath     As ZString * MAX_PATH
    m_FileTitle    As ZString * MAX_PATH
    m_InitDir      As ZString * MAX_PATH
    m_Filter       As ZString * MAX_PATH
    
    m_FontName     As ZString * MAX_PATH 
#Else
    m_DialogTitle  As WString * MAX_PATH
    m_FilePath     As WString * MAX_PATH
    m_FileTitle    As WString * MAX_PATH
    m_InitDir      As WString * MAX_PATH
    m_Filter       As WString * MAX_PATH
    
    m_FontName     As WString * MAX_PATH 
#EndIf
    m_FileFlags    As DWORD 
    m_MultiSel     As Boolean 
    m_ForeColor    As COLORREF 
    m_FontSize     As Integer 
    As Boolean m_FontItalic,m_FontBold,m_FontStrikethru,m_FontUnderline
Public:
    Declare Constructor()
    Declare Destructor()
    Declare Function ShowColor(ByVal hOwner As HWND = NULL) As COLORREF    '打开颜色对话框及其属性    
    Declare Function ShowPrinter(ByVal hOwner As HWND = NULL) As Boolean   '打开打印机选项对话框及其属性
    Declare Property PrinterFlags() As DWORD             '
    Declare Property PrinterFlags(ByVal dwTxt As DWORD)
    Declare Property Copies() As WORD             '打印份数
    Declare Property Copies(ByVal wTxt As WORD)
    Declare Property FromPage() As WORD             '起始页
    Declare Property FromPage(ByVal wTxt As WORD)
    Declare Property ToPage() As WORD             '结束页
    Declare Property ToPage(ByVal wTxt As WORD)
    Declare Property MinPage() As WORD             '最小页
    Declare Property MinPage(ByVal wTxt As WORD)
    Declare Property MaxPage() As WORD             '最大页
    Declare Property MaxPage(ByVal wTxt As WORD)
    Declare Function ShowOpen(ByVal hOwner As HWND = NULL) As LPCTSTR    '打开文件、保存文件对话框及其属性
    Declare Function ShowSave(ByVal hOwner As HWND = NULL) As LPCTSTR  
    Declare Property DialogTitle() As LPCTSTR                  '
    Declare Property DialogTitle(lpszText As LPCTSTR)
    Declare Property FilePath OverLoad() As LPCTSTR                   '
    Declare Property FilePath OverLoad(ByVal Index As Integer) As LPCTSTR 
    Declare Property FilePath(lpszText As LPCTSTR)
    Declare Property FileTitle() As LPCTSTR                  '
    Declare Property FileTitle(lpszText As LPCTSTR)
    Declare Property InitDir() As LPCTSTR                    '
    Declare Property InitDir(lpszText As LPCTSTR)
    Declare Property Filter() As LPCTSTR                   '
    Declare Property Filter(lpszText As LPCTSTR)
    Declare Property MultiSel() As Boolean               '
    Declare Property MultiSel(bMultiSel As Boolean)
    Declare Function ShowBrowse(ByVal hOwner As HWND = NULL) As LPCTSTR    '打开文件浏览器对话框及其属性  
    Declare Function FolderExists(lpszPath As LPCTSTR) As Boolean '
    Declare Function CreateFolder(lpszPath As LPCTSTR) As Boolean 
    Declare Function ShowFont(ByVal hOwner As HWND = NULL) As Boolean    '打开字体对话框及其属性
    Declare Property ForeColor() As COLORREF                  '前景色
    Declare Property ForeColor(nColor As COLORREF)
    Declare Property FontName() As LPCTSTR                   '
    Declare Property FontName(lpszText As LPCTSTR )
    Declare Property FontSize() As Integer                    '
    Declare Property FontSize(nSize As Integer)
    Declare Property FontItalic() As Boolean               '
    Declare Property FontItalic(bItalic As Boolean)
    Declare Property FontBold() As Boolean               '
    Declare Property FontBold(bBold As Boolean)
    Declare Property FontStrikethru() As Boolean               '
    Declare Property FontStrikethru(bStrikethru As Boolean)
    Declare Property FontUnderline() As Boolean               '
    Declare Property FontUnderline(bUnderline As Boolean)  '    
End Type

'----------------------------------------------------------------------------------------------------------------------------------------------------------------
'
Constructor clsCommonDialog
'-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
'
    m_PrinterFlags = PD_ALLPAGES               '
    m_Copies = 1
    m_FromPage = 1
    m_ToPage = 1
    m_MinPage = 1
    m_MaxPage = 1
'-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
'
    m_DialogTitle = "对话框"
    m_FilePath = ""
    m_FileTitle = ""
    m_InitDir = CurDir
    m_Filter = "所有文件(*.*)|*.*"
    m_MultiSel = False
    m_FileFlags = OFN_EXPLORER Or OFN_FILEMUSTEXIST Or OFN_HIDEREADONLY 
'-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
'
    m_FontName = DefFontName
    m_FontSize = DefFontSize '
    m_ForeColor = DefFontColor
    
    m_FontItalic = False
    m_FontBold = False
    m_FontStrikethru = False
    m_FontUnderline = False
End Constructor

Destructor clsCommonDialog

End Destructor
'-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
'设置颜色对话框
Function clsCommonDialog.ShowColor(ByVal hOwner As HWND = NULL) As COLORREF
	If hOwner = NULL Then hOwner = GetForegroundWindow()    '如果没有传入句柄，则获取前台窗口的句柄
	
    Dim ColorSpec As ChooseColor, lCustomColor(15) AS LONG
    ColorSpec.lStructSize  = SIZEOF(ColorSpec)
    ColorSpec.hwndOwner    = hOwner     ' // Handle of owner window.  If 0, dialog appears at top/left.
    For lCounter AS LONG = 0 TO 15
        lCustomColor(lCounter) = BGR(0, lCounter * 16, (15 - lCounter) * 16)
    Next
    ColorSpec.lpCustColors = VARPTR(lCustomColor(0))

    ColorSpec.rgbResult = m_ForeColor
    ColorSpec.Flags = ColorSpec.Flags OR CC_RGBINIT OR CC_FULLOPEN
    Function = IIf(ChooseColor(@ColorSpec), Colorspec.rgbResult, -1)
End Function
'-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
'
Function clsCommonDialog.ShowPrinter(ByVal hOwner As HWND = NULL) As Boolean
    If hOwner = NULL Then hOwner = GetForegroundWindow()    '如果没有传入句柄，则获取前台窗口的句柄
    
    DIM pd AS PrintDlg
    
    pd.lStructSize = SIZEOF(pd)
    pd.hwndOwner = hOwner
    pd.flags = m_PrinterFlags OR PD_SHOWHELP
    pd.nCopies = m_Copies
    pd.nFromPage = MAX(m_FromPage, m_MinPage)
    pd.nToPage = MAX(MIN(m_ToPage, m_MaxPage), m_MaxPage)
    pd.nMinPage = m_MinPage
    pd.nMaxPage = m_MaxPage
    If PrintDlg(@pd) Then
        'hdc = pd.hDC
        m_Copies = pd.nCopies
        m_FromPage = pd.nFromPage
        m_ToPage = pd.nToPage
        m_PrinterFlags = pd.flags
        m_MinPage = pd.nMinPage
        m_MaxPage = pd.nMaxPage
        If pd.hDevMode  Then GlobalFree(pd.hDevMode)
        If pd.hDevNames Then GlobalFree(pd.hDevNames)
        Return TRUE
    End If
    Return FALSE
End Function

Property clsCommonDialog.PrinterFlags() As DWORD 
    Return m_PrinterFlags
End Property
            '
Property clsCommonDialog.PrinterFlags(ByVal dwTxt As DWORD)
    m_PrinterFlags = dwTxt
End Property
    
Property clsCommonDialog.Copies() As WORD             '打印份数
    Return m_Copies
End Property

Property clsCommonDialog.Copies(ByVal wTxt As WORD)
    m_Copies = wTxt
End Property
    
Property clsCommonDialog.FromPage() As WORD             '起始页
    Return m_FromPage
End Property

Property clsCommonDialog.FromPage(ByVal wTxt As WORD)
    m_FromPage = wTxt
End Property
    
Property clsCommonDialog.ToPage() As WORD             '结束页
    Return m_ToPage
End Property

Property clsCommonDialog.ToPage(ByVal wTxt As WORD)
    m_ToPage = wTxt
End Property
    
Property clsCommonDialog.MinPage() As WORD             '最小页
    Return m_MinPage
End Property

Property clsCommonDialog.MinPage(ByVal wTxt As WORD)
    m_MinPage = wTxt
End Property
    
Property clsCommonDialog.MaxPage() As WORD             '最大页
    Return m_MaxPage
End Property

Property clsCommonDialog.MaxPage(ByVal wTxt As WORD)
    m_MaxPage = wTxt 
End Property
'-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
'
Function clsCommonDialog.ShowOpen(ByVal hOwner As HWND = NULL) As LPCTSTR 
    If hOwner = NULL Then hOwner = GetForegroundWindow()  '如果没有传入句柄，则获取前台窗口的句柄      
    
    Dim dwFlags As DWORD,dwBufLen As DWORD
    If m_FileFlags Then dwFlags = m_FileFlags
    'If pdwBufLen Then dwBufLen = *pdwBuflen

   ' // Filter is a sequence of WSTRINGs with a final (extra) double null terminator
   ' // The "|" characters are replaced with nulls
    Dim szMarkers As TSTR * 4 = "||"
    If Right(m_Filter, 1) <> "|" Then szMarkers += "|"
    Dim szFilter As TSTR * MAX_PATH = m_Filter & szMarkers
    Dim dwFilterStrSize As DWORD = Len(szFilter)
   ' // Replace markers("|") with nulls
    Dim pchar AS TCHAR Ptr = @szFilter
    For i As Long = 0 To Len(szFilter) - 1
        If pchar[i] = Asc("|") Then pchar[i] = 0
    Next
    
    m_FilePath = ""

   ' // If the initial directory has not been specified, assume the current directory
    If Len(m_InitDir) = 0 Then m_InitDir = CurDir 
   ' // The size of the buffer must be at least MAX_PATH characters
    If dwBufLen = 0 Then 
        If (dwFlags AND OFN_ALLOWMULTISELECT = OFN_ALLOWMULTISELECT) THEN dwBufLen = 32768  ' // 64 Kb buffer
    End If
    If dwBufLen < 260 Then dwBufLen = 260   ' // Make room for at least one path
   ' // Allocate the file name and a marker ("|") to be replaced with a null
    m_FilePath &= "|"
   ' // Store the position of the marker
    Dim cbPos As Long = Len(m_FilePath) - 1
   ' // Allocate room for the buffer
    If Len(m_FilePath) < dwBufLen Then m_FilePath += Space(dwBufLen - Len(m_FilePath))
    Dim dwFileStrSize As DWORD = Len(m_FilePath)
   ' // The filename must be null terminated (replace the marker with a null)
    pchar = @m_FilePath
    pchar[cbPos] = 0

   ' // Fill the members of the structure
    Dim ofn AS OPENFILENAME
    ofn.lStructSize     = SizeOf(ofn)
    'If AfxWindowsVersion < 5 THEN ofn.lStructSize = 76
    ofn.hwndOwner       = hOwner
    ofn.lpstrFilter     = @szFilter
    ofn.nFilterIndex    = 1
    ofn.lpstrFile       = @m_FilePath
    ofn.nMaxFile        = dwFileStrSize
    ofn.lpstrInitialDir = @m_InitDir
    If Len(m_DialogTitle) Then ofn.lpstrTitle = @m_DialogTitle
    ofn.Flags = dwFlags OR OFN_EXPLORER
    'If Len(wszDefExt) THEN ofn.lpstrDefExt = @wszDefExt

   ' // Call the open file dialog
    If GetOpenFileName(@ofn) Then 
        pchar = @m_FilePath
        For i AS Long = 0 To dwFileStrSize - 1
         ' // If double null, exit
            If pchar[i] = 0 AndAlso pchar[i + 1] = 0 Then Exit For 
         ' // Replace null with ","
            If pchar[i] = 0 Then pchar[i] = Asc(",")
        Next
      ' // Trim trailing spaces
        m_FilePath = RTrim(m_FilePath,Chr(32))
        If Right(m_FilePath, 1) = "," Then m_FilePath = Left(m_FilePath, Len(m_FilePath) - 1)
    Else
      ' // Buffer too small
        If CommDlgExtendedError = FNERR_BUFFERTOOSMALL Then 
            dwBufLen = Asc(m_FilePath)
        End If
        m_FilePath = ""
    End If 

   ' // Return the retrieved values
    If m_FileFlags Then m_FileFlags = ofn.Flags
    'If pdwBufLen Then pdwBufLen = dwBufLen
    Return @m_FilePath
End Function

Function clsCommonDialog.ShowSave(ByVal hOwner As HWND = NULL) As LPCTSTR
    If hOwner = NULL Then hOwner = GetForegroundWindow()  '如果没有传入句柄，则获取前台窗口的句柄
    
    Dim dwFlags As DWORD
    If m_FileFlags Then dwFlags = m_FileFlags

   ' // Filter is a sequence of WSTRINGs with a final (extra) double null terminator
   ' // The "|" characters are replaced with nulls
#Ifndef UNICODE
    Dim szMarkers As ZString * 4 = "||"
#Else
    Dim szMarkers As WString * 4 = "||"
#EndIf
    If Right(m_Filter, 1) <> "|" Then szMarkers += "|"
#Ifndef UNICODE
    Dim szFilter As ZString * MAX_PATH = m_Filter & szMarkers
#Else
    Dim szFilter As WString * MAX_PATH = m_Filter & szMarkers
#EndIf
    Dim dwFilterStrSize AS DWORD = Len(szFilter)
   ' // Replace markers("|") with nulls
    Dim pchar As TCHAR Ptr = @szFilter
    Dim i As  Long 
    For i = 0 TO Len(szFilter) - 1
        If pchar[i] = Asc("|") Then pchar[i] = 0
    Next

   ' // If the initial directory has not been specified, assume the current directory
    If Len(m_InitDir) = 0 Then m_InitDir = CurDir
#Ifndef UNICODE
    Dim szFile As ZString * MAX_PATH = m_FilePath & "|"
#Else
    Dim szFile As WString * MAX_PATH = m_FilePath & "|"
#EndIf
   ' // Store the position of the marker
    Dim cbPos As Long = Len(szFile) - 1
   ' // Allocate room for the buffer
    If Len(szFile) < MAX_PATH Then szFile += SPACE(MAX_PATH - LEN(szFile))
    Dim dwFileStrSize AS DWORD = LEN(szFile)
   ' // The filename must be null terminated (replace the marker with a null)
    pchar = @szFile
    pchar[cbPos] = 0

   ' // Fill the members of the structure
    Dim ofn AS OPENFILENAME
    ofn.lStructSize     = SIZEOF(ofn)
    'If AfxWindowsVersion < 5 THEN ofn.lStructSize = 76
    ofn.lpstrFilter     = @szFilter
    ofn.nFilterIndex    = 1
    ofn.lpstrFile       = @szFile
    ofn.nMaxFile        = dwFileStrSize
    ofn.lpstrInitialDir = @m_InitDir
    If Len(m_DialogTitle) THEN ofn.lpstrTitle = @m_DialogTitle
    ofn.Flags = dwFlags OR OFN_EXPLORER
    'If Len(wszDefExt) THEN ofn.lpstrDefExt = @wszDefExt

   ' // Call the save filename dialog
    If GetSaveFileName(@ofn) = 0 THEN szFile = ""

   ' // Return the retrieved values
    If m_FileFlags THEN m_FileFlags = ofn.Flags
    
    m_FilePath = szFile
    Return @m_FilePath
End Function
      
Property clsCommonDialog.DialogTitle() As LPCTSTR    
    Return @m_DialogTitle
End Property
                '
Property clsCommonDialog.DialogTitle(lpszText As LPCTSTR)
    m_DialogTitle = *lpszText
End Property
    
Property clsCommonDialog.FilePath OverLoad() As LPCTSTR    
    Return @m_FilePath
End Property

Private Function GetFileName(lpszPath As LPCTSTR,ByVal Index As Integer) As LPCTSTR
    Dim As Integer i,j = Len(*lpszPath),k = 0
#Ifndef UNICODE
    Dim sTmp() As ZString * MAX_PATH
#Else
    Dim sTmp() As WString * MAX_PATH
#EndIf
    Do 
        i = j
        j = InStrRev(*lpszPath,Chr(0),i - 1)
        'Print Len(*lpszPath),j & "  ";
    #Ifndef UNICODE
        ReDim Preserve sTmp(k) As ZString * MAX_PATH
    #Else
        ReDim Preserve sTmp(k) As WString * MAX_PATH
    #EndIf
        sTmp(k) = Mid(*lpszPath,j+1,i - j)
        'Print sTmp(k)
        
        k += 1
    Loop While j <> 0
    
    If Index >= k Then Index = 0
    
#Ifndef UNICODE
    Dim t As ZString * MAX_PATH = Trim(sTmp(k - 1)) & "\" & Trim(sTmp(k - Index - 2))
#Else
    Dim t As WString * MAX_PATH = Trim(sTmp(k - 1)) & "\" & Trim(sTmp(k - Index - 2))
#EndIf
    
    Return Cast(LPCTSTR,@t) 
End Function

Property clsCommonDialog.FilePath OverLoad(ByVal Index As Integer) As LPCTSTR    
    Return GetFileName(@m_FilePath,index)
End Property
               '
Property clsCommonDialog.FilePath(lpszText As LPCTSTR)
    m_FilePath = *lpszText
End Property
    
Property clsCommonDialog.FileTitle() As LPCTSTR    
    m_FileTitle = m_FilePath
    Dim nPos AS Long = InstrRev(m_FilePath,Any ":/\")
    IF nPos Then m_FileTitle = Mid(m_FilePath, nPos + 1)
    nPos = InstrRev(m_FileTitle, ".")
    If nPos Then m_FileTitle = Mid(m_FileTitle, 1, nPos - 1)

    Return @m_FileTitle
End Property
                '
Property clsCommonDialog.FileTitle(lpszText As LPCTSTR)
    m_FileTitle = *lpszText
End Property
    
Property clsCommonDialog.InitDir() As LPCTSTR    
    Return @m_InitDir
End Property
                '
Property clsCommonDialog.InitDir(lpszText As LPCTSTR)
    If Len(*lpszText) > 0 Then
    	m_InitDir = *lpszText
    	If Right(m_InitDir,1) = "\" Then m_InitDir = Left(m_InitDir,Len(m_InitDir) - 1)
    EndIf
End Property
    
Property clsCommonDialog.Filter() As LPCTSTR 
    Return @m_Filter
End Property
                '
Property clsCommonDialog.Filter(lpszText As LPCTSTR)
    m_Filter = *lpszText
End Property
    
Property clsCommonDialog.MultiSel() As Boolean   
    Return m_MultiSel
End Property           

Property clsCommonDialog.MultiSel(bMultiSel As Boolean)  
    m_MultiSel = bMultiSel
    m_FileFlags = OFN_EXPLORER Or OFN_FILEMUSTEXIST Or OFN_HIDEREADONLY Or IIf(bMultiSel,0,OFN_ALLOWMULTISELECT)
End Property 

'-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
'
Function clsCommonDialog.ShowBrowse(ByVal hOwner As HWND = NULL) As LPCTSTR
    If hOwner = NULL Then hOwner = GetForegroundWindow()  '如果没有传入句柄，则获取前台窗口的句柄
    '
    'm_FilePath = AfxBrowseForFolder(hOwner,m_DialogTitle,m_InitDir)
    Return @m_FilePath
End Function

Function clsCommonDialog.CreateFolder(lpszPath As LPCTSTR) As Boolean 
    m_FilePath = *Replace(lpszPath,"\\","\")  '确保没有双斜杠    
    Return MkDir(m_FilePath)
End Function
'-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
'设置字体对话框
Function clsCommonDialog.ShowFont(ByVal hOwner As HWND = NULL) As Boolean
    Dim tChsFnt As ChooseFont, tLogFnt As Logfont
    
    If hOwner = Null Then hOwner = GetForegroundWindow()  '如果没有传入句柄，则获取前台窗口的句柄
    Dim m_hDC As hDC = GetDC(hOwner)  '获取DC
'设置逻辑字体参数（）    
    With tLogFnt
        .lfHeight = -MulDiv(m_FontSize, GetDeviceCaps(m_hDC, LOGPIXELSY), 72)    '字体高度，跟字体字体大小相关
        .lfWidth = 0                                                             '字体宽度
        .lfEscapement = 0                                                        ' angle between baseline and escapement vector
        .lfOrientation = 0                                                       ' angle between baseline and orientation vector
        .lfWeight = IIf(m_FontBold,FW_BOLD,FW_NORMAL)                            '字体粗细，默认FW_NORMAL，加粗为FW_BOLD
        .lfCharSet = DEFAULT_CHARSET                                             '使用默认字符集
        .lfOutPrecision = OUT_DEFAULT_PRECIS                                     ' default precision mapping
        .lfClipPrecision = CLIP_DEFAULT_PRECIS                                   ' default clipping precision
        .lfQuality = DEFAULT_QUALITY                                             ' default quality setting
        .lfPitchAndFamily = DEFAULT_PITCH Or FF_ROMAN                            ' default pitch, proportional with serifs
        .lfFaceName = m_FontName                                                 '字体，默认为Times New Roman（新罗马字体）
        .lfItalic = m_FontItalic                                                 '是否斜体
        .lfUnderline = m_FontUnderline                                           '是否有下划线
        .lfStrikeOut = m_FontStrikethru                                          '是否有删除线
    End With

'设置选择字体对话框信息    
    With tChsFnt
        .lStructSize = Len(tChsFnt)     '结构体大小                             ' size of structure
        .hwndOwner = hOwner             '所有窗口（打开字体对话框的窗口）       
        .hDC = m_hDC                    'DC(device context)从窗口句柄获得
        .lpLogFont = @tLogFnt           '这个是指针，需要地址引用，指向逻辑字体结构
        .iPointSize = m_FontSize * 10   '默认12号（小四）字体
        .Flags = CF_BOTH Or CF_EFFECTS Or CF_FORCEFONTEXIST Or CF_INITTOLOGFONTSTRUCT Or CF_LIMITSIZE '所有功能使用
        .rgbColors = m_ForeColor        '默认黑色
        .nFontType = REGULAR_FONTTYPE   ' regular font type i.e. not bold or anything
        .nSizeMin = DefMinFontSize      '最小字号，默认8
        .nSizeMax = DefMaxFontSize      '最大字号，默认72
    End With

    If ChooseFont(@tChsFnt) Then  '打开选择字体对话框，成功返回非0（真）
        m_FontName = tLogFnt.lfFaceName
        m_FontItalic = Cast(Boolean,tLogFnt.lfItalic)   
        m_FontStrikethru = Cast(Boolean,tLogFnt.lfStrikeOut) 
        m_FontUnderline = Cast(Boolean,tLogFnt.lfUnderline) 
        
        Select Case tLogFnt.lfWeight '字体粗细
            Case FW_DONTCARE  '
            Case FW_THIN  '
            Case FW_EXTRALIGHT '
            Case FW_LIGHT '
            Case FW_NORMAL '正常(普通)
                m_FontBold = False
            Case FW_MEDIUM '
            Case FW_SEMIBOLD '
            Case FW_BOLD    '加粗
                m_FontBold = True
            Case FW_EXTRABOLD '
            Case FW_HEAVY '
            Case Else
        End Select
        m_FontSize = tChsFnt.iPointSize / 10 '
        m_ForeColor = tChsFnt.rgbColors
        
        ShowFont = True
    Else
        ShowFont = False
    End If
    
    ReleaseDC(hOwner,m_hDC)
End Function

'-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
'
Property clsCommonDialog.FontName() As LPCTSTR                  '
    Return @m_FontName  
End PROPERTY

PROPERTY clsCommonDialog.FontName(lpszText As LPCTSTR)
    m_FontName = *lpszText
End PROPERTY
    
PROPERTY clsCommonDialog.FontSize() as Integer                    '
    Return m_FontSize
End PROPERTY

PROPERTY clsCommonDialog.FontSize(nSize as Integer)
    m_FontSize = nSize
End PROPERTY
    
PROPERTY clsCommonDialog.FontItalic() as Boolean               '
    Return m_FontItalic   
End PROPERTY

PROPERTY clsCommonDialog.FontItalic(bItalic as Boolean)
    m_FontItalic = bItalic
End PROPERTY
    
PROPERTY clsCommonDialog.FontBold() as Boolean               '
    Return m_FontBold
End PROPERTY

PROPERTY clsCommonDialog.FontBold(bBold as Boolean)
    m_FontBold = bBold
End PROPERTY
    
PROPERTY clsCommonDialog.FontStrikethru() as Boolean               '
    Return m_FontStrikethru
End PROPERTY

PROPERTY clsCommonDialog.FontStrikethru(bStrikethru as Boolean)
    m_FontStrikethru = bStrikethru
End PROPERTY
    
PROPERTY clsCommonDialog.FontUnderline() as Boolean               '
    Return m_FontUnderline
End PROPERTY

PROPERTY clsCommonDialog.FontUnderline(bUnderline as Boolean)
    m_FontUnderline = bUnderline
End PROPERTY

PROPERTY clsCommonDialog.ForeColor() as COLORREF                 '前景色
    Return m_ForeColor
End PROPERTY

PROPERTY clsCommonDialog.ForeColor(nColor as COLORREF)
    m_ForeColor = nColor
End PROPERTY 







