Type Class_Frame Extends Class_Control
    Declare Constructor
    Declare Destructor
    Declare Property Caption() As String                '返回/设置控件中的文本
    Declare Property Caption(ByVal sText As String)
End Type

'----------------------------------------------------------------------------------------------------------------------------------------------------------------
Constructor Class_Frame
  
End Constructor

Destructor Class_Frame
  
End Destructor
Property Class_Frame.Caption() As String
  Return AfxGetWindowText(hWndControl)
End Property
Property Class_Frame.Caption(ByVal sText As String)
  AfxSetWindowText  hWndControl, sText
End Property

