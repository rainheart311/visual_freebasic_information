Enum LVTYPE
    eIcon, 
    eList,
    eReport,
    eSmallIcon
End Enum

Type Class_ListView Extends Class_Control
Protected : 
   m_ReDraw As Boolean  '修改列表时是否允许刷新
Public : 
   Declare Constructor
   Declare Destructor
   Declare Function AddItem(nCol As Long,sText As string,lParam As LPARAM = 0,nImage As Long = 0) As Long  '新增项(1行)，文本以 Chr(0) 分割包含所有列，返回新的项的索引。
   Declare Sub AddItemCount(nCount As Long)  '予分配项目数量内存，避免每次新增分配，速度过慢。
   Declare Function InsertItem(nRow As Long,nCol As Long,sText As string,lParam As LPARAM = 0,nImage As Long = 0) As Long  '在iRow行插入项(1行)，文本以 Chr(0) 分割包含所有列，返回新的项的索引。
   Declare Function DeleteAllItems() As Long '删除所有项目。
   Declare Function DeleteItem(iRow As Long) As Long  '删除项目(删除第iRow行)。如果成功返回 TRUE 否则为 FALSE
   Declare Function ItemCount() As Long   '项目数(多少行)
   Declare Function GetItemText(iRow As Long, iColumn As Long) As String '返回列表项的文本(索引从0开始)
   Declare Function SetItemText(iRow As Long, iColumn As Long, TheText As String) As Long  '设置列表项的文本(索引从0开始)
   Declare Property ItemData(iRow As Long) As Integer '返回/设置第iR+++++++++++++++++++++++++++++++ow行关联的 值
   Declare Property ItemData(iRow As Long, nlParam As Integer)
   Declare Property ItemImage(iRow As Long) As Long '返回/设置第iRow行图像索引
   Declare Property ItemImage(iRow As Long, iImage As Long)
   Declare Property Checked(iRow As Long) As Boolean '返回/设置第iRow行复选框状态，{=.True.False}
   Declare Property Checked(iRow As Long, nCheckState As Boolean)
   Declare Property SelectedItem() As Long '返回/设置当前选择项（索引从零开始）
   Declare Property SelectedItem(nIndex As Long)
   Declare Sub SelectAllItems()  '选择ListView控件的所有项目。
   Declare Sub UnselectItem(iIndex As Long) '取消选择ListView项目。
   Declare Sub UnselectAllItems()   '取消选择所有ListView项目。
   
   Declare Function AddColumn(sText As String, nAlignment As Long = LVCFMT_LEFT, nWidth As Long = 100) As Long  '新增列（标题）,{2.LVCFMT_CENTER 文本居中.LVCFMT_LEFT 左对齐.LVCFMT_RIGHT 右对齐},成功返回新列的索引，否则为-1
   Declare Function InsertColumn(iPosition As Long, sText As String, nAlignment As Long = LVCFMT_LEFT, nWidth As Long = 100) As Long  '插入一列（标题）,{3.LVCFMT_CENTER 文本居中.LVCFMT_LEFT 左对齐.LVCFMT_RIGHT 右对齐},成功返回新列的索引，否则为-1
   Declare Function DeleteAllColumn() As Long  '删除所有列（标题）。
   Declare Function DeleteColumn(iPosition As Long) As Long '删除1列(索引从0开始)。成功返回 TRUE 否则为 FALSE
   Declare Function ColumnCount() As Long  '返回多少列数。（标题）。
   Declare Property ColumnText(iRow As Long) As String '列标题的文本(索引从0开始)
   Declare Property ColumnText(iRow As Long, sText As String)
   Declare Property ColumnAlignment(iRow As Long) As Long '返回/设置文本对齐方式。(索引从0开始),{=.LVCFMT_CENTER 文本居中.LVCFMT_LEFT 左对齐.LVCFMT_RIGHT 右对齐}
   Declare Property ColumnAlignment(iRow As Long, nAlignment As Long)
   Declare Property ColumnWidth(iRow As Long) As Long '返回/设置列的宽度。(索引从0开始)
   Declare Property ColumnWidth(iRow As Long, nWidth As Long)
   
   Declare Function CountPerPage() As Long '可见区域项的数目（可以显示多少行）。
   Declare Function TopIndex() As Long '列表或报表视图中时最顶层的可见项的索引。
   Declare Function ScrollSelectedItem() As Long '滚动到当前选择的项目
   Declare Function FindString(fs As String) As Long '搜索列表视图项目文本。搜寻不区分大小写。成功时返回该项目的索引，否则返回-1。
   Declare Function FitContent(iCol As Long) As Long '自动调整列表视图控件的指定列的大小。成功返回TRUE，否则返回FALSE。
   Declare Function Scroll(dx As Long, dy As Long) As Long '水平和垂直滚动量（像素）。成功返回TRUE，否则返回FALSE。
   'Declare Sub ColumnToSort(lpNMV As NM_LISTVIEW Ptr) '点列名排序操作，在LVN_COLUMNCLICK事件里调用即可。
   Declare Property ImageList(ImageListType As Long) As Handle '返回/设置关联的图像列表对象，LVSIL_NORMAL 大图标,LVSIL_SMALL 小图标,LVSIL_STATE 状态图像
   Declare Property ImageList(ImageListType As Long, hImageList As HIMAGELIST)
   Declare Property ReDraw() As Boolean         '设置重绘标志，大量新增项目时关闭重绘，避免控件闪耀和提高速度。{=.True.False}
   Declare Property ReDraw(nValue As Boolean)    '修改项目时是否允许刷新，（注意：多开窗口中的控件，多窗口用同1个控件类，返回此值可能不正确）
   Declare Function ColumnsWidthSave(FileIni As String, nName As String) As Boolean   '在INI文件里保存所有列的宽度。成功返回 True (通常在窗口销毁事件里)
   Declare Sub ColumnsWidthLoad(FileIni As String, nName As String)  '从INI文件里加载所有列的宽度，来列宽，需要先新增列。 (通常在窗口加载事件里)
   
    Declare Property View() As LVTYPE
    Declare Property View(nView As LVTYPE)
    Declare Property GridLines()As Boolean
    Declare Property GridLines(ByVal bGridLines As Boolean)
    Declare Property FullRowSelect()As Boolean
    Declare Property FullRowSelect(ByVal bFullRowSelect As Boolean)
	Declare Property BackColor() As COLORREF
    Declare Property BackColor(nColor As COLORREF)
	Declare Property ForeColor() As COLORREF
    Declare Property ForeColor(nColor As COLORREF)
	Declare Property TextBkColor() As COLORREF
    Declare Property TextBkColor(nColor As COLORREF)
End Type

'----------------------------------------------------------------------------------------------------------------------------------------------------------------
Constructor Class_ListView
   m_ReDraw = True
End Constructor

Destructor Class_ListView
  
End Destructor
Function Class_ListView.AddColumn(sText As String, nAlignment As Long = LVCFMT_LEFT, nWidth As Long = 100) As Long
  '新增列（标题）,LVCFMT_CENTER 文本居中,LVCFMT_LEFT 左对齐,LVCFMT_RIGHT 右对齐,成功返回新列的索引，否则为-1
  Dim u As Long = This.ColumnCount
  Function = This.InsertColumn(u, sText, nAlignment, nWidth)
End Function
Function Class_ListView.InsertColumn(iPosition As Long, sText As String, nAlignment As Long = LVCFMT_LEFT, nWidth As Long = 100) As Long
  '插入一列（标题）,LVCFMT_CENTER 文本居中,LVCFMT_LEFT 左对齐,LVCFMT_RIGHT 右对齐,成功返回新列的索引，否则为-1
  Dim tlvc As LV_COLUMN
  '检查窗口句柄是否有效
  If IsWindow(hWndControl) Then
      tlvc.mask = LVCF_FMT Or LVCF_WIDTH Or LVCF_TEXT Or LVCF_SUBITEM
      tlvc.fmt = nAlignment
      tlvc.cx = nWidth
      tlvc.pszText = StrPtr(sText)
      tlvc.iSubItem = 0
      Function = SendMessage(hWndControl, LVM_INSERTCOLUMN, iPosition, Cast(lParam, VarPtr(tlvc)))
  End If
End Function
Function Class_ListView.DeleteAllColumn() As Long  '删除所有列（标题）。
  '检查窗口句柄是否有效
  If IsWindow(hWndControl) Then
      Do Until ListView_DeleteColumn(hWndControl, 0) = 0
      Loop
  End If
  Return 0
End Function
Function Class_ListView.DeleteColumn(iPosition As Long) As Long '删除1列(索引从0开始)。成功返回 TRUE 否则为 FALSE
  '检查窗口句柄是否有效
  If IsWindow(hWndControl) Then
      Function = ListView_DeleteColumn(hWndControl, iPosition)
  End If
End Function
Function Class_ListView.ColumnCount() As Long  '返回多少列数。（标题）。
  Dim hWndLVHdr As.hWnd
  '// get the header control
  hWndLVHdr = Cast(.hWnd, SendMessage(hWndControl, LVM_GETHEADER, 0, 0))
  '// return the number of items in the header (ie. the number of columns).
  Function = SendMessage(hWndLVHdr, HDM_GETITEMCOUNT, 0, 0)
End Function
Property Class_ListView.ColumnText(iRow As Long) As String '列标题的文本(索引从0开始)
  Dim tlvc  As LV_COLUMN
  Dim zText As ZString * MAX_PATH
  '检查窗口句柄是否有效
  If IsWindow(hWndControl) Then
      tlvc.mask = LVCF_TEXT
      tlvc.pszText = VarPtr(zText)
      tlvc.cchTextMax = SizeOf(zText)
      If SendMessage(hWndControl, LVM_GETCOLUMN, iRow, Cast(lParam, VarPtr(tlvc))) Then
          Return zText
      End If
  End If
End Property
Property Class_ListView.ColumnText(iRow As Long, sText As String)
  Dim tlvc As LV_COLUMN
  '检查窗口句柄是否有效
  If IsWindow(hWndControl) Then
      tlvc.mask = LVCF_TEXT
      tlvc.pszText = StrPtr(sText)
      SendMessage(hWndControl, LVM_SETCOLUMN, iRow, Cast(lParam, VarPtr(tlvc)))
  End If
End Property
Property Class_ListView.ColumnAlignment(iRow As Long) As Long
  Dim tlvc As LV_COLUMN
  '检查窗口句柄是否有效
  If IsWindow(hWndControl) Then
      tlvc.mask = LVCF_FMT
      If SendMessage(hWndControl, LVM_GETCOLUMN, iRow, Cast(lParam, VarPtr(tlvc))) Then
          If (tlvc.fmt And LVCFMT_RIGHT) = LVCFMT_RIGHT Then
              Return LVCFMT_RIGHT
          End If
          If (tlvc.fmt And LVCFMT_CENTER) = LVCFMT_CENTER Then
              Return LVCFMT_CENTER
          End If
          If (tlvc.fmt And LVCFMT_LEFT) = LVCFMT_LEFT Then
              Return LVCFMT_LEFT
          End If
      End If
  End If
End Property
Property Class_ListView.ColumnAlignment(iRow As Long, nAlignment As Long)
  Dim tlvc As LV_COLUMN
  '检查窗口句柄是否有效
  If IsWindow(hWndControl) Then
      tlvc.mask = LVCF_FMT
      tlvc.fmt = nAlignment
      SendMessage(hWndControl, LVM_SETCOLUMN, iRow, Cast(lParam, VarPtr(tlvc)))
  End If
End Property
Property Class_ListView.ColumnWidth(iRow As Long) As Long '返回/设置列的宽度。(索引从0开始)
  Return ListView_GetColumnWidth(hWndControl, iRow)
End Property
Property Class_ListView.ColumnWidth(iRow As Long, nWidth As Long)
  ListView_SetColumnWidth(hWndControl, iRow, nWidth)
End Property
Function Class_ListView.AddItem(nCol As Long,sText As string,lParam As LPARAM = 0,nImage As Long = 0) As Long '新增项(1行)，文本以 Chr(0) 分割包含所有列，返回新的项的索引。
  Function = InsertItem(ItemCount,nCol,sText,lParam,nImage)
End Function
Function Class_ListView.InsertItem(nRow As Long,nCol As Long,sText As string,lParam As LPARAM = 0,nImage As Long = 0) As Long  '在iRow行插入项(1行)，文本以 Chr(0) 分割包含所有列，返回新的项的索引。
    Dim tlv_item As LV_ITEM

    If IsWindow(hWndControl) Then   '检查窗口句柄是否有效
        tlv_item.iItem     = nRow
        tlv_item.iSubItem  = nCol
        tlv_item.pszText   = strptr(sText)
        tlv_item.iImage    = nImage
        tlv_item.lParam    = lParam
        
        If nCol = 0 Then
            tlv_item.mask  = LVIF_TEXT Or LVIF_PARAM Or LVIF_IMAGE 
            Function = SendMessage(hWndControl,LVM_INSERTITEM,0,Cast(.LPARAM,VarPtr(tlv_item)))
        Else 
            tlv_item.mask  = LVIF_TEXT Or LVIF_IMAGE
            Function = SendMessage(hWndControl,LVM_SETITEM,0,Cast(.LPARAM,VarPtr(tlv_item)))
        End If
    Else
    	Print "clsListView.InsertItem:"
    End If
  
End Function
Function Class_ListView.DeleteAllItems() As Long '删除所有项目。
  Function = SendMessage(hWndControl, LVM_DELETEALLITEMS, 0, 0)
End Function
Function Class_ListView.DeleteItem(iRow As Long) As Long '删除项目(删除第iRow行)。如果成功返回 TRUE 否则为 FALSE
  Function = ListView_DeleteItem(hWndControl, iRow)
End Function
Function Class_ListView.ItemCount()  As Long '项目数(多少行)
  Function = SendMessage(hWndControl, LVM_GETITEMCOUNT, 0, 0)
End Function
Function Class_ListView.GetItemText(iRow As Long, iColumn As Long) As String '返回/设置列表项的文本(索引从0开始)
  Dim tlv_item As LV_ITEM
  Dim zText    As ZString * MAX_PATH
  '检查窗口句柄是否有效
  If IsWindow(hWndControl) Then
      tlv_item.mask = LVIF_TEXT
      tlv_item.iItem = iRow
      tlv_item.iSubItem = iColumn
      tlv_item.pszText = VarPtr(zText)
      tlv_item.cchTextMax = SizeOf(zText)
      If SendMessage(hWndControl, LVM_GETITEM, 0, Cast(lParam, VarPtr(tlv_item))) Then
          Return zText
      End If
  End If
End Function
Function Class_ListView.SetItemText(iRow As Long, iColumn As Long, TheText As String) As Long
  Dim tlv_item As LV_ITEM
  '检查窗口句柄是否有效
  If IsWindow(hWndControl) Then
      tlv_item.mask = LVIF_TEXT
      tlv_item.iItem = iRow
      tlv_item.iSubItem = iColumn
      tlv_item.pszText = StrPtr(TheText)
      tlv_item.cchTextMax = Len(TheText)
      Return SendMessage(hWndControl, LVM_SETITEM, 0, Cast(lParam, VarPtr(tlv_item)))
  End If
End Function
Property Class_ListView.ItemData(iRow As Long) As Integer  '返回/设置第iRow行关联的 32 位值
  Dim tlv_item As LV_ITEM
  '检查窗口句柄是否有效
  If IsWindow(hWndControl) Then
      tlv_item.mask = LVIF_PARAM
      tlv_item.iItem = iRow
      tlv_item.iSubItem = 0
      If SendMessage(hWndControl, LVM_GETITEM, 0, Cast(lParam, VarPtr(tlv_item))) Then
          Return tlv_item.lParam
      End If
  End If
End Property
Property Class_ListView.ItemData(iRow As Long, lParam As Integer )
  Dim tlv_item As LV_ITEM
  '检查窗口句柄是否有效
  If IsWindow(hWndControl) Then
      tlv_item.mask = LVIF_PARAM
      tlv_item.iItem = iRow
      tlv_item.iSubItem = 0
      tlv_item.lParam = lParam
      SendMessage(hWndControl, LVM_SETITEM, 0, Cast(.lParam, VarPtr(tlv_item)))
  End If
End Property
Property Class_ListView.ItemImage(iRow As Long) As Long '返回/设置第iRow行图像索引
  Dim tlv_item As LV_ITEM
  '检查窗口句柄是否有效
  If IsWindow(hWndControl) Then
      tlv_item.mask = LVIF_IMAGE
      tlv_item.iItem = iRow
      tlv_item.iSubItem = 0
      If SendMessage(hWndControl, LVM_GETITEM, 0, Cast(lParam, VarPtr(tlv_item))) Then
          Return  Cast(Long, tlv_item.iImage)
      End If
  End If
End Property
Property Class_ListView.ItemImage(iRow As Long, iImage As Long)
  Dim tlv_item As LV_ITEM
  '检查窗口句柄是否有效
  If IsWindow(hWndControl) Then
      tlv_item.mask = LVIF_IMAGE
      tlv_item.iItem = iRow
      tlv_item.iSubItem = 0
      tlv_item.iImage = iImage
      SendMessage(hWndControl, LVM_SETITEM, 0, Cast(lParam, VarPtr(tlv_item)))
  End If
End Property
Property Class_ListView.Checked(iRow As Long) As Boolean '返回/设置第iRow行复选框状态，非0为选择
  Return ListView_GetCheckState(hWndControl, iRow)
End Property
Property Class_ListView.Checked(iRow As Long, nCheckState As Boolean)
  If nCheckState <> 0 Then nCheckState = 1
  ListView_SetCheckState(hWndControl, iRow, nCheckState)
End Property
Property Class_ListView.SelectedItem() As Long '返回/设置当前选择项（索引从零开始）
  Return SendMessage(hWndControl, LVM_GETSELECTIONMARK, 0, 0)
End Property
Property Class_ListView.SelectedItem(nIndex As Long)
  '检查窗口句柄是否有效
  If IsWindow(hWndControl) Then
      SendMessage(hWndControl, LVM_SETSELECTIONMARK, 0, nIndex)
      
      'ensure that the item is set visually as well
      Dim tItem As LVITEM
      tItem.mask = LVIF_STATE
      tItem.iItem = nIndex
      tItem.iSubItem = 0
      tItem.State = LVIS_FOCUSED Or LVIS_SELECTED
      tItem.statemask = LVIS_FOCUSED Or LVIS_SELECTED
      SendMessage hWndControl, LVM_SETITEMSTATE, nIndex, Cast(lParam, VarPtr(tItem))
  End If
End Property
Property Class_ListView.ImageList(ImageListType As Long) As Handle
  Return ListView_GetImageList(hWndControl, ImageListType)
End Property
Property Class_ListView.ImageList(ImageListType As Long, nImageList As HIMAGELIST)
  ListView_SetImageList(hWndControl, nImageList, ImageListType)
End Property
Sub Class_ListView.SelectAllItems()  '选择ListView控件的所有项目。
  ListView_SelectAllItems hWndControl
End Sub
Sub Class_ListView.UnselectItem(iIndex As Long)  '取消选择ListView项目。
  ListView_UnselectItem  hWndControl, iIndex
End Sub
Sub Class_ListView.UnselectAllItems()  '取消选择所有ListView项目。
  ListView_UnselectAllItems  hWndControl
End Sub
Function Class_ListView.CountPerPage() As Long '可见区域项的数目（可以显示多少行）。
  Return ListView_GetCountPerPage(hWndControl)
End Function
Function Class_ListView.TopIndex() As Long '列表或报表视图中时最顶层的可见项的索引。
  Return ListView_GetTopIndex(hWndControl)
End Function
Function Class_ListView.ScrollSelectedItem() As Long '滚动到当前选择的项目
  Dim i As Long = SendMessage(hWndControl, LVM_GETSELECTIONMARK, 0, 0)
  Return SendMessage(hWndControl, LVM_ENSUREVISIBLE, Cast(wParam, i), -1)
End Function
Function Class_ListView.FindString(fs As String) As Long '搜索列表视图项目文本。搜寻不区分大小写。成功时返回该项目的索引，否则返回-1。
  Return ListView_FindString(hWndControl, fs)
End Function
Function Class_ListView.FitContent(iCol As Long) As Long '自动调整列表视图控件的指定列的大小。成功返回TRUE，否则返回FALSE。。
  Return ListView_FitContent(hWndControl, iCol)
End Function
Function Class_ListView.Scroll(dx As Long, dy As Long) As Long '水平和垂直滚动量（像素）。成功返回TRUE，否则返回FALSE。
  Return ListView_Scroll(hWndControl, dx, dy)
End Function
Sub Class_ListView.AddItemCount(nCount As Long )  '予分配项目数量内存，避免每次新增分配，速度过慢。
   SendMessage(hWndControl, LVM_SETITEMCOUNT, nCount, LVSICF_NOINVALIDATEALL)
End Sub
Property Class_ListView.ReDraw()  As Boolean         '返回/设置重绘标志，大量新增项目时关闭重绘，避免控件闪耀和提高速度。{=.True.False}
    Property = m_ReDraw  '无需返回
End Property
Property Class_ListView.ReDraw(nValue As Boolean)
 m_ReDraw = nValue
 SendMessage(hWndControl, WM_SETREDRAW, nValue, 0)
End Property
Function Class_ListView.ColumnsWidthSave(FileIni As String, nName As String) As Boolean   '在INI文件里保存所有列的宽度。成功返回 True (通常在窗口销毁事件里)
   Dim nCols As Long = ListView_GetColumnCount(hWndControl)
   Dim wszBuffer As ZString * MAX_PATH
   For idx As Long = 0 To nCols - 1
      wszBuffer += Str(ListView_GetColumnWidth(hWndControl, idx))
      If idx < nCols - 1 Then wszBuffer += ","
   Next
   Return WritePrivateProfileString (StrPtr(nName), "ColumnsWidth", @wszBuffer, StrPtr(FileIni))
End Function
Sub Class_ListView.ColumnsWidthLoad(FileIni As String, nName As String)  '从INI文件里加载所有列的宽度，来列宽，需要先新增列。 (通常在窗口加载事件里)
   Dim nCols As Long = ListView_GetColumnCount(hWndControl)
   Dim wszBuffer As ZString * MAX_PATH
   Dim dwChars As DWord = GetPrivateProfileString(StrPtr(nName), "ColumnsWidth", Null, @wszBuffer, MAX_PATH, StrPtr(FileIni))
   If dwChars Then  
       If Len(wszBuffer) Then
           For idx As Long = 0 To nCols - 1
               Dim s As String = AfxStrParse(wszBuffer, idx + 1, ",")
               Dim nWidth As Long = Val(s)
               ListView_SetColumnWidth(hWndControl, idx, nWidth)
           Next
       End If
   End If
End Sub

Property Class_ListView.View(nView As LVTYPE)
    Select Case nView
    	Case eIcon
    		ListView_SetView(hWndControl,LV_VIEW_ICON)      '——大图标列表。
    	Case eList
    		ListView_SetView(hWndControl,LV_VIEW_LIST)      '——列表视图。
    	Case eReport
    		ListView_SetView(hWndControl,LV_VIEW_DETAILS)   '——详细视图。
    	Case eSmallIcon
    		ListView_SetView(hWndControl,LV_VIEW_SMALLICON) '——小图标。
    	Case Else
    		ListView_SetView(hWndControl,LV_VIEW_TILE)      '——平铺，如果我没记错的话，这个视图是在XP时引入的。
    End Select
End Property

Property Class_ListView.GridLines() As Boolean
    Dim As Integer lvStyle = SendMessage(hWndControl,LVM_GETEXTENDEDLISTVIEWSTYLE, 0, 0) 
    
    If (lvStyle And LVS_EX_GRIDLINES) = LVS_EX_GRIDLINES Then 
        Return True
    Else                                                      
        Return False
    End If
End Property

Property Class_ListView.GridLines(ByVal bGridLines As Boolean)
    Dim As Integer lvStyle = SendMessage(hWndControl,LVM_GETEXTENDEDLISTVIEWSTYLE, 0, 0)
    
    If bGridLines Then
        lvStyle Or= LVS_EX_GRIDLINES
    Else
        lvStyle And= (Not LVS_EX_GRIDLINES)
    End If
    SendMessage(hWndControl,LVM_SETEXTENDEDLISTVIEWSTYLE,0,ByVal lvStyle)    
End Property

Property Class_ListView.FullRowSelect()As Boolean
    Dim As Integer lvStyle = SendMessage(hWndControl,LVM_GETEXTENDEDLISTVIEWSTYLE, 0, 0)
    
    If lvStyle And LVS_EX_FULLROWSELECT Then 
         Return True
    Else                                     
         Return False
    End If
End Property

Property Class_ListView.FullRowSelect(ByVal bFullRowSelect As Boolean)
    Dim As Integer lvStyle = SendMessage(hWndControl,LVM_GETEXTENDEDLISTVIEWSTYLE, 0, 0)
    
    If bFullRowSelect Then
        lvStyle Or= LVS_EX_FULLROWSELECT
    Else
        lvStyle And= (Not LVS_EX_FULLROWSELECT)
    End If
    SendMessage(hWndControl,LVM_SETEXTENDEDLISTVIEWSTYLE,0,ByVal lvStyle)  
End Property

Property Class_ListView.BackColor() As COLORREF
    return SendMessage(hWndControl,LVM_GETBKCOLOR,0,0)
End Property

Property Class_ListView.BackColor(nColor As COLORREF)
    SendMessage(hWndControl,LVM_SETBKCOLOR,0,nColor)
End Property

Property Class_ListView.ForeColor() As COLORREF
    return SendMessage(hWndControl,LVM_GETTEXTCOLOR,0,0)
End Property

Property Class_ListView.ForeColor(nColor As COLORREF)
    SendMessage(hWndControl,LVM_SETTEXTCOLOR,0,nColor)
End Property

Property Class_ListView.TextBkColor() As COLORREF
    return SendMessage(hWndControl,LVM_GETTEXTBKCOLOR,0,0)
End Property

Property Class_ListView.TextBkColor(nColor As COLORREF)
    SendMessage(hWndControl,LVM_SETTEXTBKCOLOR,0,nColor)
End Property

