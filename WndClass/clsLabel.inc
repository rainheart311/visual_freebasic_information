Type Class_Label Extends Class_Control
    Declare Constructor
    Declare Destructor
    Declare Property Caption() As String                '返回/设置控件中的文本
    Declare Property Caption(ByVal sText As String)
End Type

'----------------------------------------------------------------------------------------------------------------------------------------------------------------
Constructor Class_Label
  
End Constructor

Destructor Class_Label
  
End Destructor
Property Class_Label.Caption() As String
  Return AfxGetWindowText(hWndControl)
End Property
Property Class_Label.Caption(ByVal sText As String)
  AfxSetWindowText  hWndControl, sText
End Property

