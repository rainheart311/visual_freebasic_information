Type Class_TabControl Extends Class_Control
    Declare Constructor
    Declare Destructor
	
    Declare Function AddTab (TabText As String,lParam As lParam = 0) As Boolean    '添加或在选项卡，如果成功返回 TRUE 否则 FALSE 。
    Declare Function InsertTab (Index As Long ,TabText As String,lParam As lParam = 0) As Boolean    '添加或在选项卡，如果成功返回 TRUE 否则 FALSE 。
	Declare Function GetCount () As Long                '返回选项卡数，如果不成功则为返回零。
    Declare Function RemoveAll () As Boolean               '删除所有选项卡，如果成功返回 TRUE 否则返回 FALSE。
    Declare Function Remove (Index As Long ) As Boolean    '删除选项卡，如果成功返回 TRUE 否则 FALSE 。
    Declare Sub SetBottom()                               '设置选项卡在底部显示
	
    Declare Property Text(Index As Long) As String                 '返回/设置标签名称（索引从0开始）
    Declare Property Text(Index As Long , TabText As String)
    Declare Property Focus() As Long                 '返回/设置具有焦点项的索引。具有焦点的项可能不同于所选项。
    Declare Property Focus(Index As Long )
    Declare Property Selected() As Long                 '返回/设置当前选定的选项卡，如果成功，返回其索引的所选的选项卡，如果没有选择则选项卡返回-1。
    Declare Property Selected(Index As Long )
	Declare Property ScaleWidth() As Long                '返回/设置对象内部的W（像素）
    Declare Property ScaleHeight() As Long               '返回/设置对象内部的H（像素）
End Type

'----------------------------------------------------------------------------------------------------------------------------------------------------------------
Constructor Class_TabControl
  
End Constructor

Destructor Class_TabControl
  
End Destructor

Function Class_TabControl.AddTab(TabText As String, nlParam As lParam = 0) As Boolean    '添加或在选项卡，如果成功返回 TRUE 否则 FALSE 。
  Function=TabCtrl_AddTab ( hWndControl,-1,TabText,nlParam)
End Function
Function Class_TabControl.InsertTab (Index As Long ,TabText As String,lParam As lParam = 0) As Boolean    '添加或在选项卡，如果成功返回 TRUE 否则 FALSE 。
  Function=TabCtrl_InsertTab (hWndControl,Index,-1,TabText,lParam)
End Function
Property Class_TabControl.Text(Index As Long) As String                 '返回/设置标签名称（索引从0开始）
   Dim TB As TC_ITEM
   Dim zT As ZString Ptr 
   Dim TabName As ZString * MAX_PATH
   Dim lResult As Long
      
   TB.Mask       = TCIF_TEXT       '  Set the item mask to return the Tab Name
   TB.cchTextMax = SizeOf(TabName)  '  Size the name buffer
   TB.pszText    = VarPtr(TabName)  '  Assign the buffer's address
   
   '  Send the message to the Tab Control
   lResult = SendMessage( hWndControl, _          '  Handle To destination control
                          TCM_GETITEM, _         '  Message ID
                          Index, _        '  WPARAM = Index of Tab Item
                          Cast(lParam, VarPtr(TB) ))            '  Pointer to Tab Text Value    
   
   Return TabName
End Property
Property Class_TabControl.Text(Index As Long , TabText As String)
   Dim TB As TC_ITEM
   
   TB.Mask    = TCIF_TEXT        '  Set the item mask to return the Tab Name      
   TB.pszText = StrPtr(TabText)  '  Assign the buffer's address
   
   '  Send the message to the Tab Control
    SendMessage( hWndControl, _          '  Handle To destination control
                           TCM_SETITEM, _         '  Message ID
                           Index, _        '  WPARAM = Index of Tab Item
                           Cast(lParam, VarPtr(TB)) )            '  Pointer to Tab Text Value 
End Property
Property Class_TabControl.Focus() As Long                 '返回/设置具有焦点项的索引。具有焦点的项可能不同于所选项。
  Return  SendMessage( hWndControl, TCM_GETCURFOCUS,0,0)
End Property
Property Class_TabControl.Focus(Index As Long )
  SendMessage( hWndControl, TCM_SETCURFOCUS,Index,0)
End Property
Property Class_TabControl.Selected() As Long                 '返回/设置当前选定的选项卡，如果成功，返回其索引的所选的选项卡，如果没有选择则选项卡返回-1。
  Return  SendMessage( hWndControl, TCM_GETCURSEL,0,0)
End Property
Property Class_TabControl.Selected(Index As Long )
  SendMessage( hWndControl, TCM_SETCURSEL,Index,0)
End Property
Function Class_TabControl.GetCount () As Long                '返回选项卡数，如果不成功则为返回零。
  Return  SendMessage( hWndControl, TCM_GETROWCOUNT,0,0)
End Function
Function Class_TabControl.RemoveAll () As Boolean               '删除所有选项卡，如果成功返回 TRUE 否则返回 FALSE。
  Return  SendMessage( hWndControl, TCM_DELETEALLITEMS,0,0)
End Function
Function Class_TabControl.Remove (Index As Long ) As Boolean    '删除选项卡，如果成功返回 TRUE 否则 FALSE 。
  Return  SendMessage( hWndControl, TCM_DELETEITEM,Index,0)
End Function

Property Class_TabControl.ScaleWidth() As Long               
    Dim rc As Rect
    GetClientRect(hWndControl,@rc)     '获得客户区大小    
    Return rc.Right - rc.Left 
End Property

Property Class_TabControl.ScaleHeight() As Long               
    Dim rc As Rect
    GetClientRect(hWndControl,@rc)     '获得客户区大小
    Return rc.Bottom - rc.Top
End Property

Sub Class_TabControl.SetBottom()
    AfxAddWindowStyle hWndControl,TCS_BOTTOM 
End Sub

