Type Class_CommandButton Extends Class_Control
    Declare Constructor
    Declare Destructor
    Declare Property Caption() As String                '返回/设置控件中的文本
    Declare Property Caption(ByVal sText As String)
    Declare Sub Click() '模拟用户点击了这个按钮
End Type

'----------------------------------------------------------------------------------------------------------------------------------------------------------------
Constructor Class_CommandButton
  
End Constructor

Destructor Class_CommandButton
  
End Destructor

Property Class_CommandButton.Caption() As String
    Return AfxGetWindowText(hWndControl)
End Property

Property Class_CommandButton.Caption(ByVal sText As String)
    AfxSetWindowText hWndControl, sText
End Property

Sub Class_CommandButton.Click() '模拟用户点击了这个按钮
    Button_Click hWndControl
End Sub
