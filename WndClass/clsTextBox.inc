Type Class_TextBox Extends Class_Control
    Declare Constructor
    Declare Destructor
	
	Declare Function GetLineCount() As Long '检索一个多行文本框中的行数。如果没有文本存在，则返回值为 1。
    Declare Function GetLine(nLine As Long) As String '获取TextBox 控件某行的文本 。行的索引从零开始。
	Declare Function GetLineNum() As Long
    Declare Function GetPageLines() As long
	
	Declare Sub SelClear() '清除（删除）当前所选内容。
    Declare Sub SelCopy() ' 复制当前所选内容，并放置在剪贴板。
    Declare Sub SelCut() ' 剪切当前所选内容，并放置在剪贴板。
    Declare Sub Paste() ' 粘贴剪贴板中的到当前位置，有选择将替换选择。
    Declare Sub ReplaceText(TheText As String) ' 当前所选内容替换成指定的文本。
    Declare Sub Scroll(nScrollFlag As Long) ' 滚动多行文本框中的文本。,{1.SB_LINEDOWN 滚动下移一行。.SB_LINEUP 滚动一行。.SB_PAGEDOWN 滚动下移一页。.SB_PAGEUP 滚动一页。}
	
    Declare Property Text() As String                '返回/设置控件中的文本
    Declare Property Text(ByVal sText As String)
	Declare Property Text(ByVal nText As Integer)
	Declare Property Text(ByVal nText As Double)
    Declare Property TextW() As CWStr                '返回/设置（宽字符版）控件中的文本
    Declare Property TextW(sText As CWStr)    
    Declare Property TextLimit() As Long                 '返回/设置当前的文本长度限制，以字符为单位。
    Declare Property TextLimit(MaxCharacters As Long)
    Declare Property PasswordChar() As String                 '返回/设置编辑控件的密码字符。当设置密码字符时，将显示该字符而不是用户键入的字符。
    Declare Property PasswordChar(nPassword As String)
    Declare Property Locked() As Boolean                      '锁定
    Declare Property Locked(ByVal bTxt As Boolean)   
    Declare Property MarginLeft() As Long '返回/设置 左侧的边距
    Declare Property MarginLeft(nLeftMargin As Long)
    Declare Property MarginRight() As Long '返回/设置 右侧的边距
    Declare Property MarginRight(nRightMargin As Long)
    Declare Property Modify() As Boolean '返回/设置文本框已被修改标志。{=.True.False}
    Declare Property Modify(nFlagValue As Boolean)
    Declare Property SelStart() As Long '返回/设置当前所选内容在文本框中的起始字符位置。1个中文算1个字符
    Declare Property SelStart(Value As Long)
    Declare Property SelEnd() As Long '返回/设置文本框中当前所选内容的结束字符位置。1个中文算1个字符
    Declare Property SelEnd(Value As Long)
	Declare Property SelLength() As Integer                   '
    Declare Property SelLength(ByVal iTxt As Integer)     
    Declare Function SetSel(nStartPos As Long, nEndPos As Long) As Long ' 设置选择某个范围的字符(当前所选字符)。1个中文算1个字符
    Declare Function TextLength() As Long '返回文本的长度
	Declare Property BackColor() As COLORREF
    Declare Property BackColor(nColor As COLORREF)
	Declare Property ForeColor() As COLORREF
    Declare Property ForeColor(nColor As COLORREF)
End Type

'----------------------------------------------------------------------------------------------------------------------------------------------------------------
Constructor Class_TextBox
  
End Constructor

Destructor Class_TextBox
  
End Destructor

Function Class_TextBox.GetLineNum() As Long
	Dim n As Long = SendMessage(hWndControl,EM_GETSEL,0,0) / 2 ^ 16
	Return SendMessage(hWndControl, EM_LINEFROMCHAR, n, 0)
End Function

Function Class_TextBox.GetPageLines() As Long
	Dim hDC As hDC = GetDC(hWndControl)
	Dim s As SIZE
    GetTextExtentPoint32 hDC,StrPtr("我"),Len("我"),@s
    Function = Height \ s.cy
	ReleaseDC(hWndControl,hDC)
End Function

Property Class_TextBox.Text() As String
  Return AfxGetWindowText(hWndControl)
End Property

Property Class_TextBox.Text(ByVal sText As String)
  AfxSetWindowText  hWndControl, sText
End Property

Property Class_TextBox.Text(ByVal nText As Integer)
  AfxSetWindowText  hWndControl, str(nText)
End Property

Property Class_TextBox.Text(ByVal nText As Double)
  AfxSetWindowText  hWndControl, str(nText)
End Property

Property Class_TextBox.TextW() As CWStr
  Return AfxGetWindowText(hWndControl)
End Property
Property Class_TextBox.TextW(sText As CWStr)
  AfxSetWindowText  hWndControl, sText
End Property
Function Class_TextBox.GetLineCount() As Long '检索一个多行文本框中的行数。如果没有文本存在，则返回值为 1。
  Function = SendMessage(hWndControl, EM_GETLINECOUNT, 0, 0)
End Function
Function Class_TextBox.GetLine(nLine As Long) As String '获取TextBox 控件某行的文本 。行的索引从零开始。
  Dim nBufferSize As Long
  Dim nBuffer     As String
  
  '检查窗口句柄是否有效
  If IsWindow(hWndControl) Then
      
      '得到的文本的长度
      nBufferSize = SendMessage(hWndControl, EM_LINELENGTH, nLine, 0)
      If nBufferSize = 0 Then
          Function = "" : Exit Function
      End If
      
      'Add an extra character for the Nul terminator
      nBufferSize = nBufferSize + 1
      
      'Create the temporary buffer
      nBuffer = Space(nBufferSize)
      
      'Retrieve the text
      SendMessage hWndControl, EM_GETLINE, nLine, Cast(lParam, StrPtr(nBuffer))
      
      'Remove the Null
      nBuffer = RTrim(nBuffer, Chr(0))
      
      Function = nBuffer
  End If
End Function
Property Class_TextBox.TextLimit() As Long                 '返回/设置当前的文本长度限制，以字符为单位。
  Return SendMessage(hWndControl, EM_GETLIMITTEXT, 0, 0)
End Property
Property Class_TextBox.TextLimit(MaxCharacters As Long)
  SendMessage hWndControl, EM_SETLIMITTEXT, MaxCharacters, 0
End Property
Property Class_TextBox.PasswordChar() As String                 '设置或删除编辑控件的密码字符。
  Return Chr(Edit_GetPasswordChar(hWndControl))
End Property
Property Class_TextBox.PasswordChar(nPassword As String)
  If IsWindow(hWndControl) Then
      Dim aa As Long
      If Len(nPassword) = 0 Then aa = 0 Else aa = Asc(nPassword)
      SendMessage(hWndControl, EM_SETPASSWORDCHAR, aa, 0)
  End If
End Property
Property Class_TextBox.Locked() As Boolean     
    Return Cast(Boolean,(GetWindowLong(hWndControl,GWL_STYLE) And ES_READONLY) = ES_READONLY)
End Property

Property Class_TextBox.Locked(ByVal nLocked As Boolean)  
    If IsWindow(hWndControl) Then
        SendMessage hWndControl,EM_SETREADONLY,nLocked,0
    End If
End Property

Sub Class_TextBox.SelClear() '清除（删除）当前所选内容。
  SendMessage hWndControl, WM_CLEAR, 0, 0
End Sub
Sub Class_TextBox.SelCopy() ' 复制当前所选内容，并放置在剪贴板。
  SendMessage hWndControl, WM_COPY, 0, 0
End Sub
Sub Class_TextBox.SelCut() ' 剪切当前所选内容，并放置在剪贴板。
  SendMessage hWndControl, WM_CUT, 0, 0
End Sub
Sub Class_TextBox.Paste() ' 粘贴剪贴板中的到当前位置，有选择将替换选择。
  SendMessage hWndControl, WM_PASTE, 0, 0
End Sub
Sub Class_TextBox.ReplaceText(TheText As String) ' 当前所选内容替换成指定的文本。
  SendMessage hWndControl, EM_REPLACESEL, True, Cast(lParam, StrPtr(TheText))
End Sub
Sub Class_TextBox.Scroll(nScrollFlag As Long) ' 滚动多行文本框中的文本。
  SendMessage hWndControl, EM_SCROLL, nScrollFlag, 0
End Sub
Property Class_TextBox.Modify() As Boolean '返回/设置文本框已被修改标志。{=.True.False}
  Property = SendMessage(hWndControl, EM_GETMODIFY, 0, 0)
End Property
Property Class_TextBox.Modify(nFlagValue As Boolean)
    SendMessage(hWndControl, EM_SETMODIFY, nFlagValue, 0)
End Property
Property Class_TextBox.MarginLeft() As Long '返回/设置 左侧的边距
    Property = LoWord(SendMessage(hWndControl, EM_GETMARGINS, 0, 0))
End Property
Property Class_TextBox.MarginLeft(nLeftMargin As Long)
  SendMessage hWndControl, EM_SETMARGINS, _
      EC_LEFTMARGIN Or EC_RIGHTMARGIN, _
      MAKELONG(nLeftMargin, This.MarginRight)
End Property
Property Class_TextBox.MarginRight() As Long '返回/设置 右侧的边距
  Property = HiWord(SendMessage(hWndControl, EM_GETMARGINS, 0, 0))
End Property
Property Class_TextBox.MarginRight(nRightMargin As Long)
    SendMessage hWndControl, EM_SETMARGINS, _
    EC_LEFTMARGIN Or EC_RIGHTMARGIN, _
    MAKELONG(This.MarginLeft, nRightMargin)
End Property
Property Class_TextBox.SelStart() As Long '检索当前所选内容在文本框中的起始字符位置。
    Dim nStartPos As Long, nEndPos As Long
    SendMessage(hWndControl, EM_GETSEL, Cast(wParam,@nStartPos),Cast(lParam,@nEndPos))
    Property = nStartPos
End Property
Property Class_TextBox.SelStart(Value As Long)
  This.SetSel Value, Value
End Property
Property Class_TextBox.SelEnd() As Long '检索文本框中当前所选内容的结束字符位置。
    Dim nStartPos As Long, nEndPos As Long
    SendMessage(hWndControl, EM_GETSEL, Cast(wParam,@nStartPos),Cast(lParam,@nEndPos))
  Property = nEndPos
End Property
Property Class_TextBox.SelLength() As Integer  
    Return HiWord(SendMessage(hWndControl, EM_GETSEL, 0, 0)) - LoWord(SendMessage(hWndControl, EM_GETSEL, 0, 0))
End Property

Property Class_TextBox.SelLength(ByVal nSelLength As Integer) 
    Dim nTmp As Integer = LoWord(SendMessage(hWndControl, EM_GETSEL, 0, 0))
    SendMessage hWndControl, EM_SETSEL, nTmp, nTmp + nSelLength
End Property
Property Class_TextBox.SelEnd(Value As Long)
    This.SetSel This.SelStart, Value
End Property
Function Class_TextBox.SetSel(nStartPos As Long, nEndPos As Long) As Long ' 设置选择某个范围的字符。
    Function = SendMessage( hWndControl, EM_SETSEL, nStartPos, nEndPos)
End Function
Function Class_TextBox.TextLength() As Long '返回文本的长度
    Function = GetWindowTextLength(hWndControl)
End Function

Property Class_TextBox.BackColor() As COLORREF
    Dim ff As FLY_DATA Ptr 
    Dim hBrush As HBRUSH
    Dim LOG_BRUSH As LOGBRUSH

    If IsWindow(hWndControl) Then  '检查窗口句柄是否有效
       ff = GetProp(hWndControl, "FLY_PTR")
       If ff Then         
          If ff->ProcessCtlColor Then      
             If ff->IsBackSysColor Then
                BackColor = GetSysColor( ff->nBackColor )
             Else
                BackColor = ff->nBackColor
             End If   

          Else
             BackColor = GetSysColor( ff->nBackColor )
          End If  
       End If
    End If
End Property

Property Class_TextBox.BackColor(nColor As COLORREF)
    Dim ff_control  As FLY_DATA Ptr 

    If IsWindow(hWndControl) Then  '检查窗口句柄是否有效
        ff_control = GetProp(hWndControl, "FLY_PTR")
        If ff_control Then
			If nColor <> -1 Then
				If ff_control->hBackBrush Then 
					DeleteObject ff_control->hBackBrush
				End If   
				ff_control->hBackBrush = CreateSolidBrush(nColor)
				ff_control->nBackColor = nColor
				ff_control->IsBackSysColor = False
				 
			    InvalidateRect hWndControl, ByVal 0, True
                UpdateWindow hWndControl 
			End If
        End If
    End If
End Property

Property Class_TextBox.ForeColor() As COLORREF
    Dim ff As FLY_DATA Ptr 
    Dim hBrush As HBRUSH
    Dim LOG_BRUSH As LOGBRUSH

    If IsWindow(hWndControl) Then  '检查窗口句柄是否有效
        ff = GetProp(hWndControl, "FLY_PTR")
        If ff Then         
            If ff->ProcessCtlColor Then      
                If ff->IsForeSysColor Then
                    ForeColor = GetSysColor( ff->nForeColor )
                Else
                    ForeColor = ff->nForeColor
                End If   
            Else
                ForeColor = GetSysColor( ff->nForeColor )
            End If  
        End If
    End If
End Property

Property Class_TextBox.ForeColor(nColor As COLORREF)
    Dim ff_control  As FLY_DATA Ptr 
    If IsWindow(hWndControl) Then  '检查窗口句柄是否有效
        ff_control = GetProp(hWndControl, "FLY_PTR")
        If ff_control Then
			If nColor <> -1 Then
				ff_control->nForeColor = nColor
				ff_control->IsForeSysColor = False
				InvalidateRect hWndControl, ByVal 0, True
                UpdateWindow hWndControl 
			End If   
        End If
    End If
End Property


          
          