Type Class_ImageButton Extends Class_Control
    Declare Constructor
    Declare Destructor
    Declare Sub Click() '模拟用户点击了这个按钮
End Type

'----------------------------------------------------------------------------------------------------------------------------------------------------------------
Constructor Class_ImageButton
  
End Constructor

Destructor Class_ImageButton
  
End Destructor

Sub Class_ImageButton.Click() '模拟用户点击了这个按钮
  Button_Click hWndControl
End Sub
