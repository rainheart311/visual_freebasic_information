Type Class_ProgressBar Extends Class_Control
    Declare Constructor
    Declare Destructor
    Declare Property Position() As Long                 '返回/设置进度栏的当前位置。
    Declare Property Position(ByVal lNewPos As Long)
    Declare Property MaxRange() As Long                 '返回/设置最大值。
    Declare Property MaxRange(ByVal lMaxRange As Long)
    Declare Property MinRange() As Long                 '返回/设置最小值。
    Declare Property MinRange(ByVal lMinRange As Long)
    Declare Function SetStep(ByVal lStepInc As Long)  As Long   '步增量。默认增量为 10。当此命令成功，它将返回以前的步长增量。
    Declare Function SetMarqueeOn(dwAnimationSpeed As DWord)  As Long   '打开进度栏字幕模式。这会导致进度栏像一个选取框一样移动。dwAnimationSpeed 选取框动画更新的时间（以毫秒为单位）
    Declare Function SetMarqueeOff()  As Boolean   '关闭进度栏字幕模式。返回是否设置选框模式。
    Declare Sub Move(ByVal nLeft As Long, ByVal nTop As Long, ByVal nWidth As Long = 0, ByVal nHeight As Long = 0)
End Type

'----------------------------------------------------------------------------------------------------------------------------------------------------------------
Constructor Class_ProgressBar
  
End Constructor

Destructor Class_ProgressBar
  
End Destructor
Property Class_ProgressBar.Position() As Long                 '返回/设置进度栏的当前位置。
  Return  SendMessage( hWndControl,PBM_GETPOS,0,0)
End Property
Property Class_ProgressBar.Position(ByVal lNewPos As Long)
  SendMessage(hWndControl, PBM_SETPOS, lNewPos, 0)
End Property
Property Class_ProgressBar.MaxRange() As Long                 '返回/设置最大值。
  Dim PB As PBRANGE 
  Dim As Long lMinRange,lMaxRange
  SendMessage(hWndControl, PBM_GETRANGE, False, Cast(lParam, VarPtr(PB)))
  lMinRange = PB.iLow
  lMaxRange = PB.iHigh
  Return lMaxRange
End Property
Property Class_ProgressBar.MaxRange(ByVal lMaxRange As Long)
  Dim PB As PBRANGE
  Dim As Long lMinRange
  SendMessage(hWndControl, PBM_GETRANGE, False, Cast(lParam, VarPtr(PB)))
  lMinRange = PB.iLow
'  lMaxRange = PB.iHigh
  SendMessage(hWndControl, PBM_SETRANGE32, lMinRange, lMaxRange)
End Property
Property Class_ProgressBar.MinRange() As Long                 '返回/设置最小值。
  Dim PB As PBRANGE 
  Dim As Long lMinRange,lMaxRange
  SendMessage(hWndControl, PBM_GETRANGE, False, Cast(lParam, VarPtr(PB)))
  lMinRange = PB.iLow
  lMaxRange = PB.iHigh
  Return lMinRange
End Property
Property Class_ProgressBar.MinRange(ByVal lMinRange As Long)
  Dim PB As PBRANGE
  Dim As Long lMaxRange
  SendMessage(hWndControl, PBM_GETRANGE, False, Cast(lParam, VarPtr(PB)))
'  lMinRange = PB.iLow
  lMaxRange = PB.iHigh
  SendMessage(hWndControl, PBM_SETRANGE32, lMinRange, lMaxRange)
End Property
Function Class_ProgressBar.SetStep(ByVal lStepInc As Long) As Long   '步增量。默认增量为 10。当此命令成功，它将返回以前的步长增量。
  Function = SendMessage(hWndControl, PBM_SETSTEP, lStepInc, 0)
End Function
Function Class_ProgressBar.SetMarqueeOn(dwAnimationSpeed As DWord) As Long   '打开进度栏字幕模式。dwAnimationSpeed 选取框动画更新的时间（以毫秒为单位）
  Function = ProgressBar_SetMarquee (hWndControl,True ,dwAnimationSpeed)
End Function
Function Class_ProgressBar.SetMarqueeOff() As Boolean   '关闭进度栏字幕模式。
  Function = ProgressBar_SetMarqueeOff (hWndControl)
End Function

