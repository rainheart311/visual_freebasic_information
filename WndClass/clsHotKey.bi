'--------------------------------------------------------------------------------
'2018-06-01
'完成热键类的创建
'可以设置热键并在结束时自动销毁热键

#define HK_CTRL             MOD_CONTROL
#define HK_ALT              MOD_ALT
#define HK_SHIFT            MOD_SHIFT
#define HK_CTRL_ALT        (HK_CTRL Or HK_ALT)
#define HK_CTRL_SHIFT      (HK_CTRL Or MOD_SHIFT)
#define HK_CTRL_ALT_SHIFT  (HK_CTRL Or HK_ALT Or MOD_SHIFT)
#define HK_ALT_SHIFT       (HK_ALT Or MOD_SHIFT)

Type clsHotKey
Private:
    As HWnd m_hWindow
    Static m_HotKeyID(Any) As Long
    Static m_HotKeyCount As Long
Public:
    Declare Constructor
    Declare Destructor
    Declare Function SetHotkey(ByVal Modifiers As UINT,vKey As String,ByVal hParent As HWnd = Null) As Boolean
    Declare Function DelHotKey() As Boolean
    
    Declare Property hHotKey() As HWND
    Declare Property hHotKey(ByVal nVal As HWND) 
    Declare Property HotKeyID(ByVal Index As Long) As Long
End Type

Dim clsHotKey.m_HotKeyID(Any) As Long
Dim clsHotKey.m_HotKeyCount As Long

Constructor clsHotKey
    m_hWindow = Null
End Constructor

Destructor clsHotKey
    DelHotKey()
End Destructor

Function clsHotKey.SetHotkey(ByVal Modifiers As UINT,vKey As String,ByVal hParent As HWnd = NULL) As Boolean  
    If hParent <> Null Then m_hWindow = hParent
    If m_hWindow = Null Then m_hWindow = GetActiveWindow
    
    Dim a As UINT = Asc(UCase(Left(Trim(vKey),1)))
    
    ReDim Preserve m_HotKeyID(m_HotKeyCount) As Long
	m_HotKeyID(m_HotKeyCount) = 3000 + m_HotKeyCount

    'Print m_HotKeyID(m_HotKeyCount) 
    Function = RegisterHotKey(m_hWindow,m_HotKeyID(m_HotKeyCount),Modifiers,a)
    m_HotKeyCount += 1
End Function

Function clsHotKey.DelHotKey() As Boolean
    If m_HotKeyCount = 0 Then Exit Function
    
    For i As Integer = 0 To m_HotKeyCount -1 
        UnregisterHotKey(m_hWindow,m_HotKeyID(i))
    Next i
    Return TRUE
End Function

Property clsHotKey.hHotKey() As HWnd
    Return m_hWindow
End Property

Property clsHotKey.hHotKey(ByVal nVal As HWnd) 
    If nVal <> Null Then
        m_hWindow = nVal
    End If
End Property

Property clsHotKey.HotKeyID(ByVal Index As Long) As Long
    If m_HotKeyCount = 0 Then Exit Property
    If Index >= m_HotKeyCount Then Exit Property
    Return m_HotKeyID(Index)    
End Property


