'[FILE:G:\FreeBASIC\pid\modules\Module2.Inc
'-----------------------------------------------------------------------------
' 由 VisualFreeBasic 5.9.2 生成的源代码
' 生成时间：2024年09月20日 16时26分21秒
' 更多信息请访问 www.yfvb.com 
'-----------------------------------------------------------------------------




' Type clsDrawChart '已经搬到定义文件处
' Private:     '已经搬到定义文件处
'     m_hWnd As hWnd  '已经搬到定义文件处
'     m_prvx As Long = 0 '已经搬到定义文件处
'     m_prvy As Long = 0  '已经搬到定义文件处
'     m_x As Long = 0 '已经搬到定义文件处
'     m_y As Long = 0 '已经搬到定义文件处
' Private:     '已经搬到定义文件处
'     Declare Static Function DrawChartSubclass(ByVal hWin As hWnd, ByVal uMsg As UINT, ByVal wParam As wParam, ByVal lParam As lParam, ByVal uIdSubclass As UINT_PTR, ByVal dwRefData As DWORD_PTR) As LResult '已经搬到定义文件处
'     '已经搬到定义文件处
' 'Public: '已经搬到定义文件处
' '    Declare Constructor '已经搬到定义文件处
' '    Declare Destructor '已经搬到定义文件处
' Public:      '已经搬到定义文件处
'     Declare Sub BindWindow(ByVal hWin As hWnd) '已经搬到定义文件处
'     Declare Sub NewPoint() '已经搬到定义文件处
'     Declare Sub SetPoint(ByVal x As Long,ByVal y As Long)     '已经搬到定义文件处
' End Type '已经搬到定义文件处

Sub clsDrawChart.BindWindow(ByVal hWin As hWnd)
    m_hWnd = hWin
    SetWindowSubclass(hWin, @DrawChartSubclass, Cast(UINT_PTR, hWin), NULL) 
    SetWindowLong(hWin,GWLP_USERDATA,Cast(LONG_PTR,@This))
End Sub

Function clsDrawChart.DrawChartSubclass(ByVal hWin As HWND, ByVal uMsg As UINT, ByVal wParam As WPARAM, ByVal lParam As LPARAM, ByVal uIdSubclass As UINT_PTR, ByVal dwRefData As DWORD_PTR) As LRESULT
    Dim pCtrl As clsDrawChart Ptr = GetWindowLongPtr(hWin,GWLP_USERDATA)
    Select Case uMsg
    Case WM_PAINT
        Dim ps As PAINTSTRUCT
        Dim dc As HDC = BeginPaint(hWin, @ps)
         
        DrawLine(dc,pCtrl->m_prvx,ps.rcPaint.bottom - pCtrl->m_prvy,pCtrl->m_x,ps.rcPaint.bottom - pCtrl->m_y,PS_SOLID,1,0)

        EndPaint(hWin,@ps)         
    'Case WM_SIZE       
        
    Case WM_DESTROY
        RemoveWindowSubclass(hWin,@DrawChartSubclass,uIdSubclass)  
    End Select
    Function = DefSubclassProc(hWin,uMsg,wParam,lParam)
End Function

Sub clsDrawChart.NewPoint()
    m_prvx = 0
    m_prvy = 0
    m_x = 0
    m_y = 0 
End Sub

Sub clsDrawChart.SetPoint(ByVal x As Long, ByVal y As Long)
    m_prvx = m_x
    m_prvy = m_y
    m_x = x
    m_y = y
    InvalidateRect(m_hWnd, NULL, True)
    UpdateWindow(m_hWnd) 
End Sub

























