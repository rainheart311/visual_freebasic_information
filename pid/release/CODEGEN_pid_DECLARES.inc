'-----------------------------------------------------------------------------
' 由 VisualFreeBasic 5.9.2 生成的源代码
' 生成时间：2024年09月20日 16时26分21秒
' 更多信息请访问 www.yfvb.com 
'-----------------------------------------------------------------------------

' 这里是全局定义源码，所有type、全局变量、函数定义 都在此处。
#include Once "Afx/CGdiPlus/CGdiPlus.inc"
#include Once "afx\CWindow.inc"
Type FormControlsPro_TYPE '保存控件的私有属性，每个控件都不同，全部在此，各取所需。
   hWndParent     As hWnd       '父窗口句柄
   nName          As String     '名称\1\用来代码中识别对象的名称\Command\
   Index          As Long       '控件数组索引，小于零表示非控件数组
   IDC            As Long       '
   CtrlFocus      As HWND       ' 当前焦点在什么控件
   BigIcon        As HICON      '窗口大图标，主要用来主窗口
   SmallIcon      As HICON      '窗口小图标
   nText          as CWSTR      '
   ControlType    as Long       '控件类型，为了画控件  >=100 为虚拟控件  100=LABEL   TEXT=1 Button=2
   nData          As String     '编写控件使用，数据存放，无特定，控件根据自己需要存放任意数据。
   CtlData(99)    As Integer    '为每个控件提供 100 个数据储存(编写控件使用，控件根据自己需要存放任意数据)。
   UserData(99)   As Integer    '为每个控件提供 100 个数据储存(用户使用)。
   Style          as UInteger   '样式，每个控件都有自己的定义
   TransPer       As Long       '透明度\0\窗口透明度，百分比(0--100)，0%不透明 100%全透明\0\
   TransColor     As Long = &H197F7F7F  '透明颜色\3\透明颜色，需要用 GetCodeColorGDI 或 GetCodeColorGDIplue 转为 GDI 和 GDI+ 颜色值
   MousePointer   As Long               '指针\2\鼠标在窗口上的形状\0 - 默认,1 - 后台运行,2 - 标准箭头,3 - 十字光标,4 - 箭头和问号,5 - 文本工字光标,6 - 不可用禁止圈,7 - 移动,8 - 双箭头↙↗,9 - 双箭头↑↓,10 - 双箭头向↖↘,11 - 双箭头←→,12 - 垂直箭头,13 - 沙漏,14 - 手型
   ForeColor      As Long = &H197F7F7F  '文字色 需要用 GetCodeColorGDI 或 GetCodeColorGDIplue 转为 GDI 和 GDI+ 颜色值
   BackColor      As Long = &H197F7F7F  '背景色 需要用 GetCodeColorGDI 或 GetCodeColorGDIplue 转为 GDI 和 GDI+ 颜色值
   hBackBrush     As HBRUSH             '背景刷子，返回给窗口和控件用
   nFont          As String                   '字体\4\用于此对象的文本字体。\微软雅黑,9,0\
   Tag            As CWSTR                    ' 附加 \ 1 \ 私有自定义文本与控件关联。 \ \
   ToolTip        As CWSTR                    ' 提示 \ 1 \ 一个提示，当鼠标光标悬停在控件时显示它。 \ \
   ToolTipBalloon As Long                     ' 气球样式 \ 2 \ 一个气球样式显示工具提示。 \ False \ True, False
   ToolWnd        As HWND                     '提示窗口句柄，用来销毁
   nCursor        As HCURSOR                  '鼠标指针句柄
   nLeft          As Long                     '返回/设置相对于父窗口的 X 响应DPI数值，为 100%DPI数值（像素）
   nTop           As Long                     '返回/设置相对于父窗口的 Y 响应DPI数值，为 100%DPI数值（像素）
   nWidth         As Long                     '返回/设置控件宽度 响应DPI数值，为 100%DPI数值（像素）
   nHeight        As Long                     '返回/设置控件高度 响应DPI数值，为 100%DPI数值（像素））
   anchor         AS LONG                     '控件布局 自动调整方式
   nRight         As Long                     '控件布局 用，相对于窗口右边距离 响应DPI数值，为 100%DPI数值（像素））
   nBottom        As Long                     '控件布局 用，相对于窗口底边距离 响应DPI数值，为 100%DPI数值（像素））
   centerX        AS LONG                     '控件布局 用 中点  响应DPI数值，为 100%DPI数值（像素）
   centerY        AS LONG                     '控件布局 用
   VrControls     As FormControlsPro_TYPE ptr '如果是主窗口带 虚拟控件 ，就是下一个虚拟控件链表指针，直到为 0 表示没了
End Type




Declare Function gFLY_GetFontHandles(mFont As String) As HFONT
Declare Function GetCodeColorGDI(coColor As Long,defaultColor As Long =-1) As Long
Declare Function GetCodeColorGDIplue(coColor As Long,defaultColor As Long =0) As Long
Declare Function FF_AddTooltip(hWndForm AS HWND, strTooltipText AS wString, bBalloon AS Long, X as Long = 0, Y As Long = 0, W As Long = 0, H As Long = 0) As HWND
Declare Sub FLY_VFB_Layout_hWndForm(hWndForm As HWND) 
Declare Sub FLY_VFB_Layout_Handle(fp As FormControlsPro_TYPE ptr, pWidth AS LONG, pHeight AS LONG, nWidth AS LONG, nHeight AS LONG, ByRef x AS LONG, ByRef y AS LONG, ByRef xWidth AS LONG, ByRef yHeight AS LONG)

Declare Sub DrawLine(ByVal hDC as hDC, ByVal X1 as Integer, ByVal Y1 as Integer, ByVal X2 as Integer, ByVal Y2 as Integer, _ '[FILE:G:\FreeBASIC\pid\release\CODEGEN_pid_UTILITY.inc|341]
ByVal nPenStyle as Integer, ByVal nWidth as Integer, ByVal crColor as Long) '[FILE:G:\FreeBASIC\pid\release\CODEGEN_pid_UTILITY.inc|342]
Declare Sub DrawFrame(DC as hDC, X1 as Long, Y1 as Long, X2 as Long, Y2 as Long, C as Long, L as Long = PS_NULL, cL as Long = 0, wL as Long = 1) '[FILE:G:\FreeBASIC\pid\release\CODEGEN_pid_UTILITY.inc|372]
Declare Sub DrawCircleFrame(ByVal sDC as hDC, ByVal X as Long, ByVal Y as Long, ByVal hWidth as Long, ByVal hHeight as Long, ByVal xCircle as Long, ByVal yCircle as Long, _ '[FILE:G:\FreeBASIC\pid\release\CODEGEN_pid_UTILITY.inc|413]
ByVal LineWidth as Long, ByVal LineType as Long , ByVal bColor as Long, ByVal tColor as Long) '[FILE:G:\FreeBASIC\pid\release\CODEGEN_pid_UTILITY.inc|414]
Declare Sub DrawEllipse(ByVal hDC as hDC, ByVal X as Integer, ByVal Y as Integer, ByVal hWidth as Integer, ByVal hHeight as Integer, _ '[FILE:G:\FreeBASIC\pid\release\CODEGEN_pid_UTILITY.inc|466]
ByVal LineType as Integer ,ByVal LineWidth as Integer, ByVal bColor as  Integer , ByVal tColor as  Integer ) '[FILE:G:\FreeBASIC\pid\release\CODEGEN_pid_UTILITY.inc|467]
Declare Function GetTextWidth(ByVal DC as hDC, nText as String) as Long '[FILE:G:\FreeBASIC\pid\release\CODEGEN_pid_UTILITY.inc|510]
Declare Function GetTextHeight(ByVal DC as hDC, nText as String) as Long '[FILE:G:\FreeBASIC\pid\release\CODEGEN_pid_UTILITY.inc|517]
Declare Sub FF_Redraw(HWND_F as HWnd ) '[FILE:G:\FreeBASIC\pid\release\CODEGEN_pid_UTILITY.inc|524]

#include Once "yGDI.inc"
#include Once "Form\ClsForm.inc"
#include Once "ClsControl.inc"
#include Once "ClsVirtualCl.inc"

#include Once "TextBox\ClsTextBox.inc"

#include Once "Label\ClsLabel.inc"

#include Once "Picture\ClsPicture.inc"

#include Once "Button\ClsButton.inc"


Type PIDController '[FILE:G:\FreeBASIC\pid\modules\Module1.Inc|2]
Private : '[FILE:G:\FreeBASIC\pid\modules\Module1.Inc|3]
kp_ As Double               '//比例增益 '[FILE:G:\FreeBASIC\pid\modules\Module1.Inc|4]
ki_ As Double               '//积分增益 '[FILE:G:\FreeBASIC\pid\modules\Module1.Inc|5]
kd_ As Double               '//微分增益 '[FILE:G:\FreeBASIC\pid\modules\Module1.Inc|6]
integral_ As Double = 0.0   '// 积分项累计值 '[FILE:G:\FreeBASIC\pid\modules\Module1.Inc|7]
lastError_ As Double = 0.0  '// 上一个误差 '[FILE:G:\FreeBASIC\pid\modules\Module1.Inc|8]
setPoint_ As Double         '// 设定点 '[FILE:G:\FreeBASIC\pid\modules\Module1.Inc|9]
Public: '[FILE:G:\FreeBASIC\pid\modules\Module1.Inc|10]
Declare Constructor(ByVal kp As Double, ByVal ki As Double, ByVal kd As Double, ByVal setPoint As Double) '[FILE:G:\FreeBASIC\pid\modules\Module1.Inc|11]
Declare Function Calculate(ByVal currentValue As Double) As Double '[FILE:G:\FreeBASIC\pid\modules\Module1.Inc|12]
Declare Sub setSetPoint(ByVal setPoint As Double) '[FILE:G:\FreeBASIC\pid\modules\Module1.Inc|13]
End Type '[FILE:G:\FreeBASIC\pid\modules\Module1.Inc|14]
Type clsDrawChart '[FILE:G:\FreeBASIC\pid\modules\Module2.Inc|2]
Private: '[FILE:G:\FreeBASIC\pid\modules\Module2.Inc|3]
m_hWnd As hWnd '[FILE:G:\FreeBASIC\pid\modules\Module2.Inc|4]
m_prvx As Long = 0 '[FILE:G:\FreeBASIC\pid\modules\Module2.Inc|5]
m_prvy As Long = 0 '[FILE:G:\FreeBASIC\pid\modules\Module2.Inc|6]
m_x As Long = 0 '[FILE:G:\FreeBASIC\pid\modules\Module2.Inc|7]
m_y As Long = 0 '[FILE:G:\FreeBASIC\pid\modules\Module2.Inc|8]
Private: '[FILE:G:\FreeBASIC\pid\modules\Module2.Inc|9]
Declare Static Function DrawChartSubclass(ByVal hWin As hWnd, ByVal uMsg As UINT, ByVal wParam As wParam, ByVal lParam As lParam, ByVal uIdSubclass As UINT_PTR, ByVal dwRefData As DWORD_PTR) As LResult '[FILE:G:\FreeBASIC\pid\modules\Module2.Inc|10]
 '[FILE:G:\FreeBASIC\pid\modules\Module2.Inc|11]
'Public: '[FILE:G:\FreeBASIC\pid\modules\Module2.Inc|12]
'    Declare Constructor '[FILE:G:\FreeBASIC\pid\modules\Module2.Inc|13]
'    Declare Destructor '[FILE:G:\FreeBASIC\pid\modules\Module2.Inc|14]
Public: '[FILE:G:\FreeBASIC\pid\modules\Module2.Inc|15]
Declare Sub BindWindow(ByVal hWin As hWnd) '[FILE:G:\FreeBASIC\pid\modules\Module2.Inc|16]
Declare Sub NewPoint() '[FILE:G:\FreeBASIC\pid\modules\Module2.Inc|17]
Declare Sub SetPoint(ByVal x As Long,ByVal y As Long) '[FILE:G:\FreeBASIC\pid\modules\Module2.Inc|18]
End Type '[FILE:G:\FreeBASIC\pid\modules\Module2.Inc|19]
Type Form1_Class_Form Extends Class_Form
    Text1 As Class_TextBox
    Label1 As Class_Label
    Picture1 As Class_Picture
    Command1 As Class_Button
    Text2 As Class_TextBox
    Label2 As Class_Label
    Text3 As Class_TextBox
    Label3 As Class_Label
    Text4 As Class_TextBox
    Label4 As Class_Label
   Declare Function Show(hWndParent As .hWnd =HWND_DESKTOP,Modal As Boolean = False ,UserData As Integer = 0) As .hWnd '加载窗口并且显示, 模态显示用True {2.True.False} UserData 是传递给创建事件的参数，不保存数值
End Type
Dim Shared gFLY_FontNames() As String ,gFLY_FontHandles() As HFONT
Dim Shared Form1 As Form1_Class_Form


Dim Shared chart As clsDrawChart '[FILE:G:\FreeBASIC\pid\forms\Form1.frm|1]
Declare Function FF_PUMPHOOK( uMsg As Msg ) As Long '[FILE:#FF_PumpHook#|0]
Declare Function FF_WINMAIN(ByVal hInstance As HINSTANCE) As Long '[FILE:#FF_WinMain#|0]
Declare Sub FF_WINEND(ByVal hInstance As HINSTANCE) '[FILE:#FF_WinMain#|18]
Declare Function Form1_FORMPROCEDURE(ByVal hWndForm As hWnd, ByVal wMsg As UInteger, ByVal wParam As wParam, ByVal lParam As lParam) As LResult '[FILE:G:\FreeBASIC\pid\forms\Form1.frm|-289]
Declare Function Form1_CODEPROCEDURE(ByVal hWndControl As HWND, ByVal wMsg As uInteger, ByVal wParam As WPARAM, ByVal lParam As LPARAM) As LRESULT '[FILE:G:\FreeBASIC\pid\forms\Form1.frm|-60]
Declare Sub Form1_Command1_BN_Clicked(hWndForm As hWnd, hWndControl As hWnd) '[FILE:G:\FreeBASIC\pid\forms\Form1.frm|3]
Declare Sub Form1_Shown(hWndForm As hWnd,UserData As Integer) '[FILE:G:\FreeBASIC\pid\forms\Form1.frm|18]
