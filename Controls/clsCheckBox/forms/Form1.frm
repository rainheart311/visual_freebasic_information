#FireFly_Form#  Version=4.0.1
Locked=0
ClientOffset=0
ClientWidth=357
ClientHeight=209


[ControlType] Form | PropertyCount=25 | zorder=1 | tabindex=0 | 
name=Form1
classstyles=CS_VREDRAW, CS_HREDRAW, CS_DBLCLKS
windowstyles=WS_POPUP, WS_THICKFRAME, WS_CAPTION, WS_SYSMENU, WS_MINIMIZEBOX, WS_MAXIMIZEBOX, WS_CLIPSIBLINGS, WS_CLIPCHILDREN, WS_VISIBLE|WS_EX_WINDOWEDGE, WS_EX_CONTROLPARENT, WS_EX_LEFT, WS_EX_LTRREADING, WS_EX_RIGHTSCROLLBAR
backbitmap=
backbitmapmode=0 - 平铺
backcolor=SYS,15
caption=复选框控件的使用
export=False
height=247
icon=
left=0
mdichild=False
minwidth=0
minheight=0
maxwidth=0
maxheight=0
multilanguage=True
startupposition=1 - 中心
tabcontrolchild=False
tabcontrolchildautosize=False
tag=
tag2=
top=0
width=373
windowstate=0 - 正常

[ControlType] CheckBox | PropertyCount=21 | zorder=1 | tabindex=11 | 
name=chkShow
windowstyles=WS_CHILD, WS_VISIBLE, WS_TABSTOP, BS_TEXT, BS_NOTIFY, BS_AUTOCHECKBOX, BS_LEFT, BS_VCENTER|WS_EX_LEFT, WS_EX_LTRREADING
backcolor=SYS,15
backstyle=1 - 不透明
caption=Check1
checked=0 - UnChecked
controlindex=0
font=微软雅黑,9,0
fontupgrade=False
forecolor=SYS,8
height=57
left=84
locked=False
multilanguage=True
resizerules=
tag=
tag2=
tooltip=
tooltipballoon=False
top=31
width=243

[ControlType] CommandButton | PropertyCount=18 | zorder=2 | tabindex=10 | 
name=Command1
windowstyles=WS_CHILD, WS_VISIBLE, WS_TABSTOP, BS_TEXT, BS_PUSHBUTTON, BS_NOTIFY, BS_CENTER, BS_VCENTER|WS_EX_LEFT, WS_EX_LTRREADING
cancel=False
caption=标题
controlindex=0
font=微软雅黑,9,0
fontupgrade=False
height=29
left=16
locked=False
multilanguage=True
resizerules=
tag=
tag2=
tooltip=
tooltipballoon=False
top=120
width=52

[ControlType] CommandButton | PropertyCount=18 | zorder=3 | tabindex=7 | 
name=Command2
windowstyles=WS_CHILD, WS_VISIBLE, WS_TABSTOP, BS_TEXT, BS_PUSHBUTTON, BS_NOTIFY, BS_CENTER, BS_VCENTER|WS_EX_LEFT, WS_EX_LTRREADING
cancel=False
caption=使能
controlindex=0
font=微软雅黑,9,0
fontupgrade=False
height=29
left=85
locked=False
multilanguage=True
resizerules=
tag=
tag2=
tooltip=
tooltipballoon=False
top=120
width=52

[ControlType] CommandButton | PropertyCount=18 | zorder=4 | tabindex=8 | 
name=Command3
windowstyles=WS_CHILD, WS_VISIBLE, WS_TABSTOP, BS_TEXT, BS_PUSHBUTTON, BS_NOTIFY, BS_CENTER, BS_VCENTER|WS_EX_LEFT, WS_EX_LTRREADING
cancel=False
caption=可见
controlindex=0
font=微软雅黑,9,0
fontupgrade=False
height=29
left=154
locked=False
multilanguage=True
resizerules=
tag=
tag2=
tooltip=
tooltipballoon=False
top=120
width=52

[ControlType] CommandButton | PropertyCount=18 | zorder=5 | tabindex=9 | 
name=Command4
windowstyles=WS_CHILD, WS_VISIBLE, WS_TABSTOP, BS_TEXT, BS_PUSHBUTTON, BS_NOTIFY, BS_CENTER, BS_VCENTER|WS_EX_LEFT, WS_EX_LTRREADING
cancel=False
caption=选中
controlindex=0
font=微软雅黑,9,0
fontupgrade=False
height=29
left=223
locked=False
multilanguage=True
resizerules=
tag=
tag2=
tooltip=
tooltipballoon=False
top=120
width=52

[ControlType] CommandButton | PropertyCount=18 | zorder=6 | tabindex=6 | 
name=Command5
windowstyles=WS_CHILD, WS_VISIBLE, WS_TABSTOP, BS_TEXT, BS_PUSHBUTTON, BS_NOTIFY, BS_CENTER, BS_VCENTER|WS_EX_LEFT, WS_EX_LTRREADING
cancel=False
caption=Tag
controlindex=0
font=微软雅黑,9,0
fontupgrade=False
height=29
left=292
locked=False
multilanguage=True
resizerules=
tag=
tag2=
tooltip=
tooltipballoon=False
top=120
width=52

[ControlType] CommandButton | PropertyCount=18 | zorder=7 | tabindex=2 | 
name=Command6
windowstyles=WS_CHILD, WS_VISIBLE, WS_TABSTOP, BS_TEXT, BS_PUSHBUTTON, BS_NOTIFY, BS_CENTER, BS_VCENTER|WS_EX_LEFT, WS_EX_LTRREADING
cancel=False
caption=大小
controlindex=0
font=微软雅黑,9,0
fontupgrade=False
height=29
left=16
locked=False
multilanguage=True
resizerules=
tag=
tag2=
tooltip=
tooltipballoon=False
top=169
width=52

[ControlType] CommandButton | PropertyCount=18 | zorder=8 | tabindex=1 | 
name=Command7
windowstyles=WS_CHILD, WS_VISIBLE, WS_TABSTOP, BS_TEXT, BS_PUSHBUTTON, BS_NOTIFY, BS_CENTER, BS_VCENTER|WS_EX_LEFT, WS_EX_LTRREADING
cancel=False
caption=左移
controlindex=0
font=微软雅黑,9,0
fontupgrade=False
height=29
left=85
locked=False
multilanguage=True
resizerules=
tag=
tag2=
tooltip=
tooltipballoon=False
top=170
width=52

[ControlType] CommandButton | PropertyCount=18 | zorder=9 | tabindex=3 | 
name=Command8
windowstyles=WS_CHILD, WS_VISIBLE, WS_TABSTOP, BS_TEXT, BS_PUSHBUTTON, BS_NOTIFY, BS_CENTER, BS_VCENTER|WS_EX_LEFT, WS_EX_LTRREADING
cancel=False
caption=右移
controlindex=0
font=微软雅黑,9,0
fontupgrade=False
height=29
left=154
locked=False
multilanguage=True
resizerules=
tag=
tag2=
tooltip=
tooltipballoon=False
top=170
width=52

[ControlType] CommandButton | PropertyCount=18 | zorder=10 | tabindex=5 | 
name=Command9
windowstyles=WS_CHILD, WS_VISIBLE, WS_TABSTOP, BS_TEXT, BS_PUSHBUTTON, BS_NOTIFY, BS_CENTER, BS_VCENTER|WS_EX_LEFT, WS_EX_LTRREADING
cancel=False
caption=上移
controlindex=0
font=微软雅黑,9,0
fontupgrade=False
height=29
left=223
locked=False
multilanguage=True
resizerules=
tag=
tag2=
tooltip=
tooltipballoon=False
top=170
width=52

[ControlType] CommandButton | PropertyCount=18 | zorder=11 | tabindex=4 | 
name=Command10
windowstyles=WS_CHILD, WS_VISIBLE, WS_TABSTOP, BS_TEXT, BS_PUSHBUTTON, BS_NOTIFY, BS_CENTER, BS_VCENTER|WS_EX_LEFT, WS_EX_LTRREADING
cancel=False
caption=下移
controlindex=0
font=微软雅黑,9,0
fontupgrade=False
height=29
left=292
locked=False
multilanguage=True
resizerules=
tag=
tag2=
tooltip=
tooltipballoon=False
top=168
width=52

[AllCode]
'这是标准的工程模版，你也可做自己的模版。
'写好工程，复制全部文件到VFB软件文件夹里【template】里即可，子文件夹名为 VFB新建工程里显示的名称
'快去打造属于你自己的工程模版吧。
'32位与64位切换，点击VFB右下角
'--------------------------------------------------------------------------------
Function FORM1_WM_CREATE ( _
                         hWndForm As hWnd, _          ' 窗体句柄
                         ByVal UserData As Integer _  ' 可选的用户定义的值
                         ) As Long
    'chkShow.Font = AfxCreateFont("宋体", 32)  '修改字体大小
  
    With chkShow
       .FontName = "宋体"
       .FontSize = 30   
       .FontBold = False
       .FontItalic = False
       .FontStrikethru = False
       .FontUnderline = False 
    End With 
    Function = 0   ' 根据你的需要改变
End Function


'                                                                                  
Function Form1_Command1_BN_Clicked ( _
                                   ControlIndex     As Long, _      ' 控件数组的索引
                                   hWndForm         As hWnd, _      ' 窗体句柄
                                   hWndControl      As hWnd, _      ' 控件句柄
                                   idButtonControl  As Long   _     ' 按钮的标识符
                                   ) As Long
    If chkShow.Caption = "Check1" Then    '判断标题
        chkShow.Caption = "复选框标题"    '修改标题
    Else
        chkShow.Caption = "Check1"        '改回原来的标题
    End If
    
    Function = 0   ' 根据你的需要改变
End Function

'                                                                                  
Function Form1_Command2_BN_Clicked ( _
                                   ControlIndex     As Long, _      ' 控件数组的索引
                                   hWndForm         As hWnd, _      ' 窗体句柄
                                   hWndControl      As hWnd, _      ' 控件句柄
                                   idButtonControl  As Long   _     ' 按钮的标识符
                                   ) As Long
    If chkShow.Enabled Then
        chkShow.Enabled =False
    Else
        chkShow.Enabled = True
    End If
    
    Function = 0   ' 根据你的需要改变
End Function

'                                                                                  
Function Form1_Command3_BN_Clicked ( _
                                   ControlIndex     As Long, _      ' 控件数组的索引
                                   hWndForm         As hWnd, _      ' 窗体句柄
                                   hWndControl      As hWnd, _      ' 控件句柄
                                   idButtonControl  As Long   _     ' 按钮的标识符
                                   ) As Long
    If chkShow.Visible Then
        chkShow.Visible = False
    Else
        chkShow.Visible = True
    End If
    
    Function = 0   ' 根据你的需要改变
End Function

'                                                                                  
Function Form1_Command4_BN_Clicked ( _
                                   ControlIndex     As Long, _      ' 控件数组的索引
                                   hWndForm         As hWnd, _      ' 窗体句柄
                                   hWndControl      As hWnd, _      ' 控件句柄
                                   idButtonControl  As Long   _     ' 按钮的标识符
                                   ) As Long
    If chkShow.Value Then
        chkShow.Value = False
    Else
        chkShow.Value = True
    End If
    
    Function = 0   ' 根据你的需要改变
End Function

'                                                                                  
Function Form1_Command5_BN_Clicked ( _
                                   ControlIndex     As Long, _      ' 控件数组的索引
                                   hWndForm         As hWnd, _      ' 窗体句柄
                                   hWndControl      As hWnd, _      ' 控件句柄
                                   idButtonControl  As Long   _     ' 按钮的标识符
                                   ) As Long
    Print chkShow.Tag
    
    Randomize
    chkShow.Tag = Str(Int(Rnd * 100) + 1)
       
    Function = 0   ' 根据你的需要改变
End Function


'                                                                                  
Function Form1_Command6_BN_Clicked ( _
                                   ControlIndex     As Long, _      ' 控件数组的索引
                                   hWndForm         As hWnd, _      ' 窗体句柄
                                   hWndControl      As hWnd, _      ' 控件句柄
                                   idButtonControl  As Long   _     ' 按钮的标识符
                                   ) As Long
    Print chkShow.Width 
    Print chkShow.Height
    
    Function = 0   ' 根据你的需要改变
End Function

'                                                                                  
Function Form1_Command7_BN_Clicked ( _
                                   ControlIndex     As Long, _      ' 控件数组的索引
                                   hWndForm         As hWnd, _      ' 窗体句柄
                                   hWndControl      As hWnd, _      ' 控件句柄
                                   idButtonControl  As Long   _     ' 按钮的标识符
                                   ) As Long
    chkShow.Left = chkShow.Left - 10 
    Function = 0   ' 根据你的需要改变
End Function

'                                                                                  
Function Form1_Command8_BN_Clicked ( _
                                   ControlIndex     As Long, _      ' 控件数组的索引
                                   hWndForm         As hWnd, _      ' 窗体句柄
                                   hWndControl      As hWnd, _      ' 控件句柄
                                   idButtonControl  As Long   _     ' 按钮的标识符
                                   ) As Long
    chkShow.Left = chkShow.Left + 10 
    Function = 0   ' 根据你的需要改变
End Function

'                                                                                  
Function Form1_Command9_BN_Clicked ( _
                                   ControlIndex     As Long, _      ' 控件数组的索引
                                   hWndForm         As hWnd, _      ' 窗体句柄
                                   hWndControl      As hWnd, _      ' 控件句柄
                                   idButtonControl  As Long   _     ' 按钮的标识符
                                   ) As Long
    chkShow.Top = chkShow.Top - 10 
    Function = 0   ' 根据你的需要改变
End Function

'                                                                                  
Function Form1_Command10_BN_Clicked ( _
                                    ControlIndex     As Long, _      ' 控件数组的索引
                                    hWndForm         As hWnd, _      ' 窗体句柄
                                    hWndControl      As hWnd, _      ' 控件句柄
                                    idButtonControl  As Long   _     ' 按钮的标识符
                                    ) As Long
    chkShow.Top = chkShow.Top + 10
    Function = 0   ' 根据你的需要改变
End Function


'                                                                                  
Function Form1_chkShow_BN_Clicked ( _
                                  ControlIndex     As Long, _      ' 控件数组的索引
                                  hWndForm         As hWnd, _      ' 窗体句柄
                                  hWndControl      As hWnd, _      ' 控件句柄
                                  idButtonControl  As Long   _     ' 按钮的标识符
                                  ) As Long
    If chkShow.Value Then
        AfxMsg "选中"
    Else
        AfxMsg "取消"
    End If
    
    Function = 0   ' 根据你的需要改变
End Function

