#FireFly_Form#  Version=4.0.1
Locked=0
ClientOffset=0
ClientWidth=357
ClientHeight=309


[ControlType] Form | PropertyCount=25 | zorder=1 | tabindex=0 | 
name=Form1
classstyles=CS_VREDRAW, CS_HREDRAW, CS_DBLCLKS
windowstyles=WS_POPUP, WS_THICKFRAME, WS_CAPTION, WS_SYSMENU, WS_MINIMIZEBOX, WS_MAXIMIZEBOX, WS_CLIPSIBLINGS, WS_CLIPCHILDREN, WS_VISIBLE|WS_EX_WINDOWEDGE, WS_EX_CONTROLPARENT, WS_EX_LEFT, WS_EX_LTRREADING, WS_EX_RIGHTSCROLLBAR
backbitmap=
backbitmapmode=0 - 平铺
backcolor=SYS,15
caption=下拉列表控件的使用
export=False
height=347
icon=
left=0
mdichild=False
minwidth=0
minheight=0
maxwidth=0
maxheight=0
multilanguage=True
startupposition=1 - 中心
tabcontrolchild=False
tabcontrolchildautosize=False
tag=
tag2=
top=0
width=373
windowstate=0 - 正常

[ControlType] CommandButton | PropertyCount=18 | zorder=2 | tabindex=20 | 
name=Command1
windowstyles=WS_CHILD, WS_VISIBLE, WS_TABSTOP, BS_TEXT, BS_PUSHBUTTON, BS_NOTIFY, BS_CENTER, BS_VCENTER|WS_EX_LEFT, WS_EX_LTRREADING
cancel=False
caption=添加数据
controlindex=0
font=微软雅黑,9,0
fontupgrade=False
height=29
left=16
locked=False
multilanguage=True
resizerules=
tag=
tag2=
tooltip=
tooltipballoon=False
top=120
width=58

[ControlType] CommandButton | PropertyCount=18 | zorder=3 | tabindex=17 | 
name=Command2
windowstyles=WS_CHILD, WS_VISIBLE, WS_TABSTOP, BS_TEXT, BS_PUSHBUTTON, BS_NOTIFY, BS_CENTER, BS_VCENTER|WS_EX_LEFT, WS_EX_LTRREADING
cancel=False
caption=清空
controlindex=0
font=微软雅黑,9,0
fontupgrade=False
height=29
left=85
locked=False
multilanguage=True
resizerules=
tag=
tag2=
tooltip=
tooltipballoon=False
top=120
width=58

[ControlType] CommandButton | PropertyCount=18 | zorder=4 | tabindex=18 | 
name=Command3
windowstyles=WS_CHILD, WS_VISIBLE, WS_TABSTOP, BS_TEXT, BS_PUSHBUTTON, BS_NOTIFY, BS_CENTER, BS_VCENTER|WS_EX_LEFT, WS_EX_LTRREADING
cancel=False
caption=使能
controlindex=0
font=微软雅黑,9,0
fontupgrade=False
height=29
left=154
locked=False
multilanguage=True
resizerules=
tag=
tag2=
tooltip=
tooltipballoon=False
top=120
width=58

[ControlType] CommandButton | PropertyCount=18 | zorder=5 | tabindex=19 | 
name=Command4
windowstyles=WS_CHILD, WS_VISIBLE, WS_TABSTOP, BS_TEXT, BS_PUSHBUTTON, BS_NOTIFY, BS_CENTER, BS_VCENTER|WS_EX_LEFT, WS_EX_LTRREADING
cancel=False
caption=可见
controlindex=0
font=微软雅黑,9,0
fontupgrade=False
height=29
left=223
locked=False
multilanguage=True
resizerules=
tag=
tag2=
tooltip=
tooltipballoon=False
top=120
width=58

[ControlType] CommandButton | PropertyCount=18 | zorder=6 | tabindex=16 | 
name=Command5
windowstyles=WS_CHILD, WS_VISIBLE, WS_TABSTOP, BS_TEXT, BS_PUSHBUTTON, BS_NOTIFY, BS_CENTER, BS_VCENTER|WS_EX_LEFT, WS_EX_LTRREADING
cancel=False
caption=Tag
controlindex=0
font=微软雅黑,9,0
fontupgrade=False
height=29
left=292
locked=False
multilanguage=True
resizerules=
tag=
tag2=
tooltip=
tooltipballoon=False
top=120
width=58

[ControlType] CommandButton | PropertyCount=18 | zorder=7 | tabindex=12 | 
name=Command6
windowstyles=WS_CHILD, WS_VISIBLE, WS_TABSTOP, BS_TEXT, BS_PUSHBUTTON, BS_NOTIFY, BS_CENTER, BS_VCENTER|WS_EX_LEFT, WS_EX_LTRREADING
cancel=False
caption=大小
controlindex=0
font=微软雅黑,9,0
fontupgrade=False
height=29
left=16
locked=False
multilanguage=True
resizerules=
tag=
tag2=
tooltip=
tooltipballoon=False
top=169
width=58

[ControlType] CommandButton | PropertyCount=18 | zorder=8 | tabindex=11 | 
name=Command7
windowstyles=WS_CHILD, WS_VISIBLE, WS_TABSTOP, BS_TEXT, BS_PUSHBUTTON, BS_NOTIFY, BS_CENTER, BS_VCENTER|WS_EX_LEFT, WS_EX_LTRREADING
cancel=False
caption=左移
controlindex=0
font=微软雅黑,9,0
fontupgrade=False
height=29
left=85
locked=False
multilanguage=True
resizerules=
tag=
tag2=
tooltip=
tooltipballoon=False
top=170
width=58

[ControlType] CommandButton | PropertyCount=18 | zorder=9 | tabindex=13 | 
name=Command8
windowstyles=WS_CHILD, WS_VISIBLE, WS_TABSTOP, BS_TEXT, BS_PUSHBUTTON, BS_NOTIFY, BS_CENTER, BS_VCENTER|WS_EX_LEFT, WS_EX_LTRREADING
cancel=False
caption=右移
controlindex=0
font=微软雅黑,9,0
fontupgrade=False
height=29
left=154
locked=False
multilanguage=True
resizerules=
tag=
tag2=
tooltip=
tooltipballoon=False
top=170
width=58

[ControlType] CommandButton | PropertyCount=18 | zorder=10 | tabindex=15 | 
name=Command9
windowstyles=WS_CHILD, WS_VISIBLE, WS_TABSTOP, BS_TEXT, BS_PUSHBUTTON, BS_NOTIFY, BS_CENTER, BS_VCENTER|WS_EX_LEFT, WS_EX_LTRREADING
cancel=False
caption=上移
controlindex=0
font=微软雅黑,9,0
fontupgrade=False
height=29
left=223
locked=False
multilanguage=True
resizerules=
tag=
tag2=
tooltip=
tooltipballoon=False
top=170
width=58

[ControlType] CommandButton | PropertyCount=18 | zorder=11 | tabindex=14 | 
name=Command10
windowstyles=WS_CHILD, WS_VISIBLE, WS_TABSTOP, BS_TEXT, BS_PUSHBUTTON, BS_NOTIFY, BS_CENTER, BS_VCENTER|WS_EX_LEFT, WS_EX_LTRREADING
cancel=False
caption=下移
controlindex=0
font=微软雅黑,9,0
fontupgrade=False
height=29
left=292
locked=False
multilanguage=True
resizerules=
tag=
tag2=
tooltip=
tooltipballoon=False
top=168
width=58

[ControlType] ComboBox | PropertyCount=19 | zorder=1 | tabindex=3 | 
name=cmbShow
windowstyles=WS_CHILD, WS_VISIBLE, WS_VSCROLL, WS_TABSTOP, CBS_DROPDOWNLIST|WS_EX_CLIENTEDGE, WS_EX_LEFT, WS_EX_LTRREADING, WS_EX_RIGHTSCROLLBAR
custom=
backcolor=SYS,5
controlindex=0
font=微软雅黑,9,0
fontupgrade=False
forecolor=SYS,8
height=25
initialselection=-1
left=59
locked=False
resizerules=
tag=
tag2=
tooltip=
tooltipballoon=False
top=40
width=253

[ControlType] CommandButton | PropertyCount=18 | zorder=1 | tabindex=1 | 
name=Command11
windowstyles=WS_CHILD, WS_VISIBLE, WS_TABSTOP, BS_TEXT, BS_PUSHBUTTON, BS_NOTIFY, BS_CENTER, BS_VCENTER|WS_EX_LEFT, WS_EX_LTRREADING
cancel=False
caption=子项数据
controlindex=0
font=微软雅黑,9,0
fontupgrade=False
height=29
left=15
locked=False
multilanguage=True
resizerules=
tag=
tag2=
tooltip=
tooltipballoon=False
top=221
width=58

[ControlType] CommandButton | PropertyCount=18 | zorder=2 | tabindex=2 | 
name=Command12
windowstyles=WS_CHILD, WS_VISIBLE, WS_TABSTOP, BS_TEXT, BS_PUSHBUTTON, BS_NOTIFY, BS_CENTER, BS_VCENTER|WS_EX_LEFT, WS_EX_LTRREADING
cancel=False
caption=列表总数
controlindex=0
font=微软雅黑,9,0
fontupgrade=False
height=29
left=84
locked=False
multilanguage=True
resizerules=
tag=
tag2=
tooltip=
tooltipballoon=False
top=222
width=58

[ControlType] CommandButton | PropertyCount=18 | zorder=3 | tabindex=4 | 
name=Command13
windowstyles=WS_CHILD, WS_VISIBLE, WS_TABSTOP, BS_TEXT, BS_PUSHBUTTON, BS_NOTIFY, BS_CENTER, BS_VCENTER|WS_EX_LEFT, WS_EX_LTRREADING
cancel=False
caption=当前序号
controlindex=0
font=微软雅黑,9,0
fontupgrade=False
height=29
left=153
locked=False
multilanguage=True
resizerules=
tag=
tag2=
tooltip=
tooltipballoon=False
top=222
width=58

[ControlType] CommandButton | PropertyCount=18 | zorder=4 | tabindex=5 | 
name=Command14
windowstyles=WS_CHILD, WS_VISIBLE, WS_TABSTOP, BS_TEXT, BS_PUSHBUTTON, BS_NOTIFY, BS_CENTER, BS_VCENTER|WS_EX_LEFT, WS_EX_LTRREADING
cancel=False
caption=顶部序号
controlindex=0
font=微软雅黑,9,0
fontupgrade=False
height=29
left=222
locked=False
multilanguage=True
resizerules=
tag=
tag2=
tooltip=
tooltipballoon=False
top=222
width=58

[ControlType] CommandButton | PropertyCount=18 | zorder=5 | tabindex=9 | 
name=Command15
windowstyles=WS_CHILD, WS_VISIBLE, WS_TABSTOP, BS_TEXT, BS_PUSHBUTTON, BS_NOTIFY, BS_CENTER, BS_VCENTER|WS_EX_LEFT, WS_EX_LTRREADING
cancel=False
caption=插入数据
controlindex=0
font=微软雅黑,9,0
fontupgrade=False
height=29
left=291
locked=False
multilanguage=True
resizerules=
tag=
tag2=
tooltip=
tooltipballoon=False
top=220
width=58

[ControlType] CommandButton | PropertyCount=18 | zorder=6 | tabindex=10 | 
name=Command16
windowstyles=WS_CHILD, WS_VISIBLE, WS_TABSTOP, BS_TEXT, BS_PUSHBUTTON, BS_NOTIFY, BS_CENTER, BS_VCENTER|WS_EX_LEFT, WS_EX_LTRREADING
cancel=False
caption=列表
controlindex=0
font=微软雅黑,9,0
fontupgrade=False
height=29
left=15
locked=False
multilanguage=True
resizerules=
tag=
tag2=
tooltip=
tooltipballoon=False
top=271
width=58

[ControlType] CommandButton | PropertyCount=18 | zorder=7 | tabindex=8 | 
name=Command17
windowstyles=WS_CHILD, WS_VISIBLE, WS_TABSTOP, BS_TEXT, BS_PUSHBUTTON, BS_NOTIFY, BS_CENTER, BS_VCENTER|WS_EX_LEFT, WS_EX_LTRREADING
cancel=False
caption=查找子项数据
controlindex=0
font=微软雅黑,9,0
fontupgrade=False
height=29
left=79
locked=False
multilanguage=True
resizerules=
tag=
tag2=
tooltip=
tooltipballoon=False
top=271
width=84

[ControlType] CommandButton | PropertyCount=18 | zorder=8 | tabindex=6 | 
name=Command19
windowstyles=WS_CHILD, WS_VISIBLE, WS_TABSTOP, BS_TEXT, BS_PUSHBUTTON, BS_NOTIFY, BS_CENTER, BS_VCENTER|WS_EX_LEFT, WS_EX_LTRREADING
cancel=False
caption=查找指定项
controlindex=0
font=微软雅黑,9,0
fontupgrade=False
height=29
left=169
locked=False
multilanguage=True
resizerules=
tag=
tag2=
tooltip=
tooltipballoon=False
top=271
width=79

[ControlType] CommandButton | PropertyCount=18 | zorder=9 | tabindex=7 | 
name=Command20
windowstyles=WS_CHILD, WS_VISIBLE, WS_TABSTOP, BS_TEXT, BS_PUSHBUTTON, BS_NOTIFY, BS_CENTER, BS_VCENTER|WS_EX_LEFT, WS_EX_LTRREADING
cancel=False
caption=查找首个字符串
controlindex=0
font=微软雅黑,9,0
fontupgrade=False
height=29
left=254
locked=False
multilanguage=True
resizerules=
tag=
tag2=
tooltip=
tooltipballoon=False
top=271
width=95

[AllCode]
'这是标准的工程模版，你也可做自己的模版。
'写好工程，复制全部文件到VFB软件文件夹里【template】里即可，子文件夹名为 VFB新建工程里显示的名称
'快去打造属于你自己的工程模版吧。
'32位与64位切换，点击VFB右下角
'--------------------------------------------------------------------------------
Function FORM1_WM_CREATE ( _
                         hWndForm As hWnd, _          ' 窗体句柄
                         ByVal UserData As Integer _  ' 可选的用户定义的值
                         ) As Long
    'cmbShow.Font = AfxCreateFont("宋体",32)  '修改字体大小
    
    With cmbShow
       .FontName = "宋体"
       .FontSize = 30   
       .FontBold = False
       .FontItalic = False
       .FontStrikethru = False
       .FontUnderline = False 
    End With     
    
    Function = 0   ' 根据你的需要改变
End Function


'                                                                                  
Function Form1_Command1_BN_Clicked ( _
                                   ControlIndex     As Long, _      ' 控件数组的索引
                                   hWndForm         As hWnd, _      ' 窗体句柄
                                   hWndControl      As hWnd, _      ' 控件句柄
                                   idButtonControl  As Long   _     ' 按钮的标识符
                                   ) As Long    
    Static i As Long = 0
    
    cmbShow.AddItem "子项" & i
    cmbShow.ItemData(i) = i
    cmbShow.ListIndex = i
    i += 1

    Function = 0   ' 根据你的需要改变
End Function

'                                                                                  
Function Form1_Command2_BN_Clicked ( _
                                   ControlIndex     As Long, _      ' 控件数组的索引
                                   hWndForm         As hWnd, _      ' 窗体句柄
                                   hWndControl      As hWnd, _      ' 控件句柄
                                   idButtonControl  As Long   _     ' 按钮的标识符
                                   ) As Long
    cmbShow.Clear 
    
    Function = 0   ' 根据你的需要改变
End Function

'                                                                                  
Function Form1_Command3_BN_Clicked ( _
                                   ControlIndex     As Long, _      ' 控件数组的索引
                                   hWndForm         As hWnd, _      ' 窗体句柄
                                   hWndControl      As hWnd, _      ' 控件句柄
                                   idButtonControl  As Long   _     ' 按钮的标识符
                                   ) As Long
    If cmbShow.Enabled Then
        cmbShow.Enabled = False
    Else
        cmbShow.Enabled = True
    End If
    
    Function = 0   ' 根据你的需要改变
End Function

'                                                                                  
Function Form1_Command4_BN_Clicked ( _
                                   ControlIndex     As Long, _      ' 控件数组的索引
                                   hWndForm         As hWnd, _      ' 窗体句柄
                                   hWndControl      As hWnd, _      ' 控件句柄
                                   idButtonControl  As Long   _     ' 按钮的标识符
                                   ) As Long
    If cmbShow.Visible Then
        cmbShow.Visible = False
    Else
        cmbShow.Visible = True
    End If
    
    Function = 0   ' 根据你的需要改变
End Function

'                                                                                  
Function Form1_Command5_BN_Clicked ( _
                                   ControlIndex     As Long, _      ' 控件数组的索引
                                   hWndForm         As hWnd, _      ' 窗体句柄
                                   hWndControl      As hWnd, _      ' 控件句柄
                                   idButtonControl  As Long   _     ' 按钮的标识符
                                   ) As Long
    Print "Tag值 " & cmbShow.Tag
    
    Randomize
    cmbShow.Tag = Str(Int(Rnd * 100) + 1)
       
    Function = 0   ' 根据你的需要改变
End Function


'                                                                                  
Function Form1_Command14_BN_Clicked ( _
                                    ControlIndex     As Long, _      ' 控件数组的索引
                                    hWndForm         As hWnd, _      ' 窗体句柄
                                    hWndControl      As hWnd, _      ' 控件句柄
                                    idButtonControl  As Long   _     ' 按钮的标识符
                                    ) As Long
    Print cmbShow.GetTopIndex
    Function = 0   ' 根据你的需要改变
End Function

'                                                                                  
Function Form1_Command15_BN_Clicked ( _
                                    ControlIndex     As Long, _      ' 控件数组的索引
                                    hWndForm         As hWnd, _      ' 窗体句柄
                                    hWndControl      As hWnd, _      ' 控件句柄
                                    idButtonControl  As Long   _     ' 按钮的标识符
                                    ) As Long
    cmbShow.InsertItem 4,"Item" & 0
    cmbShow.ListIndex = 4
    
    Function = 0   ' 根据你的需要改变
End Function

'                                                                                  
Function Form1_Command11_BN_Clicked ( _
                                    ControlIndex     As Long, _      ' 控件数组的索引
                                    hWndForm         As hWnd, _      ' 窗体句柄
                                    hWndControl      As hWnd, _      ' 控件句柄
                                    idButtonControl  As Long   _     ' 按钮的标识符
                                    ) As Long
    Print cmbShow.ItemData(0)
    cmbShow.ItemData(0) = Int(Rnd * 100) + 1
     
    Function = 0   ' 根据你的需要改变
End Function

'                                                                                  
Function Form1_Command12_BN_Clicked ( _
                                    ControlIndex     As Long, _      ' 控件数组的索引
                                    hWndForm         As hWnd, _      ' 窗体句柄
                                    hWndControl      As hWnd, _      ' 控件句柄
                                    idButtonControl  As Long   _     ' 按钮的标识符
                                    ) As Long
    Print cmbShow.ListCount
    Function = 0   ' 根据你的需要改变
End Function

'                                                                                  
Function Form1_Command13_BN_Clicked ( _
                                    ControlIndex     As Long, _      ' 控件数组的索引
                                    hWndForm         As hWnd, _      ' 窗体句柄
                                    hWndControl      As hWnd, _      ' 控件句柄
                                    idButtonControl  As Long   _     ' 按钮的标识符
                                    ) As Long
    Print cmbShow.ListIndex
    Function = 0   ' 根据你的需要改变
End Function

'                                                                                  
Function Form1_Command16_BN_Clicked ( _
                                    ControlIndex     As Long, _      ' 控件数组的索引
                                    hWndForm         As hWnd, _      ' 窗体句柄
                                    hWndControl      As hWnd, _      ' 控件句柄
                                    idButtonControl  As Long   _     ' 按钮的标识符
                                    ) As Long
    Static i As Long = 0
    
    Print cmbShow.List(0)
    cmbShow.List(0) = "修改" & i
    i += 1

    Function = 0   ' 根据你的需要改变
End Function

'                                                                                  
Function Form1_Command17_BN_Clicked ( _
                                    ControlIndex     As Long, _      ' 控件数组的索引
                                    hWndForm         As hWnd, _      ' 窗体句柄
                                    hWndControl      As hWnd, _      ' 控件句柄
                                    idButtonControl  As Long   _     ' 按钮的标识符
                                    ) As Long
    Print cmbShow.FindItemData(0,1)
    
    Function = 0   ' 根据你的需要改变
End Function

'                                                                                  
Function Form1_Command19_BN_Clicked ( _
                                    ControlIndex     As Long, _      ' 控件数组的索引
                                    hWndForm         As hWnd, _      ' 窗体句柄
                                    hWndControl      As hWnd, _      ' 控件句柄
                                    idButtonControl  As Long   _     ' 按钮的标识符
                                    ) As Long
    Print cmbShow.FindString(0,"Item0")

    Function = 0   ' 根据你的需要改变
End Function

'                                                                                  
Function Form1_Command20_BN_Clicked ( _
                                    ControlIndex     As Long, _      ' 控件数组的索引
                                    hWndForm         As hWnd, _      ' 窗体句柄
                                    hWndControl      As hWnd, _      ' 控件句柄
                                    idButtonControl  As Long   _     ' 按钮的标识符
                                    ) As Long
    Print cmbShow.FindStringExact(0,"Item0")
    Function = 0   ' 根据你的需要改变
End Function

'                                                                                  
Function Form1_Command6_BN_Clicked ( _
                                   ControlIndex     As Long, _      ' 控件数组的索引
                                   hWndForm         As hWnd, _      ' 窗体句柄
                                   hWndControl      As hWnd, _      ' 控件句柄
                                   idButtonControl  As Long   _     ' 按钮的标识符
                                   ) As Long
    Print cmbShow.Height
    Print cmbShow.Width 
    
    Function = 0   ' 根据你的需要改变
End Function

'                                                                                  
Function Form1_Command7_BN_Clicked ( _
                                   ControlIndex     As Long, _      ' 控件数组的索引
                                   hWndForm         As hWnd, _      ' 窗体句柄
                                   hWndControl      As hWnd, _      ' 控件句柄
                                   idButtonControl  As Long   _     ' 按钮的标识符
                                   ) As Long
    cmbShow.Left = cmbShow.Left - 10
    
    Function = 0   ' 根据你的需要改变
End Function

'                                                                                  
Function Form1_Command8_BN_Clicked ( _
                                   ControlIndex     As Long, _      ' 控件数组的索引
                                   hWndForm         As hWnd, _      ' 窗体句柄
                                   hWndControl      As hWnd, _      ' 控件句柄
                                   idButtonControl  As Long   _     ' 按钮的标识符
                                   ) As Long
    cmbShow.Left = cmbShow.Left + 10
    Function = 0   ' 根据你的需要改变
End Function

'                                                                                  
Function Form1_Command9_BN_Clicked ( _
                                   ControlIndex     As Long, _      ' 控件数组的索引
                                   hWndForm         As hWnd, _      ' 窗体句柄
                                   hWndControl      As hWnd, _      ' 控件句柄
                                   idButtonControl  As Long   _     ' 按钮的标识符
                                   ) As Long
    cmbShow.Top = cmbShow.Top - 10
    Function = 0   ' 根据你的需要改变
End Function

'                                                                                  
Function Form1_Command10_BN_Clicked ( _
                                    ControlIndex     As Long, _      ' 控件数组的索引
                                    hWndForm         As hWnd, _      ' 窗体句柄
                                    hWndControl      As hWnd, _      ' 控件句柄
                                    idButtonControl  As Long   _     ' 按钮的标识符
                                    ) As Long
    cmbShow.Top = cmbShow.Top + 10
    Function = 0   ' 根据你的需要改变
End Function


'                                                                                  
Function Form1_cmbShow_CBN_SelChange ( _
                                     ControlIndex  As Long,  _     ' 控件数组索引
                                     hWndForm      As hWnd, _      ' 窗体句柄
                                     hWndControl   As hWnd, _      ' 控件句柄
                                     idComboBox    As Long  _      ' ComboBox控件的标识符
                                     ) As Long
    Print "当前文本是 " & cmbShow.Text
    
    Function = 0   ' 根据你的需要改变
End Function

