#FireFly_Form#  Version=4.0.1
Locked=0
ClientOffset=0
ClientWidth=484
ClientHeight=272


[ControlType] Form | PropertyCount=25 | zorder=1 | tabindex=0 | 
name=Form1
classstyles=CS_VREDRAW, CS_HREDRAW, CS_DBLCLKS
windowstyles=WS_POPUP, WS_THICKFRAME, WS_CAPTION, WS_SYSMENU, WS_MINIMIZEBOX, WS_MAXIMIZEBOX, WS_CLIPSIBLINGS, WS_CLIPCHILDREN, WS_VISIBLE|WS_EX_WINDOWEDGE, WS_EX_CONTROLPARENT, WS_EX_LEFT, WS_EX_LTRREADING, WS_EX_RIGHTSCROLLBAR
backbitmap=
backbitmapmode=0 - 平铺
backcolor=SYS,15
caption=按钮控件使用演示
export=False
height=310
icon=
left=0
mdichild=False
minwidth=0
minheight=0
maxwidth=0
maxheight=0
multilanguage=True
startupposition=1 - 中心
tabcontrolchild=False
tabcontrolchildautosize=False
tag=
tag2=
top=0
width=500
windowstate=0 - 正常

[ControlType] CommandButton | PropertyCount=18 | zorder=1 | tabindex=11 | 
name=cmdCaption
windowstyles=WS_CHILD, WS_VISIBLE, WS_TABSTOP, BS_TEXT, BS_PUSHBUTTON, BS_NOTIFY, BS_CENTER, BS_VCENTER|WS_EX_LEFT, WS_EX_LTRREADING
cancel=False
caption=标题
controlindex=0
font=微软雅黑,9,0
fontupgrade=False
height=32
left=37
locked=False
multilanguage=True
resizerules=
tag=
tag2=
tooltip=
tooltipballoon=False
top=139
width=62

[ControlType] CommandButton | PropertyCount=18 | zorder=2 | tabindex=10 | 
name=cmdEnabled
windowstyles=WS_CHILD, WS_VISIBLE, WS_TABSTOP, BS_TEXT, BS_PUSHBUTTON, BS_NOTIFY, BS_CENTER, BS_VCENTER|WS_EX_LEFT, WS_EX_LTRREADING
cancel=False
caption=使能
controlindex=0
font=微软雅黑,9,0
fontupgrade=False
height=32
left=113
locked=False
multilanguage=True
resizerules=
tag=
tag2=
tooltip=
tooltipballoon=False
top=139
width=62

[ControlType] CommandButton | PropertyCount=18 | zorder=3 | tabindex=9 | 
name=cmdShow
windowstyles=WS_CHILD, WS_VISIBLE, WS_TABSTOP, BS_TEXT, BS_PUSHBUTTON, BS_NOTIFY, BS_CENTER, BS_VCENTER|WS_EX_LEFT, WS_EX_LTRREADING
cancel=False
caption=演示按钮
controlindex=0
font=微软雅黑,9,0
fontupgrade=False
height=70
left=140
locked=False
multilanguage=True
resizerules=
tag=
tag2=
tooltip=
tooltipballoon=False
top=27
width=200

[ControlType] CommandButton | PropertyCount=18 | zorder=4 | tabindex=6 | 
name=cmdVisible
windowstyles=WS_CHILD, WS_VISIBLE, WS_TABSTOP, BS_TEXT, BS_PUSHBUTTON, BS_NOTIFY, BS_CENTER, BS_VCENTER|WS_EX_LEFT, WS_EX_LTRREADING
cancel=False
caption=可见
controlindex=0
font=微软雅黑,9,0
fontupgrade=False
height=32
left=189
locked=False
multilanguage=True
resizerules=
tag=
tag2=
tooltip=
tooltipballoon=False
top=139
width=62

[ControlType] CommandButton | PropertyCount=18 | zorder=5 | tabindex=7 | 
name=cmdClick
windowstyles=WS_CHILD, WS_VISIBLE, WS_TABSTOP, BS_TEXT, BS_PUSHBUTTON, BS_NOTIFY, BS_CENTER, BS_VCENTER|WS_EX_LEFT, WS_EX_LTRREADING
cancel=False
caption=单击
controlindex=0
font=微软雅黑,9,0
fontupgrade=False
height=32
left=265
locked=False
multilanguage=True
resizerules=
tag=
tag2=
tooltip=
tooltipballoon=False
top=139
width=62

[ControlType] CommandButton | PropertyCount=18 | zorder=6 | tabindex=8 | 
name=Command1
windowstyles=WS_CHILD, WS_VISIBLE, WS_TABSTOP, BS_TEXT, BS_PUSHBUTTON, BS_NOTIFY, BS_CENTER, BS_VCENTER|WS_EX_LEFT, WS_EX_LTRREADING
cancel=False
caption=Tag
controlindex=0
font=微软雅黑,9,0
fontupgrade=False
height=32
left=341
locked=False
multilanguage=True
resizerules=
tag=
tag2=
tooltip=
tooltipballoon=False
top=139
width=62

[ControlType] CommandButton | PropertyCount=18 | zorder=7 | tabindex=5 | 
name=Command2
windowstyles=WS_CHILD, WS_VISIBLE, WS_TABSTOP, BS_TEXT, BS_PUSHBUTTON, BS_NOTIFY, BS_CENTER, BS_VCENTER|WS_EX_LEFT, WS_EX_LTRREADING
cancel=False
caption=大小
controlindex=0
font=微软雅黑,9,0
fontupgrade=False
height=32
left=36
locked=False
multilanguage=True
resizerules=
tag=
tag2=
tooltip=
tooltipballoon=False
top=182
width=62

[ControlType] CommandButton | PropertyCount=18 | zorder=8 | tabindex=2 | 
name=Command3
windowstyles=WS_CHILD, WS_VISIBLE, WS_TABSTOP, BS_TEXT, BS_PUSHBUTTON, BS_NOTIFY, BS_CENTER, BS_VCENTER|WS_EX_LEFT, WS_EX_LTRREADING
cancel=False
caption=左移
controlindex=0
font=微软雅黑,9,0
fontupgrade=False
height=32
left=112
locked=False
multilanguage=True
resizerules=
tag=
tag2=
tooltip=
tooltipballoon=False
top=182
width=62

[ControlType] CommandButton | PropertyCount=18 | zorder=9 | tabindex=1 | 
name=Command4
windowstyles=WS_CHILD, WS_VISIBLE, WS_TABSTOP, BS_TEXT, BS_PUSHBUTTON, BS_NOTIFY, BS_CENTER, BS_VCENTER|WS_EX_LEFT, WS_EX_LTRREADING
cancel=False
caption=右移
controlindex=0
font=微软雅黑,9,0
fontupgrade=False
height=32
left=188
locked=False
multilanguage=True
resizerules=
tag=
tag2=
tooltip=
tooltipballoon=False
top=182
width=62

[ControlType] CommandButton | PropertyCount=18 | zorder=10 | tabindex=4 | 
name=Command5
windowstyles=WS_CHILD, WS_VISIBLE, WS_TABSTOP, BS_TEXT, BS_PUSHBUTTON, BS_NOTIFY, BS_CENTER, BS_VCENTER|WS_EX_LEFT, WS_EX_LTRREADING
cancel=False
caption=上移
controlindex=0
font=微软雅黑,9,0
fontupgrade=False
height=32
left=264
locked=False
multilanguage=True
resizerules=
tag=
tag2=
tooltip=
tooltipballoon=False
top=182
width=62

[ControlType] CommandButton | PropertyCount=18 | zorder=11 | tabindex=3 | 
name=Command6
windowstyles=WS_CHILD, WS_VISIBLE, WS_TABSTOP, BS_TEXT, BS_PUSHBUTTON, BS_NOTIFY, BS_CENTER, BS_VCENTER|WS_EX_LEFT, WS_EX_LTRREADING
cancel=False
caption=下移
controlindex=0
font=微软雅黑,9,0
fontupgrade=False
height=32
left=340
locked=False
multilanguage=True
resizerules=
tag=
tag2=
tooltip=
tooltipballoon=False
top=182
width=62

[AllCode]
'这是标准的工程模版，你也可做自己的模版。
'写好工程，复制全部文件到VFB软件文件夹里【template】里即可，子文件夹名为 VFB新建工程里显示的名称
'快去打造属于你自己的工程模版吧。
'32位与64位切换，点击VFB右下角
'--------------------------------------------------------------------------------
Function FORM1_WM_CREATE ( _
                         hWndForm As hWnd, _          ' 窗体句柄
                         ByVal UserData As Integer _  ' 可选的用户定义的值
                         ) As Long
   '可以用afx函数创建字体
   'cmdShow.Font = AfxCreateFont("宋体",16)    '初始化字体
   '也可以直接设置字体属性
   With cmdShow
       .FontName = "宋体"
       .FontSize = 30   
       .FontBold = False
       .FontItalic = False
       .FontStrikethru = False
       .FontUnderline = False 
   End With 
   Function = 0   ' 根据你的需要改变
End Function


'Caption（标题）属性的使用                                                                                  
Function Form1_cmdCaption_BN_Clicked ( _
                                   ControlIndex     As Long, _      ' 控件数组的索引
                                   hWndForm         As hWnd, _      ' 窗体句柄
                                   hWndControl      As hWnd, _      ' 控件句柄
                                   idButtonControl  As Long   _     ' 按钮的标识符
                                   ) As Long
    
    If cmdShow.Caption = "演示按钮" Then                                       '如果按钮标题是"演示按钮"
        AfxMsg("原标题是：" & cmdShow.Caption & vbCrLf & "修改为：演示完毕")   '提示按钮标题，和修改内容
        cmdShow.Caption = "演示完毕"                                           '修改按钮标题
    Else                                                                       '否则
        AfxMsg("原标题是：" & cmdShow.Caption & vbCrLf & "修改为：演示按钮")   '提示按钮标题，和修改内容                                                                    
        cmdShow.Caption = "演示按钮"                                           '修改按钮标题
    End If
    
    Function = 0   ' 根据你的需要改变
End Function

'Enabled（使能）属性的使用                                                                                  
Function Form1_cmdEnabled_BN_Clicked ( _
                                     ControlIndex     As Long, _      ' 控件数组的索引
                                     hWndForm         As hWnd, _      ' 窗体句柄
                                     hWndControl      As hWnd, _      ' 控件句柄
                                     idButtonControl  As Long   _     ' 按钮的标识符
                                     ) As Long
    If cmdShow.Enabled Then         '如果按钮使能
        cmdShow.Enabled = False     '则修改为禁用
    Else                            '否则
        cmdShow.Enabled = True      '修改为使能
    End If
    
    Function = 0   ' 根据你的需要改变
End Function

'                                                                                  
Function Form1_cmdVisible_BN_Clicked ( _
                                     ControlIndex     As Long, _      ' 控件数组的索引
                                     hWndForm         As HWnd, _      ' 窗体句柄
                                     hWndControl      As HWnd, _      ' 控件句柄
                                     idButtonControl  As Long   _     ' 按钮的标识符
                                     ) As Long
    If cmdShow.Visible Then           '如果按钮可见
        cmdShow.Visible  = False      '则修改为不可见
    Else                              '否则
        cmdShow.Visible  = True       '修改为可见
    End If
    
    Function = 0   ' 根据你的需要改变
End Function

'                                                                                  
Function Form1_cmdClick_BN_Clicked ( _
                                   ControlIndex     As Long, _      ' 控件数组的索引
                                   hWndForm         As HWnd, _      ' 窗体句柄
                                   hWndControl      As HWnd, _      ' 控件句柄
                                   idButtonControl  As Long   _     ' 按钮的标识符
                                   ) As Long
    cmdShow.Click                 '时cmdShow按钮产生一次单击事件
    Function = 0   ' 根据你的需要改变
End Function

'                                                                                  
Function Form1_cmdShow_BN_Clicked ( _
                                  ControlIndex     As Long, _      ' 控件数组的索引
                                  hWndForm         As HWnd, _      ' 窗体句柄
                                  hWndControl      As HWnd, _      ' 控件句柄
                                  idButtonControl  As Long   _     ' 按钮的标识符
                                  ) As Long
    AfxMsg("你单击到我了！")      '单击时显示对话框
    Function = 0   ' 根据你的需要改变
End Function

'                                                                                  
Function Form1_Command1_BN_Clicked ( _
                                   ControlIndex     As Long, _      ' 控件数组的索引
                                   hWndForm         As HWnd, _      ' 窗体句柄
                                   hWndControl      As HWnd, _      ' 控件句柄
                                   idButtonControl  As Long   _     ' 按钮的标识符
                                   ) As Long
    cmdShow.Caption = cmdShow.Tag        '先显示Tag里的数据
    
    Randomize                            '随机数种子
    cmdShow.Tag = Str(Int(Rnd() * 100) + 1)   '再随机存入1 - 100的随机数
                  '由于Tag是字符型数据，而随机数是数值，需要用Str转换成字符串才能存储
    
    Function = 0   ' 根据你的需要改变
End Function

'                                                                                  
Function Form1_Command2_BN_Clicked ( _
                                   ControlIndex     As Long, _      ' 控件数组的索引
                                   hWndForm         As HWnd, _      ' 窗体句柄
                                   hWndControl      As HWnd, _      ' 控件句柄
                                   idButtonControl  As Long   _     ' 按钮的标识符
                                   ) As Long
    AfxMsg("按钮的宽是：" & cmdShow.Width & "像素" & vbCrLf & "按钮的高是：" & cmdShow.Height & "像素")
    Function = 0   ' 根据你的需要改变
End Function

'                                                                                  
Function Form1_Command3_BN_Clicked ( _
                                   ControlIndex     As Long, _      ' 控件数组的索引
                                   hWndForm         As HWnd, _      ' 窗体句柄
                                   hWndControl      As HWnd, _      ' 控件句柄
                                   idButtonControl  As Long   _     ' 按钮的标识符
                                   ) As Long
    cmdShow.Left = cmdShow.Left - 10
    Function = 0   ' 根据你的需要改变
End Function

'                                                                                  
Function Form1_Command4_BN_Clicked ( _
                                   ControlIndex     As Long, _      ' 控件数组的索引
                                   hWndForm         As HWnd, _      ' 窗体句柄
                                   hWndControl      As HWnd, _      ' 控件句柄
                                   idButtonControl  As Long   _     ' 按钮的标识符
                                   ) As Long
    cmdShow.Left = cmdShow.Left + 10
    Function = 0   ' 根据你的需要改变
End Function

'                                                                                  
Function Form1_Command5_BN_Clicked ( _
                                   ControlIndex     As Long, _      ' 控件数组的索引
                                   hWndForm         As hWnd, _      ' 窗体句柄
                                   hWndControl      As hWnd, _      ' 控件句柄
                                   idButtonControl  As Long   _     ' 按钮的标识符
                                   ) As Long
    cmdShow.top = cmdShow.top - 10
    Function = 0   ' 根据你的需要改变
End Function

'                                                                                  
Function Form1_Command6_BN_Clicked ( _
                                   ControlIndex     As Long, _      ' 控件数组的索引
                                   hWndForm         As hWnd, _      ' 窗体句柄
                                   hWndControl      As hWnd, _      ' 控件句柄
                                   idButtonControl  As Long   _     ' 按钮的标识符
                                   ) As Long
    cmdShow.top = cmdShow.top + 10
    Function = 0   ' 根据你的需要改变
End Function

