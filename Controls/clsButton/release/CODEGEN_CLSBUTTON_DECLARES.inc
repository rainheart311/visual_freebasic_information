'------------------------------------------------------------------------------
' 由 VisualFreeBasic 生成源代码，版本：4.0.1
' 生成：星期一  一月 21, 2019 at 04:29:52 下午
' 更多信息访问 www.planetsquires.com 
'------------------------------------------------------------------------------





Dim Shared HWND_FORM1 As HWND
Dim Shared IDC_FORM1_COMMAND4 As Long
Dim Shared HWND_FORM1_COMMAND4 As HWND
Dim Shared IDC_FORM1_COMMAND3 As Long
Dim Shared HWND_FORM1_COMMAND3 As HWND
Dim Shared IDC_FORM1_COMMAND6 As Long
Dim Shared HWND_FORM1_COMMAND6 As HWND
Dim Shared IDC_FORM1_COMMAND5 As Long
Dim Shared HWND_FORM1_COMMAND5 As HWND
Dim Shared IDC_FORM1_COMMAND2 As Long
Dim Shared HWND_FORM1_COMMAND2 As HWND
Dim Shared IDC_FORM1_CMDVISIBLE As Long
Dim Shared HWND_FORM1_CMDVISIBLE As HWND
Dim Shared IDC_FORM1_CMDCLICK As Long
Dim Shared HWND_FORM1_CMDCLICK As HWND
Dim Shared IDC_FORM1_COMMAND1 As Long
Dim Shared HWND_FORM1_COMMAND1 As HWND
Dim Shared IDC_FORM1_CMDSHOW As Long
Dim Shared HWND_FORM1_CMDSHOW As HWND
Dim Shared IDC_FORM1_CMDENABLED As Long
Dim Shared HWND_FORM1_CMDENABLED As HWND
Dim Shared IDC_FORM1_CMDCAPTION As Long
Dim Shared HWND_FORM1_CMDCAPTION As HWND


Declare Function FORM1_Show( ByVal hWndParent As HWND = HWND_DESKTOP, _
                             ByVal ShowModalFlag As Long = 0, _
                             ByVal UserData As Long = 0 _
                             )  As HWND
Declare Sub FORM1_CreateControls( ByVal hWndForm As HWND ) 
Declare Function FORM1_FORMPROCEDURE( ByVal hWndForm As HWND, _
                                      ByVal wMsg   As uInteger, _ 
                                      ByVal wParam As WPARAM, _
                                      ByVal lParam As LPARAM _
                                      ) As LRESULT
Declare Function FORM1_CODEPROCEDURE( ByVal hWndControl As HWND, _
                                      ByVal wMsg   As uInteger, _ 
                                      ByVal wParam As WPARAM, _
                                      ByVal lParam As LPARAM _
                                      ) As LRESULT
Declare Function FORM1_WM_CREATE ( _
                         hWndForm As hWnd, _
                         ByVal UserData As Integer _
                         ) As Long
Declare Function Form1_cmdCaption_BN_Clicked ( _
                                   ControlIndex     As Long, _
                                   hWndForm         As hWnd, _
                                   hWndControl      As hWnd, _
                                   idButtonControl  As Long   _
                                   ) As Long
Declare Function Form1_cmdEnabled_BN_Clicked ( _
                                     ControlIndex     As Long, _
                                     hWndForm         As hWnd, _
                                     hWndControl      As hWnd, _
                                     idButtonControl  As Long   _
                                     ) As Long
Declare Function Form1_cmdVisible_BN_Clicked ( _
                                     ControlIndex     As Long, _
                                     hWndForm         As HWnd, _
                                     hWndControl      As HWnd, _
                                     idButtonControl  As Long   _
                                     ) As Long
Declare Function Form1_cmdClick_BN_Clicked ( _
                                   ControlIndex     As Long, _
                                   hWndForm         As HWnd, _
                                   hWndControl      As HWnd, _
                                   idButtonControl  As Long   _
                                   ) As Long
Declare Function Form1_cmdShow_BN_Clicked ( _
                                  ControlIndex     As Long, _
                                  hWndForm         As HWnd, _
                                  hWndControl      As HWnd, _
                                  idButtonControl  As Long   _
                                  ) As Long
Declare Function Form1_Command1_BN_Clicked ( _
                                   ControlIndex     As Long, _
                                   hWndForm         As HWnd, _
                                   hWndControl      As HWnd, _
                                   idButtonControl  As Long   _
                                   ) As Long
Declare Function Form1_Command2_BN_Clicked ( _
                                   ControlIndex     As Long, _
                                   hWndForm         As HWnd, _
                                   hWndControl      As HWnd, _
                                   idButtonControl  As Long   _
                                   ) As Long
Declare Function Form1_Command3_BN_Clicked ( _
                                   ControlIndex     As Long, _
                                   hWndForm         As HWnd, _
                                   hWndControl      As HWnd, _
                                   idButtonControl  As Long   _
                                   ) As Long
Declare Function Form1_Command4_BN_Clicked ( _
                                   ControlIndex     As Long, _
                                   hWndForm         As HWnd, _
                                   hWndControl      As HWnd, _
                                   idButtonControl  As Long   _
                                   ) As Long
Declare Function Form1_Command5_BN_Clicked ( _
                                   ControlIndex     As Long, _
                                   hWndForm         As hWnd, _
                                   hWndControl      As hWnd, _
                                   idButtonControl  As Long   _
                                   ) As Long
Declare Function Form1_Command6_BN_Clicked ( _
                                   ControlIndex     As Long, _
                                   hWndForm         As hWnd, _
                                   hWndControl      As hWnd, _
                                   idButtonControl  As Long   _
                                   ) As Long
Declare Function FF_WINMAIN( ByVal hInstance     As HINSTANCE, _
                     ByVal hPrevInstance As HINSTANCE, _
                     ByRef lpCmdLine     As String, _  
                     ByVal iCmdShow      As Long ) As Long
Declare Function FF_PUMPHOOK( uMsg As Msg ) As Long
Declare Sub FLY_InitializeVariables()    
Declare Sub FLY_SetAppVariables()    
Declare Function FLY_AdjustWindowRect( ByVal hWndForm As HWND, _
                               ByVal cxClient As Long   , _
                               ByVal cyClient As Long    _
                               ) As Long
Declare Function FLY_EnumSysColorChangeProc( ByVal hWnd As HWND, lParam As LPARAM ) As Long
Declare Function FLY_SetControlData( ByVal hWndControl    As HWND, _
                             ByVal AllowSubclass  As Long   , _
                             ByVal AllowSetFont   As Long   , _
                             ByRef sFontString    As String, _
                             ByVal nControlIndex  As Long   , _
                             ByVal nProcessColor  As Long   , _
                             ByVal IsForeSysColor As Long   , _
                             ByVal IsBackSysColor As Long   , _
                             ByVal nForeColor     As Long   , _
                             ByVal nBackColor     As Long   , _
                             ByVal nTransparentBrush As HBRUSH, _
                             ByVal CodeProcedure  As WNDPROC, _
                             sResizeRules         As String, _      
                             ByVal nFontUpgrade   As Long    _   
                             ) As FLY_DATA ptr
Declare Function FLY_DoMessagePump( ByVal ShowModalFlag  As Long, _
                            ByVal hWndForm       As HWND, _
                            ByVal hWndParent     As HWND, _
                            ByVal nFormShowState As Long, _
                            ByVal IsMDIForm      As Long _
                            ) As HWND
Declare Function FLY_GetActiveHandle( ByVal hWnd As HWND ) as HWND
Declare Function FLY_ResizeRuleInitEnum( ByVal hWnd As HWND, ByVal lParam As LPARAM ) As Long
Declare Function FLY_ResizeRuleEnum( ByVal hWnd As HWND, ByVal lParam As LPARAM ) As Long
Declare Function WinMain( ByVal hInstance     As HINSTANCE, _
                  ByVal hPrevInstance As HINSTANCE, _
                  ByRef lpCmdLine     As String,  _
                  ByVal iCmdShow      As Long _
                  ) As Long
Declare Function FF_ListView_GetColumnAlignment( ByVal hWndControl As HWnd, _
                                         ByVal iPosition As Long _
                                         ) As Long

Declare Function FF_ListView_GetItemImage( ByVal hWndControl as HWnd, _
                                   ByVal iRow as Long, _
                                   ByVal iColumn as Long _
                                   ) as Long

Declare Function FF_ListView_GetItemlParam (ByVal hWndControl as HWnd, _
                                    ByVal iRow as Long, _
                                    ByVal iColumn as Long) as Long

Declare Function FF_ListView_GetItemText( ByVal hWndControl as HWnd, _
                                  ByVal iRow as Long, _
                                  ByVal iColumn as Long _
                                  ) as String

Declare Function FF_ListView_InsertColumn( ByVal hWndControl as HWnd, _
                                   ByVal iPosition   as Long, _
                                   ByRef TheText     as String, _
                                   ByVal nAlignment  as Long = LVCFMT_LEFT, _
                                   ByVal nWidth      as Long = 100 _
                                   ) as Integer

Declare Function FF_ListView_InsertItem( ByVal hWndControl as HWnd, _
                                 ByVal iRow        as Long, _         
                                 ByVal iColumn     as Long, _
                                 ByRef TheText     as String, _
                                 ByVal lParam as Long = 0, _
                                 ByVal iImage as Long = 0 _
                                 ) as Integer

Declare Function FF_ListView_SetColumnAlignment( ByVal hWndControl as HWnd, _
                                         ByVal iPosition as Long, _
                                         ByVal nAlignment as Long _
                                         ) as Integer

Declare Function FF_ListView_SetItemImage( ByVal hWndControl as HWnd, _
                                   ByVal iRow as Long, _
                                   ByVal iColumn as Long, _
                                   ByVal iImage as Long _
                                   ) as Integer

Declare Function FF_ListView_SetItemlParam( ByVal hWndControl as HWnd, _
                                    ByVal iRow as Long, _
                                    ByVal iColumn as Long, _
                                    ByVal lParam as Long _
                                    ) as Integer

Declare Function FF_ListView_SetItemText( ByVal hWndControl as HWnd, _
                                  ByVal iRow as Long, _
                                  ByVal iColumn as Long, _
                                  ByRef TheText as String _
                                  ) as Long

Declare Function FF_ListView_SetSelectedItem( ByVal hWndControl as HWnd, _
                                      ByVal nIndex as Long _
                                      ) as Long

Declare Function FF_Control_GetCheck( ByVal hWndControl As HWND ) As Long

Declare Sub FF_Control_GetColor( ByVal hWndControl as HWnd, _
                         ByRef ForeColor as Long, _
                         ByRef BackColor as Long)

Declare Function FF_Control_GetTag( ByVal hWndControl As HWND ) As String

Declare Function FF_Control_GetTag2( ByVal hWndControl As HWND ) As String

Declare Function FF_Control_GetText(ByVal hWndControl As HWND) As String

Declare Sub FF_Control_SetCheck( ByVal hWndControl as HWnd, _
                         ByVal nCheckState as Integer ) 

Declare Sub FF_Control_SetColor( ByVal hWndControl as HWnd, _
                         ByVal ForeColor as Integer, _
                         ByVal BackColor as Integer)

Declare Sub FF_Control_SetTag( ByVal hWnd As HWND, _
                       ByRef NewTag As String )

Declare Sub FF_Control_SetTag2( ByVal hWnd As HWND, _
                        ByRef NewTag As String )

Declare Function FF_Parse( ByRef sMainString as String, _
                   ByRef sDelimiter  as String, _
                   ByVal nPosition   as Integer _
                   ) as String

Declare Function FF_Parse_Internal( ByRef sMainString   as String, _
                            ByRef sDelimiter    as String, _
                            ByRef nPosition     as Integer, _
                            ByRef nIsAny        as Integer, _
                            ByRef nLenDelimiter as Integer _
                            ) as String

Declare Function FF_AfxGetFontPointSize( ByVal nHeight As Long ) As Long
Declare Function FF_GetFontInfo( ByRef sCurValue  As String, _
                         ByRef sFontName  As String, _
                         ByRef nPointSize As Long, _
                         ByRef nWeight    As Long, _
                         ByRef nItalic    As Long, _
                         ByRef nUnderline As Long, _
                         ByRef nStrikeOut As Long, _
                         ByRef nFontStyle As Long _
                         ) As Long
Declare Function FF_EnumCharSet( _
                       elf            As ENUMLOGFONT,   _
                       ntm            As NEWTEXTMETRIC, _
                       ByVal FontType As Long, _
                       CharSet        As Long  _
                       ) As Long
Declare Function FF_MakeFontEX( sFont            As String, _
                        ByVal PointSize  As Long,   _
                        ByVal fBold      As Long,   _ 
                        ByVal fItalic    As Long,   _
                        ByVal fUnderline As Long,   _
                        ByVal StrikeThru As Long    _
                        ) As HFONT
Declare Function FF_MakeFontEx_Internal( sFont As String ) As HFONT

Declare Sub FF_CloseForm( hwndForm As HWND, _
                  ByVal ReturnValue As Integer = 0 ) 


